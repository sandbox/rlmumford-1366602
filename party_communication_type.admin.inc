<?php

/**
 * @file
 * Party Communication Type UI.
 */

/**
 * The type editing form.
 */
function party_communication_type_form($form, &$form_state, $type, $op = 'edit') {
  $form_state['party_communication_type'] = $type;

  if (!empty($form_state['input']['mode'])) {
    $type->mode = $form_state['input']['mode'];
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $type->label,
    '#description' => t('The human-readable name of this type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($type->name) ? $type->name : '',
    '#machine_name' => array(
      'exists' => 'party_communication_get_bundles',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this communication type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $groups = party_communication_groups();
  $options = array();
  foreach ($groups as $name => $info) {
    $options[$name] = $info['label'];
  }
  $form['communication_group'] = array(
    '#title' => t('Group'),
    '#type' => 'select',
    '#default_value' => isset($type->communication_group) ? $type->communication_group : '',
    '#options' => $options,
    '#required' => TRUE,
  );

  $form['mode'] = array(
    '#type' => 'select',
    '#title' => t('Mode'),
    '#description' => t('How these communication are sent and recieved.'),
    '#default_value' => isset($type->mode) ? $type->mode : '',
    '#options' => party_communication_mode_options(),
    '#empty_option' => t('- Please Select -'),
    '#required' => TRUE,
    '#disabled' => isset($type->mode) && empty($type->is_new),
    '#ajax' => array(
      'callback' => 'party_communication_type_mode_select_ajax',
      'wrapper' => 'mode-settings-form',
    ),
  );

  if (!empty($type->mode)) {
    $form['mode_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Mode Settings'),
      '#description' => t('Configuration options for this mode of communication.'),
      '#parents' => array('mode_settings'),
      '#tree' => TRUE,
      '#prefix' => '<div id="mode-settings-form">',
      '#suffix' => '</div>',
    );

    $form['mode_settings'] = $type->getController()->settingsForm($form['mode_settings'], $form_state);
    $form['mode_settings']['#access'] = (bool) element_children($form['mode_settings']);
  }
  else {
    $form['mode']['#suffix'] = '<div id="mode-settings-form"></div>';
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Ajax Callback for mode select.
 */
function party_communication_type_mode_select_ajax($form, $form_state) {
  return $form['mode_settings'];
}

/**
 * Form API validate callback for the type form.
 */
function party_communication_type_form_validate($form, &$form_state) {
  $type = entity_ui_form_submit_build_entity($form, $form_state);
  $type->getController()->settingsFormValidate($form['mode_settings'], $form_state);
}

/**
 * Form API submit callback for the type form.
 */
function party_communication_type_form_submit(&$form, &$form_state) {
  $type = entity_ui_form_submit_build_entity($form, $form_state);
  $type->getController()->settingsFormSubmit($form['mode_settings'], $form_state);
  $type->save();
  $form_state['redirect'] = 'admin/structure/communication_types';
}

/**
 * Form API submit callback for the delete button.
 */
function party_communication_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/communication_types/manage/' . $form_state['type']->name . '/delete';
}

