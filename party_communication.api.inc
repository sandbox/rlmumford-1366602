<?php

/**
 * Perform an operation.
 *
 * @param \PartyCommunication $communication
 * @param string $op
 *
 * @return void
 */
function party_communication_api_perform_operation(PartyCommunication $communication, string $op) {
  $reasons = [];
  if (!$communication->validateOperation($op, $_REQUEST, $reasons)) {
    drupal_add_http_header('Status', 400);
    drupal_send_headers();
    print implode("\n", $reasons);
    drupal_exit();
  }

  try {
    $communication->doOperation($op, $_REQUEST);

    drupal_add_http_header('Status', 200);
    drupal_send_headers();
    // Send an empty JSON array to be compatible with drupal ajax.
    print ajax_render([]);
    drupal_exit();
  }
  catch (\Exception $exception) {
    watchdog_exception("party_communication", $exception);

    drupal_add_http_header('Status', 503);
    drupal_send_headers();
    print "Error";
    drupal_exit();
  }
}

/**
 * Access check for performing an operation.
 *
 * @param \PartyCommunication $communication
 * @param string $op
 */
function party_communication_api_perform_operation_access(PartyCommunication $communication, string $op) {
  $controller = $communication->getType()->getController();
  $ops = $controller->supportedOperations();

  return party_communication_access("update", $communication) &&
    party_communication_access($op, $communication) &&
    isset($ops[$op]) &&
    $communication->validateOperation($op, $_REQUEST);
}

/**
 * API Endpoint for communication events.
 */
function party_communication_api_event(PartyCommunication $party_communication) {
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $request = $_SERVER['CONTENT_TYPE'] === 'application/json' ? json_decode(file_get_contents('php://input'), TRUE) :  $_REQUEST;
    entity_get_controller('party_communication')->saveEvents($party_communication, PartyCommunicationEvent::create(
      $party_communication,
      $request['eventType'],
      $request['label'],
      $request['data'],
      $request['participationId'] ?? NULL,
      $request['dateTime'] ? \DateTime::createFromFormat(DATE_RFC3339_EXTENDED, $request['dateTime']) : new \DateTime(),
    ));
  }

  drupal_add_http_header('Status', 200);
  drupal_send_headers();
  // Send an empty JSON array to be compatible with drupal ajax.
  print ajax_render([]);
  drupal_exit();
}
