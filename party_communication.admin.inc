<?php

/**
 * @file
 * UI Form Communications.
 */

/**
 * UI Controller
 */
class PartyCommunicationUIController extends EntityDefaultUIController {

  public function hook_menu() {
    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = '%party_communication';

    $items[$this->path . '/add/%party_communication_type'] = array(
      'title' => t('Add Communication'),
      'page callback' => 'party_communication_add_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'party_communication_access',
      'access arguments' => array('create'),
      'file' => 'party_communication.admin.inc',
      'file path' => drupal_get_path('module', 'party_communication'),
    );

    // Loading and editing party_communication entities
    $items[$this->path . '/communication/' . $wildcard] = array(
      //'title callback' => 'party_communication_page_title',
      //'title arguments' => array($id_count + 1, 'edit'),
      'title' => t('Edit Communication'),
      'page callback' => 'party_communication_form_wrapper',
      'page arguments' => array($id_count + 1, 'edit'),
      'access callback' => 'party_communication_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'party_communication.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    $items[$this->path . '/communication/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );
    $items[$this->path . '/communication/' . $wildcard . '/send'] = array(
      'title' => 'Send',
      'type' => MENU_LOCAL_TASK,
      'weight' => 10,
      'page callback' => 'party_communication_form_wrapper',
      'page arguments' => array($id_count + 1, 'send'),
      'access callback' => 'party_communication_access',
      'access arguments' => array('edit', $id_count + 1),
      'file' => 'party_communication.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    return $items;
  }
}

/**
 * Modal Form Wrapper.
 *
 * @param $communication
 *   The communication object to edit.
 * @param $js
 */
function party_communication_form_wrapper($communication, $op = 'edit', $js = FALSE) {
  if (!$js) {
    return drupal_get_form('party_communication_form', $communication, $op);
  }

  ctools_include('ajax');
  ctools_include('modal');

  // Work out Form label.
  $info = entity_get_info('party_communication');
  $label = (empty($communication->id)) ? t('Add !label', array('!label' => $info['label'])) : t('Edit @label', array('@label' => $communication->label));

  $form_state = array(
    'title' => $label,
    'ajax' => TRUE,
    'build_info' => array(
      'args' => array(
        $communication,
        $op,
      ),
    ),
  );
  $commands = ctools_modal_form_wrapper('party_communication_form', $form_state);

  if (!empty($form_state['executed']) && empty($form_state['rebuild'])) {
    // Overwrite the output if form submission was successfully executed.
    $commands = array();
    $commands[] = ctools_ajax_command_reload();
  }

  $output = array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );
  ajax_deliver($output);
  drupal_exit();
}

/**
 * Add form wrapper.
 *
 * Creates a new communication entity before passing it onto party_communication_modal_form_wrapper.
 *
 * @param string $type
 *   (Optional) The machine name of the communication type to create.
 * @param $js
 */
function party_communication_add_form_wrapper($type = NULL, $js = FALSE) {
  $values = array(
    'inoutbound' => PARTY_COMMUNICATION_OUTBOUND,
  );
  $info = entity_get_info('party_communication');

  if (!empty($type)) {
    $values['type'] = $type->name;
  }
  else if (count($info['bundles']) === 1) {
    $values['type'] = key($info['bundles']);
  }

  $communication = entity_create('party_communication', $values);
  return party_communication_form_wrapper($communication, 'create', $js);
}

/**
 * Form Callback: create or edit a communication.
 *
 * @param $communication.
 */
function party_communication_form($form, &$form_state, $communication, $op = 'edit') {
  form_load_include($form_state, 'inc', 'party_communication', 'party_communication.admin');

  $form_state['party_communication'] = $communication;

  // If the operation is delete show a confirmation form.
  if ($op == 'delete') {
    return party_communication_delete_form($form, $form_state, $communication);
  }

  // Add the default field elements.
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => $communication->getType()->getController()->labelElementTitle($communication),
    '#default_value' => isset($communication->label) ? $communication->label : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  // Add a status field.
  $options = party_communication_status_options($communication->getType()->mode, TRUE);
  if (empty($communication->status) || !empty($options[$communication->status])) {
    $form['status'] = array(
      '#title' => t('Status'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => !empty($communication->status) ? $communication->status : NULL,
      '#required' => TRUE,
    );
  }

  field_attach_form('party_communication', $communication, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();
  $validate = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
    $validate += $form['#validate'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => $submit + array('party_communication_form_submit'),
    '#validate' => $validate + array('party_communication_form_validate'),
  );
  $form['actions']['submit_send'] = array(
    '#type' => 'submit',
    '#value' => t('Save and Send'),
    '#submit' => $submit + array('party_communication_form_submit', 'party_communication_form_submit_send'),
    '#validate' => $validate + array('party_communication_form_validate', 'party_communication_form_validate_send'),
  );

  if (!empty($communication->id)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#suffix' => l(t('Cancel'), 'admin/community/communications'),
      '#submit' => $submit + array('party_communication_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'party_communication_form_validate';

  return $form;
}

/**
 * Form API validate callback for the party_communication form
 */
function party_communication_form_validate(&$form, &$form_state) {
  $communication = $form_state['party_communication'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('party_communication', clone $communication, $form, $form_state);
}

/**
 * Form API validate callback for the party_communication form submit
 */
function party_communication_form_validate_send($form, &$form_state) {
  $reasons = array();
  $communication = entity_ui_controller('party_communication')->entityFormSubmitBuildEntity($form, $form_state);
  if (!$communication->validateSend(array(), $reasons)) {
    $message = 'This communication is not ready to send.';
    if (!empty($reasons)) {
      $message .= '<ul><li>' . implode($reasons, '</li><li>') . '</li></ul>';
    }
    form_error($form, $message);
  }
}

/**
 * Form API submit callback for the communication form.
 *
 * @todo remove hard-coded link
 */
function party_communication_form_submit(&$form, &$form_state) {

  $communication = entity_ui_controller('party_communication')->entityFormSubmitBuildEntity($form, $form_state);

  // Save the communication and go back to the list of communications

  // Add in created and changed times.
  if ($communication->is_new = isset($communication->is_new) ? $communication->is_new : 0) {
    $communication->created = time();
    $communication->status = 'draft';
  }
  else {
    $communication->is_new_revision = TRUE;
    $communication->revision_log = t('User Update');
  }

  $communication->modified = time();

  $communication->save();
}

/**
 * Form API submit to send the communication.
 */
function party_communication_form_submit_send($form, &$form_state) {
  $communication = $form_state['party_communication'];
  $communication->send();
}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function party_communication_form_submit_delete(&$form, &$form_state) {
  $form_state['build_info']['args'][1] = 'delete';
  $form_state['rebuild'] = TRUE;
}

/**
 * Form callback: confirmation form for deleting a communication.
 *
 * @param $communication
 *   The party_communication to delete
 *
 * @see confirm_form()
 */
function party_communication_delete_form($form, &$form_state, $communication) {
  $form_state['communication'] = $communication;

  $form['#submit'][] = 'party_communication_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete communication %label?', array('%label' => $communication->label)),
    'admin/community/communications/communication',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for communication_delete_form
 */
function party_communication_delete_form_submit($form, &$form_state) {
  $communication = $form_state['communication'];

  party_communication_delete($communication);

  drupal_set_message(t('The communication %label has been deleted.', array('%label' => $communication->label)));
  watchdog('communication', 'Deleted communication %label.', array('%label' => $communication->label));

  $form_state['redirect'] = 'admin/community/communications';
}

/**
 * Operations form for a communication.
 */
function party_communication_operations_form($form, &$form_state, $communication, $options = array()) {
  $form_state['communication'] = $communication;
  $controller = $communication->getType()->getController();

  $form['#prefix'] = '<div id="communication-operations-form">';
  $form['#suffix'] = '</div>';
  $ajax = array(
    'callback' => 'party_communication_operations_form_ajax',
    'wrapper' => 'communication-operations-form',
    'method' => 'replace',
  );

  if (empty($form_state['op'])) {
    $form['operations'] = array(
      '#type' => 'container',
    );
    foreach ($controller->supportedOperations() as $op => $info) {
      if (!empty($info['no_ui'])) {
        continue;
      }

      $form['operations']['do_'.$op] = array(
        '#type' => 'submit',
        '#value' => $info['label'],
        '#submit' => array('party_communication_operations_form_submit'),
        '#validate' => array('party_communication_operations_form_validate'),
        '#ajax' => $ajax,
        '#op' => $op,
      );
    }
  }
  else {
    $default_settings = !empty($options['operation_settings'][$form_state['op']]) ? $options['operation_settings'][$form_state['op']] : array();
    $default_settings += !empty($options['settings']) ? $options['settings'] : array();
    $form = $communication->operationForm($form, $form_state, $form_state['op'], $default_settings);

    $form['actions'] = array(
      '#type' => 'actions',
      '#weight' => 100,
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Continue'),
      '#submit' => array('party_communication_operations_form_submit_op'),
      '#validate' => array('party_communication_operations_form_validate_op'),
      '#ajax' => $ajax,
    );
    $form['actions']['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#submit' => array('party_communication_operations_form_cancel_op'),
      '#validate' => array(),
      '#ajax' => $ajax,
    );
  }

  return $form;
}

/**
 * Get the operations form from the full form array based on the triggering
 * element.
 */
function party_communication_get_ops_form_chunk($form, $form_state) {
   $array_parents = $form_state['triggering_element']['#array_parents'];
   array_pop($array_parents); array_pop($array_parents);
   return drupal_array_get_nested_value($form, $array_parents);
}

/**
 * Ajax callback for the form.
 */
function party_communication_operations_form_ajax($form, $form_state) {
  $form_chunk = party_communication_get_ops_form_chunk($form, $form_state);
  if (empty($form_state['commands'])) {
    return $form_chunk;
  }
  else {
    $commands[] = ajax_command_replace('#communication-operations-form', drupal_render($form_chunk));
    foreach ($form_state['commands'] as $command) {
      $commands[] = $command;
    }

    return array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
  }
}

/**
 * Validate handler to do operations.
 */
function party_communication_operations_form_validate($form, &$form_state) {
  $communication = $form_state['communication'];
  $op = $form_state['triggering_element']['#op'];

  $ops = $communication->getType()->getController()->supportedOperations();
  if (empty($ops[$op]['form'])) {
    $reasons = array();
    $communication->getType()->getController()->validateOperation($op, $communication, [], $reasons);

    foreach ($reasons as $reason) {
      form_error($form_state['triggering_element'], $reason);
    }
  }
}

/**
 * Validate handler to validate op forms.
 */
function party_communication_operations_form_validate_op($form, &$form_state) {
  /** @var \PartyCommunication $communication */
  $communication = $form_state['communication'];
  $form_chunk = party_communication_get_ops_form_chunk($form, $form_state);
  $communication->operationFormValidate($form_chunk, $form_state, $form_state['op']);
}

/**
 * Submit handler to do operations form.
 */
function party_communication_operations_form_submit_op($form, &$form_state) {
  /** @var \PartyCommunication $communication */
  $communication = $form_state['communication'];
  $form_chunk = party_communication_get_ops_form_chunk($form, $form_state);
  $conf = $communication->operationFormSubmit($form_chunk, $form_state, $form_state['op']);
  $communication->doOperation($form_state['op'], $conf);
  $communication->operationFormFinish($form_chunk, $form_state, $form_state['op']);

  $form_state['op'] = FALSE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler to cancel operation form.
 */
function party_communication_operations_form_cancel_op($form, &$form_state) {
  $form_state['op'] = FALSE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler to do the operations.
 */
function party_communication_operations_form_submit($form, &$form_state) {
  $communication = $form_state['communication'];
  $op = $form_state['triggering_element']['#op'];

  $ops = $communication->getType()->getController()->supportedOperations();

  if (!empty($ops[$op]['form'])) {
    $form_state['op'] = $op;
    $form_state['rebuild'] = TRUE;
  }
  else {
    $communication->doOperation($op);
  }
}

/**
 * Global settings form.
 */
function party_communcation_global_settings_form($form, &$form_state) {
  $form['party_communication_block_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Block ALL communications leaving this site.'),
    '#default_value' => variable_get('party_communication_block_all', FALSE),
  );
  return system_settings_form($form);
}
