<?php
/**
 * @file
 * Integration functions for the party communication stuff.
 */

/**
 * Implements hook_party_communication_contact_source_info().
 */
function party_communication_party_communication_contact_source_info() {
  $sources = array();
  $sources['party:email'] = array(
    'class' => 'PartyCommunicationContactSourceProperty',
    'label' => t('Primary Email'),
    'property' => 'email',
    'priority' => 100,
    'primitive' => 'email',
    'multiple' => FALSE,
  );

  return $sources;
}

/**
 * Implements hook_party_communication_contact_primitive_info().
 */
function party_communication_party_communication_contact_primitive_info() {
  $primitives = array();
  $primitives['email'] = array(
    'label' => t('E-Mail Address'),
    'plural label' => t('E-Mail Addresses'),
    'data_class' => PartyCommunicationContactDataEmailAddress::class,
  );
  $primitives['phone'] = array(
    'label' => t('Telephone Number'),
    'plural label' => t('Telephone Numbers'),
    'data_class' => PartyCommunicationContactDataTelephoneNumber::class,
  );
  $primitives['fax'] = array(
    'label' => t('Fax Number'),
    'plural label' => t('Fax Numbers'),
    'data_class' => PartyCommunicationContactDataTelephoneNumber::class,
  );
  $primitives['sms'] = array(
    'label' => t('SMS Number'),
    'plural label' => t('SMS Numbers'),
    'data_class' => PartyCommunicationContactDataTelephoneNumber::class,
  );
  $primitives['postal_address'] = array(
    'label' => t('Postal Address'),
    'plural label' => t('Postal Addresses'),
    'data_class' => PartyCommunicationContactDataPostalAddress::class,
  );
  return $primitives;
}
