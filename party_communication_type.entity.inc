<?php

/**
 * @file
 *  Contains party communication type entity class.
 */

class PartyCommunicationType extends Entity {

  /**
   * Get the mode controller for this communication type.
   *
   * @return \PartyCommunicationModeInterface
   *   The communication mode.
   */
  public function getController() {
    return party_communication_mode_controller($this->mode, $this->data);
  }
}

class PartyCommunicationTypeController extends EntityAPIControllerExportable {

  /**
   * {@inheritdoc}
   */
  public function invoke($hook, $entity) {
    parent::invoke($hook, $entity);

    if (in_array($hook, array('insert', 'update'))) {
      $entity->getController()->postTypeSave($entity);
    }
  }
}
