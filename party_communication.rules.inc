<?php

/**
 * @file
 * Rules integration for party communications.
 */

/**
 * Implements hook_rules_condition_info().
 */
function party_communication_rules_condition_info() {
  $conditions = array();
  $conditions['party_communication_contact_check_data'] = array(
    'label' => t('Check Party Contact Data'),
    'parameter' => array(
      'party' => array(
        'label' => t('Party'),
        'type' => 'party',
      ),
      'primitive' => array(
        'label' => t('Primitive'),
        'type' => 'text',
        'description' => t('The type of contact data.'),
        'optional' => TRUE,
        'options list' => 'party_communication_contact_check_data_primitive_options',
        'restriction' => 'input',
      ),
      'value' => array(
        'type' => '*',
        'label' => t('Value'),
        'allow null' => TRUE,
      ),
    ),
    'group' => t('Communications'),
    'base' => 'party_communication_contact_check_data',
  );
  return $conditions;
}

/**
 * Options list for primitives.
 */
function party_communication_contact_check_data_primitive_options() {
  $source_info = PartyCommunicationContact::getSourceInfo();
  $options = array(
    '' => '- Any -',
  );
  foreach ($source_info as $source) {
    if (empty($options[$source['primitive']])) {
      $options[$source['primitive']] = ucwords($source['primitive']);
    }
  }
  return $options;
}

/**
 * Condition callback for rules to check data.
 */
function party_communication_contact_check_data($party, $primitive, $value) {
  return PartyCommunicationContact::checkData($party, $value, $primitive);
}

/**
 * Implement hook_rules_action_info().
 */
function party_communication_rules_action_info() {
  $actions = array();
  $actions['party_communication_save_send']  = array(
    'label' => t('Save & Send a Communication'),
    'parameter' => array(
      'communication' => array(
        'type' => 'party_communication',
        'label' => t('Communication'),
      ),
      'skip_validation' => array(
        'type' => 'boolean',
        'label' => t('Skip Validation?'),
        'optional' => TRUE,
        'allow null' => TRUE,
        'default value' => FALSE,
      ),
    ),
    'group' => t('Communications'),
    'base' => 'party_communication_rules_action_send',
  );

  foreach (party_communication_mode_info() as $name => $mode) {
    $bundles = db_select('party_communication_type', 't')
      ->condition('mode', $name)
      ->fields('t', array('name'))
      ->execute()
      ->fetchCol();

    $actions["party_communication_{$name}_op"] = array(
      'label' => t('Perform a @mode operation', array('@mode' => $mode['label'])),
      'parameter' => array(
        'communication' => array(
          'type' => 'party_communication',
          'bundles' => $bundles,
          'label' => t('Communication'),
        ),
        'operation' => array(
          'type' => 'text',
          'label' => t('Operation'),
          'options list' => 'party_communication_rules_operation_options',
        ),
      ),
      'group' => t('Communications'),
      'base' => 'party_communication_rules_action_op',
      'communication mode' => $name,
    );
  }

  return $actions;
}

/**
 * Action operations options.
 */
function party_communication_rules_operation_options(RulesPlugin $element, $param_name) {
  $controller = party_communication_mode_controller($element->info()['communication mode']);
  $options = array();
  foreach ($controller->supportedOperations() as $op => $info) {
    $options[$op] = $info['label'];
  }
  return $options;
}

/**
 * Action implementation: Operation.
 */
function party_communication_rules_action_op($communication, $op) {
  $communication->doOperation($op);
}

/**
 * Action implementation: Send.
 */
function party_communication_rules_action_send($communication, $skip_validation = FALSE) {
  $communication->send(array(
    'skip_validation' => $skip_validation,
  ));
}
