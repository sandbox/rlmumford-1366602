<?php

/**
 * Implements hook_entity_template_component_plugin_info().
 */
function party_communication_entity_template_component_plugin_info() {
  $info = [];
  $info['party_communication_add_participation'] = [
    'class' => 'PartyCommunicationAddParticipationEntityTemplateComponent',
    'label' => t('Add a Participation'),
  ];
  return $info;
}
