<?php

/**
 * Implements hook_entity_property_info_alter().
 */
function party_communication_twilio_entity_property_info_alter(&$info) {
  $types = db_select('party_communication_type', 't')
    ->condition('t.mode', 'twilio_conference')
    ->fields('t', ['name'])
    ->execute()->fetchCol();
  foreach ($types as $type) {
    $info['party_communication']['bundles'][$type]['properties']['communication_sender'] = [
      'type' => 'party',
      'label' => 'Caller',
      'getter callback' => 'party_communication_entity_property_party_from_participation_role',
      'participation role' => 'caller',
    ];
    $info['party_communication']['bundles'][$type]['properties']['communication_recipients'] = [
      'type' => 'party',
      'label' => 'Recipient',
      'getter callback' => 'party_communication_entity_property_party_from_participation_role',
      'participation role' => 'recipient',
    ];
  }
}
