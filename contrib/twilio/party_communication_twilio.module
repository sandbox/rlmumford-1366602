<?php

/**
 * Implements hook_menu().
 */
function party_communication_twilio_menu() {
  $items = [];

  $items['api/twilio/conferencestatus'] = [
    'page callback' => 'party_communication_twilio_api_conference_status_update',
    'access callback' => TRUE,
    'theme callback' => 'variable_get',
    'theme arguments' => array('admin_theme'),
    'delivery callback' => 'ajax_deliver',
    'type' => MENU_CALLBACK,
  ];

  $items['api/twilio/voice/accesstoken'] = [
    'page callback' => 'party_communication_twilio_api_get_voice_accesstoken',
    'access callback' => 'user_is_logged_in',
    'type' => MENU_CALLBACK,
  ];

  return $items;
}

/**
 * Implements twilio_sms_incoming.
 */
function party_communication_twilio_twilio_sms_incoming($sms, $options) {
  $query = db_select('party_communication', 'c')
    ->condition('inoutbound', PARTY_COMMUNICATION_INBOUND)
    ->condition('timestamp', time() - 30, '>');
  $query->join('party_communication_participation', 'pcp', 'pcp.id = c.id AND pcp.participation_role = :sender AND pcp.contact_data_primitive = :sms AND pcp.contact_data = :number', [
    ':sender' => 'sender',
    ':sms' => 'sms',
    ':number' => $sms['number'],
  ]);
  $query->join('field_data_communication_twilio_number', 'cn', 'cn.entity_type = :party_communication AND cn.entity_id = c.id AND cn.delta = 0', [':party_communication' => 'party_communication']);
  $query->condition('cn.communication_twilio_number_value', $sms['number_twilio']);
  if (!empty($options['AccountSid'])) {
    $query->join(
      'field_data_communication_twilio_account',
      'ta',
      'ta.entity_type = :party_communication AND ta.entity_id = c.id AND ta.delta = 0',
      [':party_communication' => 'party_communication']
    );
    $query->condition('ta.communication_twilio_account_value', $options['AccountSid']);
  }
  $query->addField('c', 'id', 'id');

  if ($existing_id = $query->execute()->fetchField()) {
    $communication = entity_load_single('party_communication', $existing_id);
    $communication->communication_twilio_message[LANGUAGE_NONE][0]['value'] .= ' '.preg_replace('/[^A-Za-z0-9 !?,"\'\/\\@#$%^&*().;+:_-~]/u', '', html_entity_decode($sms['message'], ENT_QUOTES));

    $raw = json_decode($communication->communication_twilio_raw[LANGUAGE_NONE][0]['value']);
    $raw[] = $sms;
    $communication->communication_twilio_raw[LANGUAGE_NONE][0]['value'] = json_encode($raw);
  }
  else {
    $values = array(
      'type' => party_communication_twilio_inbound_type($sms, $options),
      'inoutbound' => PARTY_COMMUNICATION_INBOUND,
      'timestamp' => time(),
      'status' => 'received',
    );
    /** @var \PartyCommunication $communication */
    $communication = entity_create('party_communication', $values);
    $communication->label = t('Inbound Communication');
    $communication->inoutbound = PARTY_COMMUNICATION_INBOUND;
    if (!empty($options['AccountSid'])) {
      $communication->communication_twilio_account[LANGUAGE_NONE][0]['value'] = $options['AccountSid'];
    }
    $communication->communication_twilio_message[LANGUAGE_NONE][0]['value'] = preg_replace('/[^A-Za-z0-9 !?,"\'\/\\@#$%^&*().;+:_-~]/u', '', html_entity_decode($sms['message'], ENT_QUOTES));
    $communication->communication_twilio_number[LANGUAGE_NONE][0]['value'] = $sms['number_twilio'];
    $communication->communication_twilio_raw[LANGUAGE_NONE][0]['value'] = json_encode(array($sms));

    $communication->addParticipation(PartyCommunicationParticipation::create(
      $communication,
      'sender',
      'sender',
      PartyCommunicationContactData::create('sms', $sms['number']),
    )->setNeedsAcquisition());
    $communication->addParticipation(PartyCommunicationParticipation::create(
      $communication,
      'recipient',
      'recipient',
      PartyCommunicationContactData::create('sms', $sms['number_twilio']),
    ));
  }

  // Handle Attached Media.
  if (!empty($sms['media'])) {
    if (is_string($sms['media'])) {
      $sms['media'] = array(array('url' => $sms['media'], 'type' => NULL));
    }

    $i = 0;
    foreach ($sms['media'] as $media_item) {
      $url = $media_item['url'];
      $temp_file_name = drupal_tempnam('temporary://', 'twilio_');

      $response = drupal_http_request($url);
      file_put_contents($temp_file_name, $response->data);

      $extension = '';
      $type = $response->headers['content-type'];
      if (!empty($type)) {
        [$cat,$ext] = explode('/', $type);

        if (in_array($cat, array('image', 'audio', 'video', 'application'))) {
          $unusual_exts = array(
            'x-icon' => 'ico',
            'svg+xml' => 'svg',
            'ogg' => ($cat == 'audio') ? 'oga' : 'ogv',
            'webm' => ($cat == 'audio') ? 'weba' : 'webm',
            '3gpp' => '3gp',
            '3gpp2' => '3g2',
            'x-msvideo' => 'avi',
            'msword' => 'doc',
            'vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx',
            'javascript' => 'js',
            'pdf' => 'pdf',
            'vnd.ms-powerpoint' => 'ppt',
            'vnd.openxmlformats-officedocument.presentationml.presentation' => 'pptx',
            'vnd.ms-excel' => 'xls',
            'vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xlsx',
            'zip' => 'zip',
          );
          $extension = '.'.(!empty($unusual_exts[$ext]) ? $unusual_exts[$ext] : (($cat == 'application') ? 'bin' : $ext));
        }
      }

      $filename = 'twilio_media_'.drupal_basename($url).$extension;
      if (!empty($response->headers['content-disposition'])) {
        $dparts = explode(';',$response->headers['content-disposition']);
        foreach ($dparts as $dpart) {
          $dpart = trim($dpart);
          [$dkey, $dval] = explode('=', $dpart);
          if ($dkey == 'filename') {
            $filename = trim($dval, '"\'');
            break;
          }
        }
      }
      if (module_exists('transliteration') && variable_get('transliteration_file_uploads', TRUE)) {
        $filename = transliteration_clean_filename($filename);
      }

      $file = new stdClass();
      $file->uid = 0;
      $file->status = 1;
      $file->filename = $filename;
      $file->uri = $temp_file_name;
      $file->filemime = $response->headers['content-type'];
      $file->filesize = filesize($temp_file_name);
      $file->source = 'MMS';

      $destination = "private://communication/twilio/media";
      file_prepare_directory($destination, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $file->destination = file_destination($destination .'/'.$file->filename, FILE_EXISTS_RENAME);
      $file->uri = $file->destination;
      if (!file_unmanaged_move($temp_file_name, $file->uri)) {
        continue;
      }
      drupal_chmod($file->uri);
      $file = file_save($file);

      if ($file->fid) {
        $file_item = array(
          'fid' => $file->fid,
          'display' => 1,
          'description' => '',
        );
        $communication->communication_twilio_media[LANGUAGE_NONE][$i] = $file_item;
        $i++;
      }
    }
  }

  $query = db_select('party_communication', 'c');
  $query->join('party_communication_participation', 'cp', 'cp.id = c.id AND cp.participation_role = :recipient AND cp.contact_data_primitive = :sms AND cp.contact_data = :number', [
    ':recipient' => 'recipient',
    ':sms' => 'sms',
    ':number' => $sms['number'],
  ]);
  $query->orderBy('c.timestamp', 'DESC');
  $query->condition('c.inoutbound', PARTY_COMMUNICATION_OUTBOUND);
  $query->addField('c', 'id', 'id');
  $query->range(0, 1);

  $last_communication = FALSE;
  if ($id = $query->execute()->fetchField()) {
    $last_communication = party_communication_load($id);
    $communication->label = "RE: " . $last_communication->label;
    $communication->communication_sender = $last_communication->communication_recipients;
    $communication->getParticipation('sender')->setPartyId($last_communication->communication_recipients[LANGUAGE_NONE][0]['target_id'] ?? NULL);
    $communication->communication_recipients = $last_communication->communication_sender;
    $communication->getParticipation('recipient')->setPartyId($last_communication->communication_sender[LANGUAGE_NONE][0]['target_id'] ?? NULL);
    $communication->setNeedsParticipationsSave();
    $communication->wrapper()->communication_in_reply_to = $last_communication;
  }
  drupal_alter('party_communication_twilio_inbound_sms', $communication, $last_communication);

  $communication->save();
}

/**
 * Get the type for an inbound sms.
 */
function party_communication_twilio_inbound_type($sms, $options) {
  return 'twilio_inbound_sms';
}

/**
 * Add some js to the widget for the text field.
 */
function party_communication_twilio_field_widget_form_alter(&$element, &$form_state, $context) {
  if ($context['field']['field_name'] != 'communication_twilio_message') {
    return;
  }

  $behavior = variable_get('twilio_long_sms', TWILIO_SMS_LONG_MULTIPLE);
  if ($behavior == TWILIO_SMS_LONG_TRUNCATE) {
    $element['value']['#maxlength'] = 160;
    $element['value']['#maxlength_js'] = TRUE;
  }
  else {
    $element['value']['#attached']['js'][] = drupal_get_path('module', 'party_communication_twilio') . '/js/party_communication_twilio.message_length.js';
  }
}

/**
 * Page callback for receiving a twilio conference status update.
 */
function party_communication_twilio_api_conference_status_update() {
  $input = file_get_contents('php://input');
  $status_update = json_decode($input, TRUE);

  if (!$input || !$status_update) {
    if (!empty($_POST['ConferenceSid']) || !empty($_POST['CallSid'])) {
      // @todo: Validate inputs against a whitelist.
      $status_update = $_POST;
    }
    else {
      watchdog(
        'party_communication_twilio',
        'Bad Request Sent to conference status update: ' . $input,
        [],
        WATCHDOG_WARNING
      );
      drupal_add_http_header('Status', '400 Invalid Request');
      drupal_page_header();
      drupal_exit();
    }
  }

  if (!empty($status_update['ConferenceSid'])) {
    $conference_sid = $status_update['ConferenceSid'];
    while (!lock_acquire('twilioconf__' . $conference_sid)) {
      lock_wait('twilioconf__' . $conference_sid, 1);
    }

    $communication_id = str_starts_with($status_update['FriendlyName'] ?? '', 'party_communication:') ?
      explode(':', $status_update['FriendlyName'])[1] :
      db_select('field_data_communication_twilio_conference', 'c')
        ->condition('c.communication_twilio_conference_value', $conference_sid)
        ->fields('c', ['entity_id'])
        ->execute()
        ->fetchField();

    if (!$communication_id || !($communication = entity_load_single('party_communication', $communication_id))) {
      // Find the appropriate communication type.
      // Get the first communication type of the twilio conference mode.
      $type = db_select('party_communication_type', 't')
        ->fields('t', ['name'])
        ->condition('mode', 'twilio_conference')
        ->orderBy('name')
        ->execute()->fetchField();

      $communication = entity_create('party_communication', ['type' => $type]);
      // Set to internal intially. This will be updated when the external participant joins.
      $communication->timestamp = REQUEST_TIME;
      $communication->inoutbound = PARTY_COMMUNICATION_INTERNAL;
      $communication->communication_twilio_account[LANGUAGE_NONE][0]['value'] = $status_update['AccountSid'];
      $communication->communication_twilio_conference[LANGUAGE_NONE][0]['value'] = $conference_sid;
      $communication->status = 'active';
      if (
        !empty($status_update['CallInfo']) &&
        (
          $twilio_number = $status_update['CallInfo']['direction'] === "inbound" ?
            $status_update['CallInfo']['to'] : $status_update['CallInfo']['from']
        )
      ) {
        $communication->communication_twilio_number[LANGUAGE_NONE][0]['value'] = $twilio_number;
      }
      $communication->save();
    }

    /** @var \PartyCommunication $communication */
    $operation_name = 'apply_status_update';
    if (isset($status_update['RecordingSid'])) {
      $operation_name = 'apply_recording_status_update';
    }
    else {
      $candidate_op_name = 'apply_status_update_' . strtolower(str_replace('-', '_', $status_update['StatusCallbackEvent'] ?: 'other'));
      if (!empty($communication->getType()->getController()->supportedOperations()[$candidate_op_name])) {
        $operation_name = $candidate_op_name;
      }
    }

    $communication->doOperation($operation_name, $status_update);
    lock_release('twilio_conf__' . $conference_sid);
  }
  else if (!empty($status_update['CallSid'])) {
    // If this has a call sid and the call is completed and we can't find a conference associated with the call, then
    // make a communication with the CA* id. This will be used for storing the voicemail recording.
    $call_communication_id = db_select('field_data_communication_twilio_conference', 'c')
      ->condition('c.communication_twilio_conference_value', $status_update['CallSid'])
      ->fields('c', ['entity_id'])
      ->range(0, 1)
      ->execute()
      ->fetchField();
    $participation_communication_id = db_select('party_communication_participation', 'p')
      ->condition('p.participation_id', db_like($status_update['CallSid']).'%', 'LIKE')
      ->fields('p', ['id'])
      ->range(0, 1)
      ->execute()
      ->fetchField();

    // If nothing was found.
    if (
      $status_update['CallStatus'] === 'completed' &&
      !$call_communication_id &&
      !$participation_communication_id
    ) {
      // Find the appropriate communication type.
      // Get the first communication type of the twilio conference mode.
      $type = db_select('party_communication_type', 't')
        ->fields('t', ['name'])
        ->condition('mode', 'twilio_conference')
        ->orderBy('name')
        ->execute()->fetchField();

      /** @var \PartyCommunication $communication */
      $communication = entity_create('party_communication', ['type' => $type]);
      // Set to internal intially. This will be updated when the external participant joins.
      $communication->timestamp = REQUEST_TIME;
      $communication->inoutbound = $status_update['Direction'] === 'inbound' ?
        PARTY_COMMUNICATION_INBOUND : PARTY_COMMUNICATION_OUTBOUND;
      $communication->communication_twilio_conference[LANGUAGE_NONE][0]['value'] = $status_update['CallSid'];
      $communication->communication_twilio_number[LANGUAGE_NONE][0]['value'] = $status_update['Direction'] === 'inbound' ?
        $status_update['Called'] : $status_update['Caller'];

      $caller_primitive = str_starts_with($status_update['Caller'], 'client:') ? 'twilio_client' : 'phone';
      $caller_contact_data = str_starts_with($status_update['Caller'], 'client:') ? substr($status_update['Caller'], 7) : $status_update['Caller'];

      if ($caller_primitive !== 'phone' || strtolower($caller_contact_data) !== 'anonymous') {
        $acquisition_context = [
          'name' => 'party_communication_inbound',
          'class' => 'PartyCommunicationContactAcquisition',
          'primitive' => $caller_primitive,
          'communication' => $communication,
          'behavior' => \PartyAcquisitionInterface::BEHAVIOR_NOTHING,
          'party_hat' => [
            'add' => [
              'party_indiv',
            ],
          ],
        ];
        $caller_party = party_acquire([$caller_primitive => $caller_contact_data], $acquisition_context, $method);
        if ($method == 'create') {
          $caller_party->save();
          foreach ($caller_party->data_set_controllers as $controller) {
            $controller->save(TRUE);
          }
        }
      }

      $communication->addParticipation(PartyCommunicationParticipation::create(
        $communication,
        'caller',
        $status_update['CallSid'] . '_from',
        PartyCommunicationContact::createData($caller_primitive, $caller_contact_data),
        $status_update['CallerName'] ?? ($status_update['CallInfo']['callerName'] ?: NULL),
        !empty($caller_party) ? $caller_party->identifier() : NULL,
        'completed',
      ));

      $called_primitive = str_starts_with($status_update['Called'], 'client:') ? 'twilio_client' : 'phone';
      $called_contact_data = str_starts_with($status_update['Called'], 'client:') ? substr($status_update['Called'], 7) : $status_update['Called'];

      if ($called_primitive !== 'phone' || strtolower($called_contact_data) !== 'anonymous') {
        $acquisition_context = [
          'name' => 'party_communication_inbound',
          'class' => 'PartyCommunicationContactAcquisition',
          'primitive' => $called_primitive,
          'communication' => $communication,
          'behavior' => \PartyAcquisitionInterface::BEHAVIOR_NOTHING,
          'party_hat' => [
            'add' => [
              'party_indiv',
            ],
          ],
        ];
        $called_party = party_acquire([$called_primitive => $called_contact_data], $acquisition_context, $method);
        if ($method == 'create') {
          $called_party->save();
          foreach ($called_party->data_set_controllers as $controller) {
            $controller->save(TRUE);
          }
        }
      }
      $communication->addParticipation(PartyCommunicationParticipation::create(
        $communication,
        'recipient',
        $status_update['CallSid'] . '_to',
        PartyCommunicationContact::createData($called_primitive, $called_contact_data),
        NULL,
        !empty($called_party) ? $called_party->identifier() : NULL,
        'completed',
      ));

      $communication->save();
    }

    /** @var \PartyCommunication $communication */
    $communication = $communication ?:
      ($call_communication_id || $participation_communication_id ?
        entity_load_single('party_communication', $call_communication_id ?: $participation_communication_id) :
        NULL);
    if (isset($status_update['RecordingSid']) && $communication) {
      $sid = $communication->communication_twilio_conference[LANGUAGE_NONE][0]['value'];
      while (!lock_acquire('twilioconf__' . $sid)) {
        // If we haven't attempted downloading the file yet, do so now to save time later.
        if (empty($status_update['file'])) {
          /** @var \PartyCommunication $communication */
          $status_update['file'] = $communication->getType()->getController()->downloadRecording(
            $communication,
            $status_update
          );
        }

        lock_wait('twilioconf__' . $sid, 1);
      }

      $communication = entity_load_unchanged('party_communication', $communication->id);
      $communication->doOperation('apply_recording_status_update', $status_update);

      lock_release('twilioconf__' . $sid);
    }
  }

  drupal_add_http_header('Status', '200');
  drupal_page_header();
  print "OK";
  drupal_exit();
}

/**
 * Get a new voice access token.
 */
function party_communication_twilio_api_get_voice_accesstoken() {
  global $user;

  $party = party_user_get_party($user);
  /** @var \PartyCommunicationModeTwilioConference $controller */
  $controller = party_communication_mode_controller('twilio_conference');
  if ($token = $controller->generateVoiceAccessToken($party)) {
    drupal_add_http_header('Status', '200');
    drupal_page_header();
    print $token->toJWT();
    drupal_exit();
  }

  drupal_add_http_header('Status', '403');
  drupal_page_header();
  drupal_exit();
}

/**
 * Implements hook_form_alter() for party_communication_add_participation_form.
 */
function party_communication_twilio_form_party_communication_add_participation_form_alter(&$form, &$form_state) {
  if ($form_state['communication']->mode === 'twilio_conference' && $form_state['communication']->status !== 'complete') {
    $form['actions']['submit']['#submit'][] = 'party_communication_twilio_conference_add_participation_form_submit';
  }
}

/**
 * Submit the form and automatically and the participant to the form.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array.
 */
function party_communication_twilio_conference_add_participation_form_submit($form, &$form_state) {
  /** @var \PartyCommunication $communication */
  $communication = entity_load_unchanged('party_communication', $form_state['communication']->id);
  if ($communication->status === 'active' && $communication->validateOperation('add_participant', ['participation' => $form_state['participation']->id()])) {
    $communication->doOperation('add_participant', ['participation' => $form_state['participation']->id()]);
  }
}

