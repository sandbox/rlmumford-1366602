<?php
/**
 * @file
 * Integration functions for the party communication stuff.
 */

/**
 * Implements hook_party_communication_contact_source_info().
 */
function party_communication_twilio_party_communication_contact_source_info() {
  $sources = array();
  return $sources;
}

/**
 * Implements hook_party_communication_contact_primitive_info().
 */
function party_communication_twilio_party_communication_contact_primitive_info() {
  $primitives = array();
  $primitives['twilio_client'] = [
    'label' => t('Twilio Client'),
    'plural label' => t('Twilio Clients'),
    'data_class' => PartyCommunicationContactDataTwilioClient::class,
  ];
  $primitives['sip_address'] = [
    'label' => t('SIP Address'),
    'plural label' => t('SIP Addresses'),
  ];
  $primitives['twilio_sim_sid'] = [
    'label' => t('Twilio Sim Sid'),
    'plural label' => t('Twilio Sim Sids'),
  ];
  return $primitives;
}
