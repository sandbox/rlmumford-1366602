/**
 * @file
 * Count the number of messages this will take.
 */

(function($) {
  var tml = tml || {};

  /**
   * Code below is based on:
   *   Character Count Plugin - jQuery plugin
   *   Dynamic character count for text areas and input fields
   *   written by Alen Grakalic
   *   http://cssglobe.com/post/7161/jquery-plugin-simplest-twitterlike-dynamic-character-count-for-textareas
   *
   *  @param obj
   *    a jQuery object for input elements
   *  @param options
   *    an array of options.
   *  @param count
   *    In case obj.val() wouldn't return the text to count, this should
   *    be passed with the number of characters.
   */
  tml.calculate = function(obj, options, count, wysiwyg, getter, setter) {
    var counter = $('#' + obj.attr('id') + '-' + options.css);
    var limit = 160;

    if (count == undefined) {
      count = tml.twochar_lineending(obj.val()).length;
    }

    var available = limit - (count % limit);
    var messages = parseInt(count/limit) + 1;

    if (available <= options.warning) {
      counter.addClass(options.cssWarning);
    }
    else {
      counter.removeClass(options.cssWarning);
    }

    counter.html(options.counterText.replace('@messages', messages).replace('@remaining', available));
  };

  /**
   * Replaces line ending with to chars, because PHP-calculation counts with two chars
   * as two characters.
   *
   * @see http://www.sitepoint.com/blogs/2004/02/16/line-endings-in-javascript/
   */
  tml.twochar_lineending = function(str) {
    return str.replace(/(\r\n|\r|\n)/g, "\r\n");
  };

  Drupal.behaviors.party_communication_twilio_message_length = {
    attach: function(context, settings) {
      $('.field-name-communication-twilio-message textarea', context).once('twilio-message-length', function() {
        $(this).twilioCharCount();
      });
    }
  };

  $.fn.twilioCharCount = function() {
    var defaults = {
      warning: 10,
      css: 'counter',
      counterElement: 'div',
      cssWarning: 'messages warning',
      cssExceeded: 'error',
      counterText: Drupal.t('SMS limited to 160 characters per message. Number of messages so far: <strong>@messages</strong>. Next message in <strong>@remaining</strong> characters.'),
      action: 'attach'
    };

    var options = defaults;

    var counterElement = $('<' + options.counterElement + ' id="' + $(this).attr('id') + '-' + options.css + '" class="' + options.css + '"></' + options.counterElement + '>');
    if ($(this).next('div.grippie').length) {
      $(this).next('div.grippie').after(counterElement);
    } else {
      $(this).after(counterElement);
    }

    tml.calculate($(this), options);
    $(this).keyup(function() {
      tml.calculate($(this), options);
    });
    $(this).change(function() {
      tml.calculate($(this), options);
    });
  }
}(jQuery));
