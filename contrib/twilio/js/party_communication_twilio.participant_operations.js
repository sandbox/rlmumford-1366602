;(function ($) {
  Drupal.behaviors.party_communication_twilio_participant_operations = {
    attach: function(context) {
      $("a.participation-operation.participation-operation-remove", context).once("initiate-confirm", function() {
        $(this).unbind("click").click(function (e) {
          e.preventDefault();

          var element_settings = {};
          element_settings.progress = { "type": "throbber" };
          if ($(this).attr("href")) {
            element_settings.url = $(this).attr("href");
            element_settings.event = "activate-operation";
          }

          const base = $(this).attr("id");
          const ajax = new Drupal.ajax(base, this, element_settings);
          const element = this;
          if (typeof $.confirm === "function") {
            $.confirm({
              title: "End the call for everyone?",
              animation: "zoom",
              closeAnimation: "zoom",
              draggable: true,
              boxWidth: "25%",
              buttons: {
                confirm: {
                  text: "Yes, end the call.",
                  btnClass: "btn-green",
                  keys: ["enter"],
                  action: function() {
                    ajax.options.url += "&end_conference=1";
                    $(element).trigger("activate-operation");
                  }
                },
                cancel: {
                  text: "No",
                  btnClass: "btn-red",
                  keys: ["esc"],
                  action: function() {
                    ajax.options.url += "&end_conference=0";
                    $(element).trigger("activate-operation");
                  }
                }
              }
            });
          }
          else if (window.confirm("End the call for everyone?")) {
            ajax.options.data.end_conference = true;
            $(this).trigger("activate-operation");
          }
          else {
            ajax.options.data.end_conference = false;
            $(this).trigger("activate-operation");
          }
        });
      });
    }
  };
})(jQuery);
