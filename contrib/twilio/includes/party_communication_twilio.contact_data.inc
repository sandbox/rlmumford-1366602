<?php

class PartyCommunicationContactDataTwilioClient extends PartyCommunicationContactData {

  /**
   * {@inheritdoc}
   */
  public function renderData($format = self::FORMAT_INLINE) {
    if ($format === self::FORMAT_INLINE) {
      return [
        '#theme' => 'html_tag',
        '#tag' => 'span',
        '#value' => str_replace(['_40', '_2E'], ['@', '.'], $this->data),
        '#attributes' => [
          'class' => ['contact-data', 'contact-data-' . str_replace('_', '-', $this->primitive)],
        ],
      ];
    }
    else if ($format === self::FORMAT_PLAIN) {
      return str_replace(['_40', '_2E'], ['@', '.'], $this->data);
    }
  }

}
