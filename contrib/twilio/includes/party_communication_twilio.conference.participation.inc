<?php

class PartyCommunicationTwilioConferenceParticipation extends PartyCommunicationParticipation {

  /**
   * Override the status to be initialized as pending.
   *
   * @var string|null
   */
  protected ?string $status = 'pending';

  /**
   * {@inheritdoc}
   */
  public function __construct(
    string $id,
    string $role,
    ?PartyCommunicationContactDataInterface $contact_data = NULL,
    ?string $contact_name = NULL,
    ?int $party_id = NULL,
    ?string $status = 'pending',
    array $data = []
  ) {
    parent::__construct($id, $role, $contact_data, $contact_name, $party_id, $status ?: 'pending', $data);
  }

  /**
   * {@inheritdoc}
   */
  public function render($format = self::FORMAT_INLINE, $options = []): array {
    $build = parent::render($format, $options);

    if ($format === static::FORMAT_INLINE) {
      $build['status'] = [
        '#theme' => 'html_tag',
        '#tag' => 'span',
        '#value' => $this->status(),
        '#attributes' => [
          'class' => ['participation-status'],
        ],
      ];

      $build['operations'] = [
        '#theme' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => [
          'class' => ['participation-ops'],
        ],
      ];

      if (
        $this->status() === 'pending' && $this->getCommunication()->status !== 'complete' &&
        party_communication_access("edit", $this->getCommunication()) &&
        party_communication_access("add_participant", $this->getCommunication())
      ) {
        $build['operations']['add_participant'] = [
          '#theme' => 'link',
          '#path' => '/api/party_communication/' . $this->getCommunication()->identifier() . '/op/add_participant',
          '#text' => 'call',
          '#options' => [
            'query' => [
              'participation' => $this->id(),
            ],
            'attributes' => [
              'class' => ['material-icons'],
            ],
          ],
        ];
      }
      // @todo: Mute and hold.
      if (
        $this->getCommunication()->status !== 'complete' &&
        in_array($this->status(), ['connecting', 'connected', 'ringing']) &&
        party_communication_access("edit", $this->getCommunication()) &&
        party_communication_access("remove_participant", $this->getCommunication())
      ) {
        $build['operations']['mute_participant'] = [
          '#theme' => 'link',
          '#path' => '/api/party_communication/' . $this->getCommunication()->identifier() . '/op/' . (!empty($this->data['config']['muted']) ? 'unmute_participant' : 'mute_participant'),
          '#text' => (!empty($this->data['config']['muted']) ? 'mic_off' : 'mic'),
          '#options' => [
            'query' => [
              'participation' => $this->id(),
            ],
            'attributes' => [
              'class' => ['material-icons', 'use-ajax', 'participation-operation', 'participation-operation-mute', !empty($this->data['config']['muted']) ? 'participation-muted' : 'participation-not-muted'],
              'title' => (!empty($this->data['config']['muted']) ? t('Unmute') : t('Mute'))
            ],
          ],
        ];
        $build['operations']['hold_participant'] = [
          '#theme' => 'link',
          '#path' => '/api/party_communication/' . $this->getCommunication()->identifier() . '/op/' . (!empty($this->data['config']['hold']) ? 'unhold_participant': 'hold_participant'),
          '#text' => 'phone_paused',
          '#options' => [
            'query' => [
              'participation' => $this->id(),
            ],
            'attributes' => [
              'class' => ['material-icons', 'use-ajax', 'participation-operation', 'participation-operation-hold', (!empty($this->data['config']['hold']) ? 'participation-on-hold': 'participation-not-on-hold')],
              'title' => (!empty($this->data['config']['hold']) ? 'Resume': 'Hold')
            ],
          ],
        ];
        $build['operations']['remove_participant'] = [
          '#theme' => 'link',
          '#path' => '/api/party_communication/' . $this->getCommunication()->identifier() . '/op/remove_participant',
          '#text' => 'call_end',
          '#options' => [
            'query' => [
              'participation' => $this->id(),
           ],
            'attributes' => [
              'class' => ['material-icons', 'participation-operation', 'participation-operation-remove'],
              'title' => t('Remove Participant'),
            ],
          ],
        ];
        $build['operations']['#attached']['js'][] = drupal_get_path('module', 'party_communication_twilio') . '/js/party_communication_twilio.participant_operations.js';
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['#tree'] = TRUE;
    $form['config'] = [
      '#type' => 'container',
    ];
    $form['config']['endConferenceOnExit'] = [
      '#type' => 'checkbox',
      '#title' => t('End Conference on Exit'),
      '#description' => t('End the conference call for everyone when this participant leaves the call.'),
      '#default_value' => !isset($this->data['config']['endConferenceOnExit']) || !empty($this->data['config']['endConferenceOnExit']),
      '#access' => $this->status === 'pending',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array $form, array &$form_state) {
    parent::submitForm($form, $form_state);

    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents'] ?? []);
    $this->data['config']['endConferenceOnExit'] = !empty($values['config']['endConferenceOnExit']);
  }

  /**
   * {@inheritdoc}
   */
  public static function structurePropertyInfo() : array {
    $info = parent::structurePropertyInfo();

    $info['data']['property info']['customParameters'] = [
      'type' => 'structure',
      'label' => 'Custom Parameters',
      'property info' => [],
      'property info alter' => [static::class, 'dataStructurePropertyInfoAlter'],
      'getter callback' => 'entity_property_verbatim_get',
    ];
    $info['data']['property info']['config'] = [
      'type' => 'structure',
      'label' => t('Configuration'),
      'property info' => [
        'autoAnswerCall' => [
          'type' => 'boolean',
          'label' => t('Auto Answer call?'),
          'getter callback' => 'entity_property_verbatim_get',
        ],
        'endConferenceOnExit' => [
          'type' => 'boolean',
          'label' => t('End conference on exit?'),
          'getter callback' => 'entity_property_verbatim_get',
        ],
        'muted' => [
          'type' => 'boolean',
          'label' => t('Is Muted?'),
          'getter callback' => 'entity_property_verbatim_get',
        ],
        'hold' => [
          'type' => 'boolean',
          'label' => t('Is on Hold?'),
          'getter callback' => 'entity_property_verbatim_get',
        ],
      ],
      'getter callback' => 'entity_property_verbatim_get',
    ];
    $info['data']['property info']['call_sid'] = [
      'type' => 'text',
      'label' => 'Call Sid',
      'getter callback' => 'entity_property_verbatim_get',
    ];
    $info['data']['property info']['joined'] = [
      'type' => 'date',
      'label' => 'Joined Date & Time',
      'getter callback' => 'entity_property_verbatim_get',
    ];
    $info['data']['property info']['left'] = [
      'type' => 'date',
      'label' => 'Left Date & Time',
      'getter callback' => 'entity_property_verbatim_get',
    ];

    return $info;
  }

}
