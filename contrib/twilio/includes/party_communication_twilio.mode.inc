<?php

/**
 * @file
 *   Contains mode class for twilio.
 */

/**
 * Trait for methods shared between the twilio modes.
 */
trait PartyCommunicationTwilioModeTrait {

  /**
   * Get the twilio credentials.
   *
   * @param \PartyCommunication $communication
   *   The twilio credentials.
   *
   * @return array
   */
  protected function getTwilioCredentials(PartyCommunication $communication) {
    $credentials = [
      $communication->getType()->data['twilio_account'] ?: variable_get('twilio_account', FALSE),
      $communication->getType()->data['twilio_token'] ?: variable_get('twilio_token', FALSE),
    ];
    drupal_alter('party_communication_twilio_credentials', $credentials, $communication);
    return $credentials;
  }

  /**
   * Create a client.
   *
   * @param \PartyCommunication $communication
   *
   * @return \Twilio\Rest\Client
   * @throws \Twilio\Exceptions\ConfigurationException
   */
  public function getClient(PartyCommunication $communication) : \Twilio\Rest\Client {
    [$account, $token] = $this->getTwilioCredentials($communication);
    return new \Twilio\Rest\Client($account, $token);
  }

  /**
   * Create shared twilio fields.
   *
   * @param $bundle
   *
   * @return void
   * @throws \FieldException
   */
  public function createTwilioFields($bundle) {
    if (!field_info_field('communication_twilio_account')) {
      $field = array(
        'field_name' => 'communication_twilio_account',
        'type' => 'text',
        'cardinality' => 1,
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_twilio_account', $bundle)) {
      $instance = array(
        'field_name' => 'communication_twilio_account',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Account SID'),
        'settings' => array(
          'text_processing' => 0,
        ),
      );
      field_create_instance($instance);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, &$form_state) {
    $type = $form_state['party_communication_type'];

    $form['twilio_account'] = [
      '#type' => 'textfield',
      '#title' => t('Twilio Account SID'),
      '#description' => t('The Twilio Account SID, you can find this on your console. Leave blank to use the twilio module default.'),
      '#default_value' => !empty($type->data['twilio_account']) ? $type->data['twilio_account'] : '',
    ];

    $form['twilio_token_input'] = [
      '#type' => 'password',
      '#title' => t('Twilio Auth Token'),
      '#description' => t(
        'The current token is !pass, leave blank to keep using this token.',
        [
          '!pass' => !empty($type->data['twilio_token']) ? str_repeat("*", strlen($type->data['twilio_token'])) : 'empty',
        ]
      ),
      '#element_validate' => [[$this, 'passwordInputValidate']],
    ];
    $form['twilio_token'] = [
      '#type' => 'value',
      '#value' => !empty($type->data['twilio_token']) ? $type->data['twilio_token'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    $type = $form_state['party_communication_type'];

    $type->data['twilio_account'] = $values['twilio_account'];
    $type->data['twilio_token'] = $values['twilio_token'];
  }

}

/**
 * Twilio SMS Mode
 */
class PartyCommunicationModeTwilio extends PartyCommunicationModeDefault {
  use PartyCommunicationTwilioModeTrait;

  /**
   * {@inheritdoc}
   */
  public function getParticipationRoles(): array {
    return [
      'sender' => [
        'label' => t('From'),
        'supported_primitives' => ['sms', 'twilio_channel_address'],
        'required' => TRUE,
        'cardinality' => 1,
      ],
      'recipient' => [
        'label' => t('To'),
        'supported_primitives' => ['sms', 'twilio_channel_address'],
        'required' => TRUE,
        'cardinality' => 1,
      ],
    ];
  }

  /**
   * {inheritdoc}
   */
  public function createType($bundle) {
    parent::createType($bundle);
    $this->createTwilioFields($bundle);

    if (!field_info_field('communication_twilio_message')) {
      $field = array(
        'field_name' => 'communication_twilio_message',
        'type' => 'text_long',
        'cardinality' => 1,
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_twilio_message', $bundle)) {
      $instance = array(
        'field_name' => 'communication_twilio_message',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Message'),
        'settings' => array(
          'text_processing' => 0,
        ),
      );
      field_create_instance($instance);
    }

    if (!field_info_field('communication_twilio_number')) {
      $field = array(
        'field_name' => 'communication_twilio_number',
        'type' => 'text',
        'cardinality' => 1,
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_twilio_number', $bundle)) {
      $instance = array(
        'field_name' => 'communication_twilio_number',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Number'),
        'settings' => array(
          'text_processing' => 0,
        ),
      );
      field_create_instance($instance);
    }

    if (!field_info_field('communication_twilio_raw')) {
      $field = array(
        'field_name' => 'communication_twilio_raw',
        'type' => 'text_long',
        'cardinality' => 1,
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_twilio_raw', $bundle)) {
      $instance = array(
        'field_name' => 'communication_twilio_raw',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Raw Response From Twilio'),
        'settings' => array(
          'text_processing' => 0,
        ),
      );
      field_create_instance($instance);
    }

    if (!field_info_field('communication_twilio_media')) {
      $field = array(
        'field_name' => 'communication_twilio_media',
        'type' => 'file',
        'cardinality' => 10,
        'settings' => array(
          'uri_scheme' => 'private',
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_twilio_media', $bundle)) {
      $instance = array(
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'field_name' => 'communication_twilio_media',
        'label' => t('Media'),
        'description' => t('Media attached to the message.'),
        'settings' => array(
          'file_extensions' => 'jpg jpeg gif png txt doc docx xls xlsx pdf csv zip gz',
          'file_directory' => 'communication/twilio/media',
        ),
      );
      field_create_instance($instance);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function send($communication, $settings = array()) {
    if (empty($settings['skip_validation']) && !$this->validateSend($communication, $settings, $reasons)) {
      $communication->status = 'failed';
      $communication->is_new_revision = TRUE;
      $communication->revision_log = implode("\n", $reasons);
      $communication->save();
      return;
    }

    // Apply credentials.
    [$account, $auth_token] = $this->getTwilioCredentials($communication);
    if ($account) {
      $settings['twilio_account'] = $account;
    }
    if ($auth_token) {
      $settings['twilio_token'] = $auth_token;
    }

    $this->prepare($communication);

    if (!$communication->getParticipation('recipient')) {
      $party = $communication->wrapper()->communication_recipients->value();

      $recipient = PartyCommunicationParticipation::create(
        $communication,
        'recipient',
        'recipient',
        $party ? PartyCommunicationContact::getData($party, 'sms') : NULL,
        $party ? $party->label() : NULL,
        $party ? $party->pid : NULL
      );
      $communication->addParticipation($recipient);
    }

    if (!$communication->getParticipation('sender')) {
      $party = $communication->wrapper()->communication_sender->value();

      $sender = PartyCommunicationParticipation::create(
        $communication,
        'sender',
        'sender',
        PartyCommunicationContact::createData('sms', $settings['twilio_number'] ?: variable_get('twilio_number', NULL)),
        $party ? $party->label() : NULL,
        $party ? $party->pid : NULL
      );
      $communication->addParticipation($sender);
    }

    $message = $this->prepareContent($communication->communication_twilio_message[LANGUAGE_NONE][0]['value'], $communication);
    $message = check_markup($message, $communication->communication_twilio_message[LANGUAGE_NONE][0]['format'] ?: 'plain_text');
    $message = trim(strip_tags($message));
    $message = htmlspecialchars_decode($message, ENT_QUOTES);
    $communication->communication_twilio_message[LANGUAGE_NONE][0]['value'] = $message;
    $communication->communication_twilio_message[LANGUAGE_NONE][0]['safe_value'] = $message;
    $communication->communication_twilio_account[LANGUAGE_NONE][0]['value'] = $account;

    if (!variable_get('party_communication_twilio_enabled', TRUE)) {
      $communication->status = 'failed';
      $communication->revision_timestamp = REQUEST_TIME;
      $communication->revision_log = t('Aborted Sending: Twilio is disabled');
      $communication->is_new_revision = TRUE;
      $communication->save();
    }
    $response = twilio_send(
      $communication->getParticipation('recipient')->contactData()->getData(),
      $message,
      TWILIO_DEFAULT_COUNTRY_CODE,
      NULL,
      array_filter([
        'twilio_number' => $communication->getParticipation('sender')->contactData()->getData(),
      ]) + $settings,
    );
    $communication->communication_twilio_number[LANGUAGE_NONE][0]['value'] = $communication->getParticipation('sender')->contactData()->getData() ?: (!empty($settings['twilio_number']) ? $settings['twilio_number'] : variable_get('twilio_number', FALSE));
    $communication->communication_twilio_raw[LANGUAGE_NONE][0]['value'] = json_encode($response);

    if ($response) {
      $communication->timestamp = time();
      $communication->is_new_revision = TRUE;
      $communication->revision_log = t('Successfully sent sms to twilio.');
      $communication->status = 'sent';
    }
    else {
      $communication->is_new_revision = TRUE;
      $communication->revision_log = t('Failed to send sms to twilio.');
      $communication->status = 'failed';
    }

    $communication->save();
  }

  /**
   * {inheritdoc}
   */
  public function validateSend($communication, $settings = array(), &$reasons = array()) {
    parent::validateSend($communication, $settings, $reasons);

    if (
      $communication->getParticipation('recipient') &&
      !$communication->getParticipation('recipient')->contactData()->getData() &&
      (
        !$communication->wrapper()->communication_recipients->value() ||
        !PartyCommunicationContact::getData(
          $communication->wrapper()->communication_recipients->value(),
          'sms',
        )
      )
    ) {
      $reasons[] = t('The Recipient has no text-able phone number.');
    }

    if (empty($reasons)) {
      return TRUE;
    }

    return FALSE;
  }
}

