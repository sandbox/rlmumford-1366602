<?php

class PartyCommunicationEvent_Speech extends PartyCommunicationEvent {

  /**
   * {@inheritdoc}
   */
  public function render($format = self::FORMAT_INLINE, $options = []): array {
    if ($format === self::FORMAT_PLAIN) {
      $participation_label = $this->participation()->render(PartyCommunicationParticipation::FORMAT_PLAIN);
      return [
        '#markup' => $this->dateTime()->format('Y-m-d H:i:s') . ": " .
          drupal_render($participation_label) . " said \"" .
          ($this->data()['transcript'] ?? '') . "\"",
      ];
    }

    $build = parent::render($format, $options);

    $p_keys = array_keys($this->communication()->getParticipations());
    $p_key = array_search($this->participationId(), $p_keys);
    $build['#attributes']['class'][] = 'party-communication-speech--speaker-' . $p_key;

    if ($speaker_party = party_load($this->participation()->partyId())) {
      foreach ($speaker_party->party_hat[LANGUAGE_NONE] as $hat_item) {
        $build['#attributes']['class'][] = 'party-communication-speech--speaker-hat--' . $hat_item['hat_name'];
      }
    }

    unset($build['label']);
    $build['speech'] = [
      '#type' => 'html_tag',
      '#tag' => $format === self::FORMAT_INLINE ? 'span' : 'p',
      '#attributes' => [
        'class' => ['party-communication-event-speech'],
      ],
      '#value' => $this->data()['transcript'] ?? '',
    ];

    return $build;
  }

}
