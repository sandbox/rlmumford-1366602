<?php

/**
 * @file
 *  Party Communication Hooks.
 */

/**
 * Implements hook_party_communication_mode_info().
 */
function party_communication_twilio_party_communication_mode_info() {
  $modes = array();
  $modes['twilio'] = array(
    'label' => t('Twilio SMS'),
    'class' => 'PartyCommunicationModeTwilio',
  );

  $modes['twilio_conference'] = [
    'label' => t('Twilio Voice Conference'),
    'class' => 'PartyCommunicationModeTwilioConference',
  ];

  return $modes;
}

/**
 * Implements hook_party_communication_status_info().
 */
function party_communication_twilio_party_communication_status_info() {
  $statuses = [];
  $statuses['active'] = [
    'label' => t('Active'),
    'manual' => TRUE,
    'modes' => [
      'twilio_conference' => 'twilio_conference',
    ],
  ];
  $statuses['complete'] = [
    'label' => t('Completed'),
    'manual' => TRUE,
    'participation_locked' => TRUE,
    'modes' => [
      'twilio_conference' => 'twilio_conference',
    ],
  ];
  return $statuses;
}
