<?php
/**
 * @file
 *   Contains the mode class for PDFs.
 */

class PartyCommunicationModeFaxage extends PartyCommunicationModeDefault {

  /**
   * {@inheritdoc}
   */
  public function getParticipationRoles(): array {
    return [
      'recipient' => [
        'label' => t('Recipient'),
        'supported_primitives' => ['fax'],
        'required' => TRUE,
        'cardinality' => 1,
      ],
      'sender' => [
        'label' => t('Sender'),
        'supported_primitives' => ['fax'],
        'cardinality' => 1,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function createType($bundle) {
    parent::createType($bundle);

    if (!field_info_field('communication_files')) {
      $field = array(
        'field_name' => 'communication_files',
        'type' => 'file',
        'cardinality' => FIELD_CARDINALITY_UNLIMITED,
        'settings' => array(
          'uri_scheme' => 'private',
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_files', $bundle)) {
      $instance = array(
        'field_name' => 'communication_files',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Content'),
        'description' => t('The files that make up the content of the fax.'),
        'settings' => array(
          'file_extensions' => 'jpg jpeg pdf ps tif tiff doc dot docx odt rtf xls xlsx ods csv htm html bmp gif pcl txt',
          'file_directory' => 'party_communication_faxage',
        ),
        'display' => array(
          'default' => array(
            'label' => 'inline',
          ),
        ),
      );
      field_create_instance($instance);
    }

    // Fax Number Field
    if (!field_info_field('communication_fax_number')) {
      $field = array(
        'field_name' => 'communication_fax_number',
        'type' => 'text',
        'cardinality' => 1,
        'settings' => array(
          'max_length' => 14,
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_fax_number', $bundle)) {
      $instance = array(
        'field_name' => 'communication_fax_number',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Fax Number'),
        'description' => t('The number to send the fax to.'),
      );
      field_create_instance($instance);
    }

    // Fax Recipient
    if (!field_info_field('communication_fax_recipient')) {
      $field = array(
        'field_name' => 'communication_fax_recipient',
        'type' => 'text',
        'cardinality' => 1,
        'settings' => array(
          'max_length' => 32,
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_fax_recipient', $bundle)) {
      $instance = array(
        'field_name' => 'communication_fax_recipient',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Recipient Name'),
        'description' => t('The name of the recipient sent to faxage.'),
      );
      field_create_instance($instance);
    }

    // Faxage Job ID
    if (!field_info_field('communication_fax_id')) {
      $field = array(
        'field_name' => 'communication_fax_id',
        'type' => 'number_integer',
        'cardinality' => 1,
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_fax_id', $bundle)) {
      $instance = array(
        'field_name' => 'communication_fax_id',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Fax Job ID'),
      );
      field_create_instance($instance);
    }

    // Raw Response Field
    if (!field_info_field('communication_fax_response')) {
      $field = array(
        'field_name' => 'communication_fax_response',
        'type' => 'text_long',
        'cardinality' => 1,
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_fax_response', $bundle)) {
      $instance = array(
        'field_name' => 'communication_fax_response',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Faxage Webservice Response'),
        'description' => t('The response from the faxage webservice.'),
        'settings' => array(
          'text_processing' => 0,
        ),
      );
      field_create_instance($instance);
    }

    // Job Status.
    if (!field_info_field('communication_fax_status')) {
      $field = array(
        'field_name' => 'communication_fax_status',
        'type' => 'list_text',
        'cardinality' => 1,
        'settings' => array(
          'allowed_values' => array(
            'pending' => t('Pending'),
            'success' => t('Success'),
            'failure' => t('Failure'),
          ),
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_fax_status', $bundle)) {
      $instance = array(
        'field_name' => 'communication_fax_status',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Fax Job Status'),
        'description' => t('The Fax Job Status'),
      );
      field_create_instance($instance);
    }

    // Fax coversheet body.
    if (!field_info_field('communication_fax_cover_body')) {
      $field = array(
        'field_name' => 'communication_fax_cover_body',
        'type' => 'text_long',
        'cardinality' => 1,
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_fax_cover_body', $bundle)) {
      $instance = array(
        'field_name' => 'communication_fax_cover_body',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Cover Sheet Comment'),
      );
      field_create_instance($instance);
    }
  }

  /**
   * Generate a coversheet pdf.
   *
   * @param PartyCommunication $communication
   * @param array $settings
   *
   * @return string|boolean
   *   The file path for the coversheet or false if none was generated.
   */
  public function generateCoverSheet($communication, $settings = array()) {
    if (empty($communication->label) && empty($communication->communication_fax_cover_body[LANGUAGE_NONE][0])) {
      return FALSE;
    }

    $cover_dir = 'temporary://communication_fax_cover_sheets';
    file_prepare_directory($cover_dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

    $cover_file_name = 'cover-fax-'.$communication->id.'-'.REQUEST_TIME;
    $cover_html_file = drupal_realpath("{$cover_dir}/{$cover_file_name}.html");
    $cover_pdf_file = drupal_realpath("{$cover_dir}/{$cover_file_name}.pdf");
    $cover_log_file = drupal_realpath("{$cover_dir}/{$cover_file_name}.log");

    $type = $communication->getType();
    $default_cover_wk_options = variable_get('party_communication_fax_cover_wkhtmltopdf_defaults', array(
      // Quality adjustments.
      '--disable-smart-shrinking',
      '--print-media-type',
      // Disable borders.
      '-T 0',
      '-R 0',
      '-B 0',
      '-L 0',
      // Disable outlines.
      '--no-outline',
      '--outline-depth 0',
    ));
    $cover_wk_options = !empty($type->data['cover']['wk_options']) ? $type->data['cover']['wk_options'] : $default_cover_wk_options;
    $cover_wk_options = implode(' ', $cover_wk_options);
    $cover_wk_options = preg_replace("/\r|\n/", "", $cover_wk_options);

    $cover_css = variable_get('party_communication_fax_cover_css_default', '
        div.cover-sheet-wrapper {
          height: 200px;
          width: 400px;
          position: fixed;
          top: 50%;
          left: 50%;
          margin-top: -100px;
          margin-left: -200px;
        }
        h1.cover-sheet-title {
          text-align: center;
        }
      ');
    if (!empty($type->data['cover']['css'])) {
      $cover_css .= $type->data['cover']['css'];
    }

    $cover_comment = '';
    if (!empty($communication->communication_fax_cover_body[LANGUAGE_NONE][0])) {
      $item = $communication->communication_fax_cover_body[LANGUAGE_NONE][0];
      $cover_comment = check_markup($item['value'], $item['format'], LANGUAGE_NONE);
    }

    $cover_content = '<html>
      <head><style>'.$cover_css.'</style></head>
      <body>
        <div style="width: 100%; height: 100%">
          <div class="cover-sheet-wrapper">
            <h1 class="cover-sheet-title">'.$communication->label.'</h1>
            '.$cover_comment.'
          </div>
        </div>
        </body></html>';
    file_put_contents($cover_html_file, $cover_content);

    $script = "wkhtmltopdf {$cover_wk_options} \"{$cover_html_file}\" \"{$cover_pdf_file}\" &> \"{$cover_log_file}\"";
    exec($script);

    if (!file_exists($cover_pdf_file)) {
      $message = 'Failed to generate fax cover sheet.';
      watchdog('party_communication_fax', $message, array(), WATCHDOG_ERROR);
      return FALSE;
    }

    return $cover_pdf_file;
  }

  /**
   * Settings send.
   */
  public function settingsSend($communication, $settings = array()) {
    return $communication->getType()->data;
  }

  /**
   * {@inheritdoc}
   */
  public function validateSend($communication, $settings = array(), &$reasons = array()) {
    parent::validateSend($communication, $settings, $reasons);

    if (empty($communication->communication_files)) {
      $reasons[] = t('There are no files to fax.');
    }
    if (!$this->getFaxNumber($communication)) {
      $reasons[] = t('No valid fax numbers.');
    }

    if (empty($reasons)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function send($communication, $settings = array()) {
    try {
      if (empty($settings['skip_validation']) && !$this->validateSend($communication, $settings, $reasons)) {
        throw new Exception("Failed Validation: \n".implode("\n", $reasons));
      }
      $this->prepare($communication);
      $type = $communication->getType();
      $wrapper = entity_metadata_wrapper('party_communication', $communication);

      $this->setHost($communication, $settings);

      if (!variable_get('party_communication_faxage_enabled', TRUE)) {
        $communication->status = 'failed';
        $communication->revision_timestamp = REQUEST_TIME;
        $communication->revision_log = t('Aborted Sending: Faxage is disabled');
        $communication->is_new_revision = TRUE;
        $communication->save();
      }

      $params = array(
        'username' => $settings['username'],
        'company' => $settings['company'],
        'password' => $settings['password'],
        'recipname' => $wrapper->communication_fax_recipient->value(),
        'faxno' => $wrapper->communication_fax_number->value(),
        'operation' => 'sendfax',
        'faxfilenames' => array(),
        'faxfiledata' => array(),
      );

      // Prepend the coversheet if we can generate one.
      if ($cover_sheet = $this->generateCoverSheet($communication, $settings)) {
        $params['faxfilenames'][] = basename($cover_sheet);
        $params['faxfiledata'][] = base64_encode(file_get_contents($cover_sheet));
      }

      // Add the fields.
      foreach ($wrapper->communication_files as $file_item) {
        $params['faxfilenames'][] = $file_item->file->name->value();
        $params['faxfiledata'][] = base64_encode(file_get_contents($file_item->file->value()->uri));
      }
      $test = $response = $this->request('httpsfax.php', $params);
      if (substr($test, 0, 3) == 'ERR') {
        throw new Exception($response);
      }
      $communication->status = 'sent';
      $communication->timestamp = REQUEST_TIME;
      $wrapper->communication_fax_response = $response;
      $wrapper->communication_fax_id = (int) substr($response, 7);
      $wrapper->communication_fax_status = 'pending';
    }
    catch (Exception $e) {
      $communication->status = 'failed';
      $wrapper->communication_fax_response = $e->getMessage();
    }
    finally {
      $communication->revision_timestamp = REQUEST_TIME;
      $communication->revision_log = t('Attempted Sending Through API');
      $communication->is_new_revision = TRUE;
      $communication->save();
    }
  }

  /**
   * Prepare.
   */
  public function prepare($communication) {
    parent::prepare($communication);

    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    $wrapper->communication_fax_number = $this->getFaxNumber($communication);
    $wrapper->communication_fax_recipient = $this->getFaxRecipient($communication);
  }

  /**
   * Update the status.
   */
  public function updateStatus($communication, $settings = array()) {
    try {
      $this->setHost($communication);
      $wrapper = entity_metadata_wrapper('party_communication', $communication);

      $params = array(
        'username' => $settings['username'],
        'company' => $settings['company'],
        'password' => $settings['password'],
        'operation' => 'status',
        'extqueue' => 1,
        'jobid' => $wrapper->communication_fax_id->value(),
      );
      $test = $response = $this->request('httpsfax.php', $params);
      if (substr($test, 0, 3) == 'ERR') {
        throw new Exception($response);
      }

      $bits = explode("\t", $response);
      $wrapper->communication_fax_status = $bits[4];

      if ($bits[4] == 'failure') {
        $wrapper->communication_fax_response = $wrapper->communication_fax_response->value()
          . "\n"
          . $bits[5];
      }

      $communication->revision_timestamp = REQUEST_TIME;
      $communication->revision_log = t('Checked FAX Status');
      $communication->is_new_revision = TRUE;
      $communication->save();

      if (empty($settings['no_message'])) {
        drupal_set_message(
          t("Status: @status.", array('@status' => $bits[4])),
          $bits[4] == 'failure' ? 'error' : 'status'
        );
      }
    }
    catch (Exception $e) {
      if (empty($settings['no_message'])) {
        drupal_set_message(t('Status Update Failed'), 'error');
      }
      watchdog_exception('party_communication', $e);
    }
  }

  /**
   * Validate whether we can update the status.
   */
  public function validateUpdateStatus($communication, $settings = array(), &$reasons = array()) {
    if ($communication->status != 'sent') {
      $reasons[] = t('Fax not sent yet');
    }

    if (empty($communication->communication_fax_id[LANGUAGE_NONE])) {
      $reasons[] = t('There is no faxage id on this communication.');
    }
  }

  /**
   * Get the addresses.
   */
  public function getFaxNumber($communication) {
    if (!empty($communication->communication_fax_number[LANGUAGE_NONE][0]['value'])) {
      return $communication->communication_fax_number[LANGUAGE_NONE][0]['value'];
    }

    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    $party = $wrapper->communication_recipients;
    try {
      foreach ($party->profile2_party_indiv->field_contact_phone as $phone_wrapper) {
        $number = $phone_wrapper->field_phone_number->value();
        if (!empty($number) && in_array($phone_wrapper->field_phone_type->value(), array(22,23))) {
          return str_replace(array('(',')','-',' '), '', $number);
        }
      }
    }
    catch (Exception $e) { }
    return FALSE;
  }

  /**
   * Get the fax recipient.
   */
  public function getFaxRecipient($communication) {
    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    if ($wrapper->communication_fax_recipient->value()) {
      return $wrapper->communication_fax_recipient->value();
    }

    return substr($wrapper->communication_recipients->label->value(), 0, 32);
  }

  /**
   * Set the host variable.
   */
  public function setHost($communication = NULL) {
    $this->host = 'https://api.faxage.com/';
  }

  /**
   * Do a request.
   */
  public function request($path, $content, $headers = array(), $method = 'POST') {
    if (empty($this->host)) {
      throw new Exception(t('Cannot make request, service host has not been set.'));
    }

    $op = $content['operation'];
    $content = http_build_query($content);
    $request = curl_init($this->host.$path);
    if ($method == 'POST') {
      curl_setopt($request, CURLOPT_POST, TRUE);
      curl_setopt($request, CURLOPT_POSTFIELDS, $content);
    }
    if (!empty($headers)) {
      curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
    }
    curl_setopt($request, CURLOPT_RETURNTRANSFER, TRUE);
    $raw = curl_exec($request);
    curl_close($request);

    if (substr($raw, 0, 5) == 'ERR11') {
      return "";
    }
    else if (substr($raw, 0, 3) == 'ERR') {
      throw new \Exception(t(
        'Faxage operation !op returned an error: !error',
        array(
          '!error' => $raw,
          '!op' => $op,
        )
      ));
    }

    return $raw;
  }

  /**
   * {@inheritdoc}
   */
  public function labelElementTitle($communication) {
    return t('Subject');
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, &$form_state) {
    $type = $form_state['party_communication_type'];

    $form['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Faxage Username'),
      '#description' => t('The Faxage Account Username'),
      '#default_value' => !empty($type->data['username']) ? $type->data['username'] : '',
    );

    $form['company'] = array(
      '#type' => 'textfield',
      '#title' => t('Faxage Company'),
      '#description' => t('The Faxage Account Company'),
      '#default_value' => !empty($type->data['company']) ? $type->data['company'] : '',
    );

    $form['password_input'] = array(
      '#type' => 'password',
      '#title' => t('Faxage Password'),
      '#description' => t(
        'The password is currently !pass, leave blank to continue using this password',
        array(
          '!pass' => !empty($type->data['password']) ? str_repeat("*", strlen($type->data['password'])) : 'empty',
        )
      ),
      '#element_validate' => array(array($this, 'passwordInputValidate')),
    );
    $form['password'] = array(
      '#type' => 'value',
      '#value' => !empty($type->data['password']) ? $type->data['password'] : '',
    );

    $form['inbound'] = array(
      '#type' => 'fieldset',
      '#title' => t('Inbound Settings'),
      '#description' => t('Configuration options for receiving faxes.'),
    );
    if (!empty($type->name) && ($last_check = variable_get('party_communication_faxage_'.$type->name.'_last_check', FALSE))) {
      $form['inbound']['status'] = array(
        '#markup' => t('Faxes were last received at !date', array('!date' => date('g:ia \o\n j F Y', $last_check))),
      );
    }
    $form['inbound']['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Recieve Inbound Faxes'),
      '#default_value' => !empty($type->data['inbound']['enabled']),
    );
    $form['inbound']['frequency'] = array(
      '#type' => 'textfield',
      '#title' => t('Frequency'),
      '#size' => 5,
      '#field_prefix' => t('Check inbox every '),
      '#field_suffix' => t('minutes.'),
      '#default_value' => !empty($type->data['inbound']['frequency']) ? $type->data['inbound']['frequency'] : 10,
      '#description' => t('The fax inbox is checked on cron therefore you must ensure that cron fires atleast as frequently as this value.'),
    );

    $default_cover_wk_options = variable_get('party_communication_fax_cover_wkhtmltopdf_defaults', array(
      // Quality adjustments.
      '--disable-smart-shrinking',
      '--print-media-type',
      // Disable borders.
      '-T 0',
      '-R 0',
      '-B 0',
      '-L 0',
      // Disable outlines.
      '--no-outline',
      '--outline-depth 0',
    ));
    $form['cover'] = array(
      '#type' => 'fieldset',
      '#title' => t('Cover Sheets'),
      '#description' => t('Configuration options for automatically generated cover sheets. The subject/label will be sent as the cover sheet title.'),
    );
    $form['cover']['css'] = array(
      '#type' => 'textarea',
      '#title' => t('Cover Sheet CSS'),
      '#description' => t('CSS for the Cover Sheet pages. The title is an h1 with class "cover-sheet-title" wrapped with a div class "cover-sheet-wrapper". Leave blank to use default CSS.'),
      '#default_value' => !empty($type->data['cover']['css']) ? $type->data['cover']['css'] : '',
    );
    $form['cover']['wk_options'] = array(
      '#type' => 'textarea',
      '#title' => t('Cover Sheet WKHTMLtoPDF Options'),
      '#description' => t(
        'Options to pass to WKHTMLtoPDF. For more information on what options are available !link.',
        array(
          '!link' => l('click here', 'https://madalgo.au.dk/~jakobt/wkhtmltoxdoc/wkhtmltopdf-0.9.9-doc.html'),
        )
      ),
      '#default_value' => implode("\n", (!empty($type->data['cover']['wk_options']) ? $type->data['cover']['wk_options'] : $default_cover_wk_options)),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    $type = $form_state['party_communication_type'];

    $type->data['username'] = $values['username'];
    $type->data['company'] = $values['company'];
    $type->data['password'] = $values['password'];
    $type->data['inbound'] = $values['inbound'];
    $type->data['cover'] = $values['cover'];
  }

  /**
   * {@inheritdoc}
   */
  public function supportedOperations() {
    $ops = parent::supportedOperations();
    if (empty($ops['send']['settingsCallback'])) {
      $ops['send']['settingsCallback'] = array($this, 'settingsSend');
    }

    $ops['check'] = array(
      'label' => t('Check Status'),
      'do' => array($this, 'updateStatus'),
      'validate' => array($this, 'validateUpdateStatus'),
      'settingsCallback' => array($this, 'settingsSend'),
    );

    return $ops;
  }
}
