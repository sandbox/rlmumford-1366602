<?php
/**
 * Plugin for specially rendered content.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Communication faxage Proof'),
  'description' => t('Rendered faxage Proof.'),
  'required context' => new ctools_context_required(t('Communication'), 'party_communication'),
  'category' => t('Communications'),
);

function party_communication_faxage_party_communication_faxage_proof_content_type_render($subtype, $conf, $panel_args, &$context) {
  $communication = $context->data;
  $info = entity_get_info('party_communication');
  $path = $info['admin ui']['path'].'/communication/'.$communication->id.'/proof';
  $output = '<iframe src="/' . $path . '" width="100%" height="800px" id="pdf-preview"></iframe>';

  $block = new stdClass();
  $block->module = 'party_communication_faxage';
  $block->delta = 'proof';
  $block->content = $output;

  return $block;
}

function party_communication_faxage_party_communication_faxage_proof_content_type_admin_title($subtype, $conf, $context) {
  return t('Communication faxage Proof');
}

function party_communication_faxage_party_communication_faxage_proof_content_type_edit_form($form, &$form_state) {
  return $form;
}
