<?php

/**
 * @file
 *  Party Communication Hooks.
 */

/**
 * Implements hook_party_communication_mode_info().
 */
function party_communication_faxage_party_communication_mode_info() {
  $modes = array();
  $modes['faxage'] = array(
    'label' => t('Faxage'),
    'class' => 'PartyCommunicationModeFaxage',
  );

  return $modes;
}
