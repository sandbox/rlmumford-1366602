<?php

/**
 * Form for configuring billing settings.
 */
function party_communication_click2mail_billing_settings_form($form, &$form_state) {
  $form['billing_type'] = array(
    '#type' => 'select',
    '#title' => t('Billing type'),
    '#description' => t('What type of billing should be used.'),
    '#options' => array(
      'Credit Card' => t('Credit Card'),
      'Invoice' => t('Invoice'),
      'ACH' => 'ACH',
      'User Credit' => t('User Credit'),
    ),
    '#default_value' => variable_get('c2m_billing_type', 'ACH'),
  );

  $billing_info = variable_get('c2m_billing_info');
  // Credit Card Parts.
  $variables = array(
    'billingName' => t('Name on Card'),
    'billingCompany' => t('Company'),
    'billingAddress1' => t('Street Address 1'),
    'billingAddress2' => t('Street Address 2'),
    'billingCity' => t('City'),
    'billingState' => t('State'),
    'billingZip' => t('ZIP Code'),
    'billingCcType' => t('Card Type'),
    'billingNumber' => t('Card Number'),
    'billingMonth' => t('Expiration Month'),
    'billingYear' => t('Expiration Year'),
    'billingCvv' => t('CVV Code'),
  );

  $weight = 10;
  foreach ($variables as $field_name => $label) {
    $form[$field_name] = array(
      '#title' => $label,
      '#type' => 'textfield',
      '#default_value' => $billing_info[$field_name],
      '#weight' => $weight,
      '#states' => array(
        'visible' => array(
          ':input[name="billing_type"]' => array('value' => 'Credit Card'),
        ),
        'required' => array(
          ':input[name="billing_type"]' => array('value' => 'Credit Card'),
        ),
      ),
    );
    $weight++;
  }

  // Don't require compnany.
  unset($form['billingCompany']['#states']['required']);

  // Dont require street address 2.
  unset($form['billingAddress2']['#states']['required']);

  // Make the cc type a select list.
  $form['billingCcType'] = array(
    '#type' => 'select',
    '#options' => array(
      'AE' => 'American Express',
      'DI' => 'Discover Card',
      'MC' => 'MasterCard',
      'VI' => 'Visa',
    ),
  ) + $form['billingCcType'];

  // Limit Month and year boxes.
  $form['billingMonth']['#size'] = $form['billingYear']['#size'] = 3;

  // Hide the CVV.
  $form['billingCvv']['#default_value'] = NULL;
  $form['billingCvv']['#size'] = 4;

  // Hide the card number.
  if (module_exists('aes')) {
    $form['billingNumber']['#default_value'] = aes_decrypt($billing_info['billingNumber']);
  }
  else {
    drupal_set_message(t('Enable AES module to encrypt card details when using the Credit Card billing type.'), 'warning');
  }
  $form['billingNumber_input'] = $form['billingNumber'];
  $form['billingNumber'] = array(
    '#type' => 'value',
    '#value' => $form['billingNumber_input']['#default_value'],
  );
  $card_number = $form['billingNumber_input']['#default_value'];
  $last_four = substr($card_number, -4);
  $form['billingNumber_input']['#default_value'] = str_pad($last_four, strlen($card_number), 'X', STR_PAD_LEFT);

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    ),
  );
  $form['#theme'] = 'system_settings_form';
  return $form;
}

/**
 * Validate.
 */
function party_communication_click2mail_billing_settings_form_validate($form, &$form_state) {
  if ($form_state['values']['billing_type'] == 'Credit Card') {
    $required = array(
      'billingName', 'billingAddress1', 'billingCity', 'billingState',
      'billingZip', 'billingCcType', 'billingNumber_input', 'billingMonth',
      'billingYear', 'billingCvv',
    );

    foreach ($required as $field_name) {
      if (empty($form_state['values'][$field_name])) {
        form_error($form[$field_name], t('%field is required to use credit card payments.', array('%field' => $form[$field_name]['#title'])));
      }
    }
  }
}

/**
 * Submit.
 */
function party_communication_click2mail_billing_settings_form_submit($form, &$form_state) {
  if ($form_state['values']['billingNumber_input'] != $form['billingNumber_input']['#default_value']) {
    $form_state['values']['billingNumber'] = $form_state['values']['billingNumber_input'];
  }

  variable_set('c2m_billing_type', $form_state['values']['billing_type']);

  $billing_info = $form_state['values'];
  $billing_info = array_filter($billing_info, function($key) {
    return (strpos($key, 'billing') === 0) && ($key != 'billing_type');
  }, ARRAY_FILTER_USE_KEY);

  unset($billing_info['billingNumber_input']);
  if (module_exists('aes')) {
    $billing_info['billingNumber'] = aes_encrypt($billing_info['billingNumber']);
  }

  variable_set('c2m_billing_info', $billing_info);
  drupal_set_message('Updated configuration.');
}
