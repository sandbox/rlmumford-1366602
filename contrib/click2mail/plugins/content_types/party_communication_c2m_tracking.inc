<?php
/**
 * Plugin for specially rendered content.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Communication click2mail Tracking'),
  'description' => t('Rendered click2mail Ttracking Information.'),
  'required context' => new ctools_context_required(t('Communication'), 'party_communication'),
  'category' => t('Communications'),
);

function party_communication_click2mail_party_communication_c2m_tracking_content_type_render($subtype, $conf, $panel_args, &$context) {
  $communication = $context->data;
  $tracking = $communication->getType()->getController()->getTracking();
  $output = array(
    '#theme' => 'table',
    '#header' => array(t('Status'), t('Location'), t('Time')),
    '#rows' => array(),
    '#empty' => t('No tracking information.'),
  );

  foreach ($tracking as $data) {
    $output['#rows'][] = array(
      $data->status,
      $data->dateTime,
      $data->statusLocation,
    );
  }

  $block = new stdClass();
  $block->module = 'party_communication_click2mail';
  $block->delta = 'tracking';
  $block->content = drupal_render($output);

  return $block;
}

function party_communication_click2mail_party_communication_c2m_tracking_content_type_admin_title($subtype, $conf, $context) {
  return t('Communication click2mail Tracking');
}

function party_communication_click2mail_party_communication_c2m_tracking_content_type_edit_form($form, &$form_state) {
  return $form;
}
