<?php

/**
 * @file
 *  Party Communication Hooks.
 */

/**
 * Implements hook_party_communication_mode_info().
 */
function party_communication_click2mail_party_communication_mode_info() {
  $modes = array();
  $modes['click2mail'] = array(
    'label' => t('Click2Mail'),
    'class' => 'PartyCommunicationModeClick2Mail',
  );

  return $modes;
}
