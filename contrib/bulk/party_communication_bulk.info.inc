<?php
/**
 * @file
 * Info hooks for bulk communication entity.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function party_communication_bulk_entity_property_info_alter(&$info) {
  $properties = &$info['party_communicaton_bulk']['properties'];
  $properties['creator']['type'] = 'party';
  $properties['created']['type'] = 'date';
  $properties['builder']['options list'] = 'party_communication_bulk_builder_options';
}
