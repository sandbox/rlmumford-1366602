<?php
/**
 * @file
 * Contains the from seed builder.
 */

class BulkCommunicationBuilderSeed extends BulkCommunicationBuilderBase {

  public static $label = 'Custom';

  public function build($key, $args = array()) {
    $communication = entity_import('party_communication', $this->bulkCommunication->builder_settings['seed']);
    return $communication;
  }

  public static function builderSettingsForm($form, &$form_state) {
    $bulk = $form_state['bulk_communication'];

    $bundles = field_info_bundles('party_communication');
    $options = array();
    foreach ($bundles as $bundle => $info) {
      $options[$bundle] = $info['label'];
    }
    $form['bundle'] = array(
      '#type' => 'select',
      '#title' => t('Communication'),
      '#options' => $options,
      '#default_value' => !empty($bulk->builder_settings['bundle']) ? $bulk->builder_settings['bundle'] : NULL,
    );

    return $form;
  }

  public static function builderSettingsFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    $form_state['bulk_communication']->builder_settings['bundle'] = $values['bundle'];
  }

  public static function bulkFormAlter(&$form, &$form_state) {
    if ($form_state['step'] == 'default') {
      $form['actions']['generate']['#access'] = FALSE;
      $form['actions']['save']['#submit'][] = array('BulkCommunicationBuilderSeed','bulkFormSubmitSetStep');
    }
    else if ($form_state['step'] == 'seed') {
      $builder = $form_state['bulk_communication']->builder;
      $form += static::bulkSeedForm($form, $form_state);

      $form['actions'] = array(
        '#type' => 'actions',
        'save' => array(
          '#type' => 'submit',
          '#value' => t('Save'),
          '#validate' => array(
            array($builder, 'bulkFormValidateSeed'),
          ),
          '#submit' => array(
            array($builder, 'bulkFormSubmitSaveSeed'),
          ),
        ),
        'generate' => array(
          '#type' => 'submit',
          '#value' => t('Generate'),
          '#validate' => array(
            array($builder, 'bulkFormValidateSeed'),
          ),
          '#submit' => array(
            array($builder, 'bulkFormSubmitSaveSeed'),
            'party_communication_bulk_form_submit_generate',
          ),
        ),
      );
    }
  }

  public static function bulkSeedForm($form, &$form_state) {
    $form_state['seed_entity']
      = $form_state['party_communication']
      = $seed
      = static::createSeedEntity($form_state['bulk_communication']);

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $seed->getType()->getController()->labelElementTitle(),
      '#default_value' => isset($seed->label) ? $seed->label : '',
      '#maxlength' => 255,
      '#required' => TRUE,
      '#weight' => -5,
    );
    field_attach_form('party_communication', $seed, $form, $form_state);

    // Let the processor edit the seed form.
    $processor = $form_state['bulk_communication']->processor();
    if (is_callable(array($processor, 'alterCustomSeedForm'))) {
      $processor->alterCustomSeedForm($form, $form_state);
    }

    return $form;
  }

  public static function bulkFormSubmitSetStep($form, &$form_state) {
    $form_state['step'] = 'seed';
    $form_state['rebuild'] = TRUE;
  }

  public static function bulkFormValidateSeed($form, &$form_state) {
    $communication = $form_state['party_communication'];
    field_attach_form_validate('party_communication', clone $communication, $form, $form_state);
  }

  public static function bulkFormSubmitSaveSeed($form, &$form_state) {
    $bulk = $form_state['bulk_communication'];

    $controller = entity_ui_controller('party_communication');
    $seed_entity = $controller->entityFormSubmitBuildEntity($form, $form_state);

    $bulk->builder_settings['seed'] = entity_export('party_communication', $seed_entity);
    $bulk->save();
  }

  public static function createSeedEntity($bulk) {
    if (!empty($bulk->builder_settings['seed'])) {
      if ($entity = entity_import('party_communication', $bulk->builder_settings['seed'])) {
        return $entity;
      }
    }
    return entity_create('party_communication', array('type' => $bulk->builder_settings['bundle']));
  }

  /**
   * {@inheritdoc}
   */
  public static function isUsable($bulk) {
    return ($bulk->processor() instanceof
      BulkCommunicationSeedProcessorInterface);
  }
}
