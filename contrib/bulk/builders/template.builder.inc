<?php

class BulkCommunicationTemplateBuilder extends BulkCommunicationBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function build($key, $args = array()) {
    $template = entity_load_single($this->bulkCommunication->processor_settings['template']);
    return $template->createNewEntityFromTemplate($args);
  }

  /**
   * {@inheritdoc}
   */
  public static function isUsable($bulk) {
    return ($bulk->processor() instanceof BulkCommunicationTemplateParameterSetProcessor);
  }
}