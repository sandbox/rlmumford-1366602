<?php
/**
 * @file
 * Contains the from seed builder.
 */

class BulkCommunicationBuilderTemplateSeed extends BulkCommunicationBuilderSeed {

  public static $label = 'Custom from Template';

  public static function bulkSeedForm($form, &$form_state) {
    $template = entity_template_load($form_state['bulk_communication']->builder_settings['template']);
    if (empty($template->settings['form']) || $template->settings['form'] == 'entity_template:standard') {
      return parent::bulkSeedForm($form, $form_state);
    }

    // Make seed entity.
    $form_state['seed_entity']
      = $form_state['party_communication']
      = $seed
      = static::createSeedEntity($form_state['bulk_communication']);

    $flexiform_name = substr($template->settings['form'], 10);
    $flexiform = entity_load_single('flexiform', $flexiform_name);
    $builder = $flexiform->getBuilder($seed);
    $form += $builder->form($form, $form_state);

    return $form;
  }

  public static function bulkFormValidateSeed($form, &$form_state) {
    $template = entity_template_load($form_state['bulk_communication']->builder_settings['template']);
    if (empty($template->settings['form']) || $template->settings['form'] == 'entity_template:standard') {
      return parent::bulkFormValidateSeed($form, $form_state);
    }
  }

  public static function bulkFormSubmitSaveSeed($form, &$form_state) {
    $template = entity_template_load($form_state['bulk_communication']->builder_settings['template']);
    if (empty($template->settings['form']) || $template->settings['form'] == 'entity_template:standard') {
      return parent::bulkFormSubmitSaveSeed($form, $form_state);
    }

    $flexiform_builder = $form['#flexiform_builder'];
    $flexiform_state = &$flexiform_builder->getFlexiformState($form, $form_state);

    foreach (element_children($form) as $element_namespace) {
      if (empty($form[$element_namespace]['#flexiform_element'])) {
        continue;
      }

      $element = $form[$element_namespace]['#flexiform_element'];
      $entity = $flexiform_builder->getFormEntity($element->getEntityNamespace(), $flexiform_state);
      $element->formSubmit($form, $form_state, $entity);
    }

    $flexiform_builder->invoke($form, $form_state, 'submit');

    $seed = $flexiform_builder->getFormEntity('base_entity', $flexiform_state);

    $bulk = $form_state['bulk_communication'];
    $bulk->builder_settings['seed'] = entity_export('party_communication', $seed);
    $bulk->save();
  }

  public static function builderSettingsForm($form, &$form_state) {
    $bulk = $form_state['bulk_communication'];
    $settings = $bulk->builder_settings;

    // Build the list of offered templates.
    $template_options = array();
    $template_names = db_select('entity_template', 't')
      ->fields('t', array('name'))
      ->condition('entity_type', 'party_communication')
      ->execute()
      ->fetchCol();
    foreach (entity_load('entity_template', $template_names) as $template) {
      if (!empty($template->parameters['bulk_communication'])
        && $template->parameters['bulk_communication']['type'] == 'party_communication_bulk') {
        $template_options[$template->name] = $template->label;
      }
    }
    drupal_alter('party_communication_bulk_seed_template_options', $template_options, $bulk);

    // Allow passing of template in URL.
    if (!empty($_GET['template']) && empty($settings['template']) && !empty($template_options[$_GET['template']])) {
      $builder->builder_settings['template'] = $_GET['template'];
      $form_state['template_specified_in_url'] = TRUE;
    }

    $form['template'] = array(
      '#type' => 'select',
      '#title' => t('Template'),
      '#options' => $template_options,
      '#default_value' => !empty($bulk->builder_settings['template']) ? $bulk->builder_settings['template'] : NULL,
      '#access' => empty($form_state['template_specified_in_url']),
    );

    return $form;
  }

  public static function builderSettingsFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    $form_state['bulk_communication']->builder_settings['template'] = $values['template'];
  }

  public static function createSeedEntity($bulk) {
    if (!empty($bulk->builder_settings['seed'])) {
      if ($entity = entity_import('party_communication', $bulk->builder_settings['seed'])) {
        return $entity;
      }
    }
    $template = entity_template_load($bulk->builder_settings['template']);
    $template_params = array('bulk_communication' => $bulk);
    drupal_alter('party_communication_bulk_seed_template_parameters', $template_params, $bulk, $template);
    return $template->createNewEntityFromTemplate($template_params);
  }

  /**
   * {@inheritdoc}
   */
  public static function isUsable($bulk) {
    return ($bulk->processor() instanceof
      BulkCommunicationSeedProcessorInterface);
  }
}
