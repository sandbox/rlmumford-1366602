<?php
/**
 * @file
 * Entity Classes for the Bulk Communication Entity.
 */

class BulkCommunication extends Entity {

  /**
   * The processor Object used to Generate all the Communications.
   *
   * @var BulkCommunicationProcessor
   */
  protected $processor;


  /**
   * Get the Processor.
   */
  public function processor() {
    if (empty($this->processor)) {
      $bundle_info = party_communication_bulk_type_info($this->type);
      if (empty($bundle_info['processor'])) {
        throw new Exception(t('No Processor Class set for type %type', array('%type' => $bundle_info['label'])));
      }
      $class = $bundle_info['processor'];
      if (!class_exists($class) || !in_array('BulkCommunicationProcessorInterface', class_implements($class))) {
        throw new Exception(t('Invalid Bulk Processor class: %class', array('%class' => $class)));
      }
      $this->processor = $class::createProcessor($this);
    }
    return $this->processor;
  }

  /**
   * Generate all the Necessary Communications.
   */
  public function generateAll() {
    $this->processor()->generateAll();
  }
}
