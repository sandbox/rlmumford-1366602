<?php
/**
 * @file
 * Contains classes for builders.
 */

/**
 * Interface for Bulk Communication Builders.
 *
 * Builders are responsible for taking a key and args and turning them into
 * a fulle communication entity.
 */
interface BulkCommunicationBuilderInterface {

  /**
   * Create a Builder object.
   *
   * @param BulkCommunication $bulk
   *   The bulk communication configuration.
   *
   * @return BulkCommunicationBuilderInterface
   */
  public static function createBuilder($bulk);

  /**
   * Build the communication.
   *
   * @return PartyCommunication
   */
  public function build($key, $args = array());

  /**
   * Determine whether this builder is usable for a given bulk communication.
   *
   * @param BulkCommunication $bulk
   */
  public static function isUsable($bulk);


}

/**
 * Base Class for Bulk Communication Builders.
 */
abstract class BulkCommunicationBuilderBase implements BulkCommunicationBuilderInterface {

  /**
   * The bulk communication entity.
   *
   * @var BulkCommunication $bulkCommunication
   */
  protected $bulkCommunication;

  /**
   * {@inheritdoc}
   */
  public static function createBuilder($bulk) {
    return new static($bulk);
  }

  /**
   * Construct a new bulk builder object.
   *
   * @param BulkCommunication $bulk
   *   The bulk communication entity.
   */
  public function __construct($bulk) {
    $this->bulkCommunication = $bulk;
  }

  /**
   * {@inheritdoc}
   */
  public abstract function build($key, $args = array());

  /**
   * {@inheritdoc}
   */
  public static function isUsable($bulk) {
    return FALSE;
  }

}
