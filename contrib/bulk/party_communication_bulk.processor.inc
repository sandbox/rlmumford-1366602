<?php
/**
 * @file
 * Contains Bulk Communication Processors.
 */

/**
 * Interface for Bulk Communication Processors.
 *
 * The processors are responsible for taking a bulk communication entity and
 * creating from them the correct set of communication entities. Each
 * communication created should have a key that is unique with the bulk
 * communication entity.
 */
interface BulkCommunicationProcessorInterface {

  /**
   * Create a new bulk processor object.
   *
   * @param BulkCommunication $bulk
   *   The bulk communication entity.
   *
   * @return BulkCommunicationProcessorInterface
   *   A constructed communication processor.
   */
  public static function createProcessor($bulk);

  /**
   * Generate all communications for this processor.
   */
  public function generateAll();

  /**
   * Generate a batch process for this processor.
   */
  public function generateBatch();
}

/**
 * Interface for Bulk Communication Processors that expect a seed entity that
 * is then tweaked by the processor.
 *
 * There are roughly two ways of doing processors. One is store a 'seed' copy
 * of the communication on the the bulk communication then create copies of
 * this. The other is two create a new communication programmatically on each
 * case.
 */
interface BulkCommunicationSeedProcessorInterface {

  /**
   * Alter the custom seed form.
   *
   * @see BulkCommunicationBuilderSeed::bulkFormAlter().
   */
  public function alterCustomSeedForm(&$form, &$form_state);

}

/**
 * Base class for Bulk Communication Processors.
 */
abstract class BulkCommunicationProcessorBase implements BulkCommunicationProcessorInterface {

  /**
   * The bulk communication entity.
   *
   * @var BulkCommunication $bulkCommunication
   */
  protected $bulkCommunication;

  /**
   * The bulk communication builder.
   *
   * @var BulkCommunicationBuilderInterface
   */
  protected $builder;

  /**
   * {@inheritdoc}
   */
  public static function createProcessor($bulk) {
    return new static($bulk);
  }

  /**
   * Construct a new bulk processor object.
   *
   * @param BulkCommunication $bulk
   *   The bulk communication entity.
   */
  public function __construct($bulk) {
    $this->bulkCommunication = $bulk;
  }

  /**
   * {@inheritdoc}
   */
  public function generateAll() {
    $keys = $this->getUngeneratedCommunicationKeys();
    foreach ($keys as $key => $args) {
      $this->generate($key, $args);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function generateBatch() {
    // @todo: Implement.
    $batch = array(
      'operations' => array(
        array(array($this, 'batchProcess'), array()),
      ),
      'finished' => array($this, 'batchFinished'),
      'title' => t('Generating Communications'),
      'init_message' => t('Preparing Communication Generation'),
      'progress_message' => t('Processed @current out of @total'),
      'error_message' => t('Faile to Generate Communications'),
    );
    batch_set($batch);
  }

  /**
   * Process the batch.
   */
  public function batchProcess(&$context) {
    $ungenerated_keys = $this->getUngeneratedCommunicationKeys(10);
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($this->getCommunicationKeys());
    }

    $context['message'] = '';
    foreach ($ungenerated_keys as $key => $args) {
      $communication = $this->generate($key, $args);
      $context['sandbox']['progress']++;
      $context['message'] .= t('Generated !key: !label', array('!key' => $key, '!label' => $communication->label)) . '<br />';
    }

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Finish the Batch.
   */
  public function batchFinished($success, $results, $operations) {
    if ($success) {
      drupal_set_message('Successfully created communications.');
    }
    else {
      drupal_set_message('An error occurred', 'error');
    }
  }

  /**
   * Get a the communication builder.
   */
  protected function builder() {
    if (empty($this->builder)) {
      $class = $this->bulkCommunication->builder;
      if (!class_exists($class) || !in_array('BulkCommunicationBuilderInterface', class_implements($class))) {
        throw new Exception(t('Invalid Bulk Builder class: %class', array('%class' => $class)));
      }
      $this->builder = $class::createBuilder($this->bulkCommunication);
    }
    return $this->builder;
  }

  /**
   * Generate a specific communication.
   *
   * @param string $key
   *   The key of this communication entity.
   * @param array $args
   *   The args for this particular key.
   */
  protected function generate($key, $args = array()) {
    $communication = $this->builder()->build($key, $args);
    $this->build($communication, $key, $args);
    $communication->save();
    $this->recordGeneration($key, $communication);
    return $communication;
  }

  /**
   * Alter the built communication.
   *
   * Processors can override this method to make tweaks to the created
   * communication.
   *
   * @param PArtyCommunication $communication
   * @param string $key
   * @param array $args
   */
  protected function build($communication, $key, $args = array()) {}

  /**
   * Record a specific communication.
   *
   * @throws InvalidMergeQueryException
   */
  protected function recordGeneration($key, $communication) {
    db_merge('party_communication_bulk_comm')
      ->key(array('bulk_id' => $this->bulkCommunication->id, 'comm_key' => $key))
      ->fields(array(
        'bulk_id' => $this->bulkCommunication->id,
        'comm_key' => $key,
        'comm_id' => $communication->id,
      ))
      ->execute();
  }

  /**
   * Get a list of all the keys and their arguments.
   *
   * @return array
   *   Array of arguments for communication creation keyed by key.
   */
  abstract protected function getCommunicationKeys();

  /**
   * Get un generated keys.
   */
  protected function getUngeneratedCommunicationKeys($limit = 0) {
    $keys = $this->getCommunicationKeys();
    $generated_keys = db_select('party_communication_bulk_comm', 'c')
      ->fields('c', array('comm_key'))
      ->condition('bulk_id', $this->bulkCommunication->id)
      ->condition('comm_id', 0, '>')
      ->execute()
      ->fetchCol();
    $ungenerated_keys = array_diff_key($keys, drupal_map_assoc($generated_keys));
    return $limit ? array_slice($ungenerated_keys, 0, $limit, TRUE) : $ungenerated_keys;
  }
}
