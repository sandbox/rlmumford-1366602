<?php

class BulkCommunicationTemplateParameterSetProcessor extends BulkCommunicationProcessorBase {

  /**
   * {@inheritdoc}
   */
  protected function getCommunicationKeys() {
    return $this->bulkCommunication->processor_settings['parameter_sets'];
  }

  /**
   * Processor Settings Form
   *
   * @param array $form
   * @param array &$form_state
   *
   * @return array
   */
  public function processorSettingsForm($form, &$form_state) {
    $template_options = array();
    $query = db_select('entity_template', 't');
    $query->condition('entity_type', 'party_communication');
    $query->fields('t', array('label', 'name', 'bundle'));

    $party_communication_info = entity_get_info('party_communication');
    foreach ($query->execute()->fetchAllAssoc('name') as $name => $row) {
      $bundle_label = $row->bundle;
      if (!empty($party_communication_info['bundles'][$bundle_label]['label'])) {
        $bundle_label = $party_communication_info['bundles'][$bundle_label]['label'];
      }
      $template_options[$bundle_label][$name] = $row->label;
    }

    $form['template'] = array(
      '#type' => 'select',
      '#title' => t('Communication Template'),
      '#options' => $template_options,
      '#default_value' => !empty($this->bulkCommunication->processor_settings['template']) ? $this->bulkCommunication->processor_settings['template'] : NULL,
      '#ajax' => array(
        'wrapper' => 'bulk-comm-template-parameters-wrapper',
        'callback' => array($this, 'processorSettingsFormAjaxCallback'),
      ),
      '#template_parameter_template_selector' => TRUE,
    );

    if (!empty($form_state['triggering_element']['#template_parameter_template_selector'])) {
      $form_state['template_parameter_set_template'] = entity_load_single(
        'entity_template',
        drupal_array_get_nested_value(
          $form_state['values'],
          $form_state['triggering_element']['#parents']
        )
      );
    }

    dpm($form_state);

    if (!empty($form_state['template_parameter_set_template'])) {
      $form['parameter_sets'] = array(
        '#type' => 'container',
      );

      $parameter_sets = !empty($this->bulkCommunication->processor_settings['parameter_sets']) ? $this->bulkCommunication->processor_settings['parameter_sets'] : array();
      foreach ($parameter_sets as $key => $parameter_set) {
        $form['parameter_sets'][$key] = array(
          '#type' => 'fieldset',
          '#prefix' => '<div id="bulk-comm-template-parameters-wrapper">',
          '#suffix' => '</div>',
        );

        foreach ($form_state['template_parameter_set_template']->parameters as $name => $parameter_info) {
          $form['parameter_sets'][$key][$name] = array(
            '#type' => 'textfield',
            '#title' => $property_info['label'],
            '#default_value' => $parameter_set[$name],
            '#required' => empty($parameter_info['optional']),
          );

          if (entity_get_info($parameter_info['type'])) {
            $form['parameter_sets'][$key][$name]['#description'] = t('Please enter the entity id.');
          }
        }

        $form['parameter_sets'][$key]['actions__remove'] = array(
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'parameter_set_remove_'.$key,
          '#key_to_remove' => $key,
          '#submit' => array(
            array($this, 'processorSettingsFormSubmitRemoveSet'),
          ),
          '#limit_validation_errors' => array(),
          '#ajax' => array(
            'wrapper' => 'bulk-comm-template-parameters-wrapper',
            'callback' => array($this, 'processorSettingsFormAjaxCallback'),
          ),
        );
      }

      $form['parameters_sets']['actions__add'] = array(
        '#type' => 'submit',
        '#value' => t('Add Communication'),
        '#name' => 'parameter_set_add',
        '#submit' => array(
          array($this, 'processorSettingsFormSubmitAddSet'),
        ),
        '#limit_validation_errors' => array(),
        '#ajax' => array(
          'wrapper' => 'bulk-comm-template-parameters-wrapper',
          'callback' => array($this, 'processorSettingsFormAjaxCallback'),
        ),
      );
    }
    else {
      $form['parameter_sets'] = array(
        '#type' => 'markup',
        '#markup' => '<div id="bulk-comm-template-parameters-wrapper"></div>',
      );
    }

    return $form;
  }

  /**
   * Remove Parameter Set Submit Callback
   */
  public function processorSettingsFormSubmitRemoveSet($form, &$form_state) {
    $bulk = $form_state['bulk_communication'];
    unset($bulk->processor_settings['parameter_sets'][$form_state['triggering_element']['#key_to_remove']]);
    $form_state['rebuild'] = TRUE;
  }

  /**
   * Add new parameter set submit callback.
   */
  public function processorSettingsFormSubmitAddSet($form, &$form_state) {
    $bulk = $form_state['bulk_communication'];
    $bulk->processor_settings['parameter_sets'][] = array();
    $form_state['rebuild'] = TRUE;
  }

  /**
   * Return the parameter sets part of the form.
   */
  public function processorSettingsFormAjaxCallback($form, $form_state) {
    return $form['processor_settings']['parameter_sets'];
  }
}