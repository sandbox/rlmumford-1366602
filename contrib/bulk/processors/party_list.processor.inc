<?php
/**
 * @file
 * Add a processor for custom lists of parties.
 */

class BulkCommunicationProcessorPartyList extends BulkCommunicationProcessorBase implements BulkCommunicationSeedProcessorInterface {

  public function getCommunicationKeys() {
    $keys = array();
    if (!empty($this->bulkCommunication->bulk_comm_recipients[LANGUAGE_NONE])) {
      foreach ($this->bulkCommunication->bulk_comm_recipients[LANGUAGE_NONE] as $item) {
        $keys[$item['target_id']] = array(
          'recipient' => $item['target_id'],
        );
      }
    }
    return $keys;
  }

  /**
   * {@inheritdoc}
   */
  protected function build($communication, $key, $args) {
    parent::build($communication, $key, $args);
    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    if ($party = party_load($args['recipient'])) {
      $wrapper->communication_recipients = $party;
    }
  }

  /**
   * Alter the custom seed form.
   *
   * @see BulkCommunicationBuilderSeed::bulkFormAlter().
   */
  public function alterCustomSeedForm(&$form, &$form_state) {
    $form['communication_recipients']['#access'] = FALSE;
  }
}
