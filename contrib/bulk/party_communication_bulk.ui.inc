<?php
/**
 * @file
 * UI Code for Bulk Communications.
 */

/**
 * Form to Create Bulk Communications.
 */
function party_communication_bulk_form($form, &$form_state, $bulk, $step = 'default') {
  // Add a wrapping div
  $form['#prefix'] = '<div id="party-communication-bulk-form-ajax-wrapper">';
  $form['#suffix'] = '</div>';

  // Allow the builder to be specified in the url.
  $builder_options = party_communication_bulk_builder_options();
  if (!empty($_GET['builder']) && empty($bulk->builder) && !empty($builder_options[$_GET['builder']])) {
    $bulk->builder = $_GET['builder'];
    $form_state['builder_specified_in_url'] = TRUE;
  }

  $form_state['bulk_communication'] = $bulk;
  if (empty($form_state['step'])) {
    $form_state['step'] = $step;
  }

  $builder = !empty($form_state['input']['builder']) ? $form_state['input']['builder'] : $bulk->builder;
  if ($form_state['step'] == 'default') {
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => $bulk->label,
      '#weight' => -50,
    );

    field_attach_form('party_communication_bulk', $bulk, $form, $form_state);

    $form['processor_settings'] = array(
      '#type' => 'container',
      '#weight' => 49,
      '#tree' => TRUE,
    );
    $processor = $bulk->processor();
    if (is_callable(array($processor, 'processorSettingsForm'))) {
      $form['processor_settings'] += $processor->processorSettingsForm($form['processor_settings'], $form_state);
    }

    $form['builder'] = array(
      '#type' => 'select',
      '#options' => party_communication_bulk_builder_options($bulk),
      '#weight' => 50,
      '#title' => t('Communication Builder'),
      '#description' => t('How should this communication be created?'),
      '#empty_option' => t('- Please Select -'),
      '#default_value' => $bulk->builder,
      '#access' => empty($form_state['builder_specified_in_url']),
      '#ajax' => array(
        'wrapper' => 'party-communication-bulk-form-ajax-wrapper',
        'callback' => 'party_communication_bulk_form_reload_form',
      ),
    );

    $form['builder_settings'] = array(
      '#type' => 'container',
      '#tree' => TRUE,
      '#prefix' => '<div id="bulk-communication-builder-settings">',
      '#suffix' => '</div>',
      '#weight' => 50.1,
    );
    if ($builder && is_callable($builder.'::builderSettingsForm')) {
      $form['builder_settings'] += $builder::builderSettingsForm($form['builder_settings'], $form_state);
    }

    $form['actions'] = array(
      '#type' => 'actions',
      '#weight' => 100,
    );
    $form['actions']['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#submit' => array('party_communication_bulk_form_submit_save'),
    );
    $form['actions']['generate'] = array(
      '#type' => 'submit',
      '#value' => t('Save & Generate'),
      '#submit' => array(
        'party_communication_bulk_form_submit_save',
        'party_communication_bulk_form_submit_generate',
      ),
    );
  }

  if (is_callable($builder.'::bulkFormAlter')) {
    $builder::bulkFormAlter($form, $form_state);
  }

  return $form;
}

/**
 * AJAX Callback for when the Builder is update.
 */
function party_communication_bulk_form_reload_form($form, $form_state) {
  return $form;
}

/**
 * Submit & Save Callback for the form.
 */
function party_communication_bulk_form_submit_save($form, &$form_state) {
  /* @var BulkCommunication $bulk */
  $bulk = $form_state['bulk_communication'];
  $bulk->label = $form_state['values']['label'];
  $bulk->builder = $builder = $form_state['values']['builder'];
  field_attach_submit('party_communication_bulk', $bulk, $form, $form_state);

  if (is_callable(array($bulk->processor(), 'processorSettingsFormSubmit'))) {
    $bulk->processor()->processorSettingsFormSubmit($form['processor_settings'], $form_state);
  }

  if (is_callable($builder.'::builderSettingsFormSubmit')) {
    $builder::builderSettingsFormSubmit($form['builder_settings'], $form_state);
  }

  if (is_callable($builder.'::bulkFormSubmitSave')) {
    $builder::bulkFormSubmitSave($form, $form_state);
  }

  $bulk->save();
}

/**
 * Submit & Generate Communications.
 */
function party_communication_bulk_form_submit_generate($form, &$form_state) {
  $bulk = $form_state['bulk_communication'];
  $bulk->processor()->generateBatch();
}

