<?php
/**
 * @file
 * Bulk Communication Hooks.
 */

/**
 * Implements hook_party_communication_bulk_type_info().
 */
function party_communication_bulk_party_communication_bulk_type_info() {
  $types['party_list'] = array(
    'processor' => 'BulkCommunicationProcessorPartyList',
    'label' => t('Party List'),
  );
  $types['template'] = array(
    'processor' => 'BulkCommunicationTemplateParameterSetProcessor',
    'label' => t('Template'),
  );
  return $types;
}
