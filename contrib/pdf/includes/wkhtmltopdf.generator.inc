<?php

class PartyCommunicationPDFWKHTMLToPDFGenerator implements PartyCommunicationPDFGeneratorInterface {

  /**
   * {@inheritdoc}
   */
  public function generate($render_content, $out_file, array $options = array(), $log_file = NULL) {
    $options += array(
      'in_type' => 'url',
    );

    switch ($options['in_type']) {
      case 'html':
        do {
          $tmp_file = tempnam(drupal_realpath('temporary://'), 'pdfrender_');
        } while (!rename($tmp_file, $tmp_file.'.html'));
        $tmp_file .= '.html';
        file_put_contents($tmp_file, $render_content);
        $in_file = 'file://'.$tmp_file;
        break;
      case 'file':
        $in_file = drupal_realpath($render_content);
        break;
      case 'url':
      default:
        $in_file = $render_content;
        break;
    }
    unset($options['in_type']);

    $script = "wkhtmltopdf ".implode(' ', $options)." \"{$in_file}\" \"{$out_file}\"";
    if ($log_file) {
      $script .= " &> \"{$log_file}\"";
    }

    exec($script);
  }
}
