<?php
/**
 * Created by PhpStorm.
 * User: Rob
 * Date: 12/06/2018
 * Time: 12:29
 */

interface PartyCommunicationPDFGeneratorInterface {

  /**
   * Generate a PDF.
   *
   * @param string $render_content
   *   The url or raw html content to render.
   * @param string $out_file
   *   The resultant file name of the pdf file.
   * @param array $options
   *   Options including:
   *   - in_type: One of 'file', 'html' or 'url'. Defaults to 'url'.
   * @param string $log_file
   *   The file to put any logs into.
   */
  public function generate($render_content, $out_file, array $options = array(), $log_file = NULL);

}
