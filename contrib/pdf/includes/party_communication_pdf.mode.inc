<?php
/**
 * @file
 *   Contains the mode class for PDFs.
 */

class PartyCommunicationModePDF extends PartyCommunicationModeLongText {

  /**
   * {@inheritdoc}
   */
  public function getParticipationRoles(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createType($bundle) {
    parent::createType($bundle);

    if (!field_info_field('communication_pdf')) {
      $field = array(
        'field_name' => 'communication_pdf',
        'type' => 'file',
        'cardinality' => 1,
        'settings' => array(
          'uri_scheme' => 'private',
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_pdf', $bundle)) {
      $instance = array(
        'field_name' => 'communication_pdf',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('PDF'),
        'description' => t('The generated PDF.'),
        'settings' => array(
          'file_extensions' => 'pdf',
          'file_directory' => 'party_communication_pdf',
        ),
        'display' => array(
          'default' => array(
            'label' => 'inline',
          ),
        ),
      );
      field_create_instance($instance);
    }
  }

  /**
   * Get the filename.
   */
  public function getFilename($communication) {
    return str_replace(array('/','\\'), '-', $communication->label) . ' ' . $communication->id . '.pdf';
  }

  /**
   * {@inheritdoc}
   */
  public function generate($communication, $preview = FALSE) {
    // Call the pregenerate hook.
    module_invoke_all('party_communication_pdf_pregenerate', $communication, $preview);

    // Get the render path
    $query = array(
      'token' => party_communication_pdf_access_token($communication),
    );
    if ($preview) {
      $query['preview'] = 'preview';
    }
    $render_path = url(party_communication_pdf_communication_print_path($communication), array(
        'query' => $query,
        'absolute' => TRUE,
      ));

    // File path
    $directory = "temporary://party_communication_pdf";
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    $file_name = 'pdf_'.$communication->id.'_'.REQUEST_TIME.($preview ? '_preview' : '').'.pdf';
    $file_path = $directory."/".$file_name;
    $real_file = drupal_realpath($file_path);
    $log_file = substr($real_file, 0, -4) . ".log";

    // Make sure the directory exists.
    $directory = dirname($real_file);
    if (!is_dir($directory)) {
      mkdir($directory);
    }

    $options = $this->prepareOptions($communication);
    party_communication_pdf_generator()->generate($render_path, $real_file, $options, $log_file);

    if (!file_exists($real_file)) {
      $message = 'Failed to communication PDF. Render path: %render_path. File path: %file_path. For more information, please see the log file %log_file';
      $vars = array(
        '%render_path' => $render_path,
        '%file_path' => $real_file,
        '%log_file' => $log_file,
      );
      watchdog('party_communication_pdf', $message, $vars, WATCHDOG_ERROR);
      return;
    }

    module_invoke_all('party_communication_pdf_generate', $communication, $file_path);

    if (!file_exists($file_path)) {
      throw new Exception('PDF Generation Failed');
    }
    else if (function_exists('finfo_open')) {
      $finfo = finfo_open(FILEINFO_MIME_TYPE);
      if (finfo_file($finfo, $file_path) != 'application/pdf') {
        throw new Exception('PDF Generation Failed');
      }
    }

    return $file_path;
  }

  /**
   * Prepare wkhtmltopdf options.
   */
  public function prepareOptions($communication) {
    $default_wk_options = variable_get('party_communication_pdf_wkhtmltopdf_defaults', array(
      // Quality adjustments.
      '--disable-smart-shrinking',
      '--print-media-type',
      // Disable borders.
      '-T 0',
      '-R 0',
      '-B 0',
      '-L 0',
      // Disable outlines.
      '--no-outline',
      '--outline-depth 0',
    ));

    $type = $communication->getType();
    $options = !empty($type->data['wkhtmltopdf_options']) ? $type->data['wkhtmltopdf_options'] : $default_wk_options;
    $options = implode(' ||| ', $options);
    $options = preg_replace("/\r|\n/", "", $options);
    // Run prepare content on the options to allow for tokens.
    $options = $this->prepareContent($options, $communication);
    $options = html_entity_decode($options);
    $options = explode(' ||| ', $options);

    return $options;
  }

  /**
   * Generate a preview.
   */
  public function preview($communication) {
    try {
      $file_path = $this->generate($communication, TRUE);
      return $file_path;
    }
    catch (\Exception $e) {
      watchdog_exception('party_communication_pdf', $e);
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateSend($communication, $settings = array(), &$reasons = array()) {
    parent::validateSend($communication, $settings, $reasons);

    if (!empty($communication->communication_pdf[LANGUAGE_NONE])) {
      $reasons[] = t('The PDF has already been generated.');
    }

    if (empty($reasons)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   *
   * PDFs need to be prepared in the correct theme.
   */
  protected function prepare($communication) {
    global $theme, $theme_key, $theme_info, $base_theme_info, $theme_engine, $theme_path;

    $type = $communication->getType();
    $print_theme = NULL;
    if (
      !empty($type->data['theme']['custom_theme'])
      && $type->data['theme']['custom_theme'] != 'default'
      && $type->data['theme']['custom_theme'] !== $theme_key
    ) {
      $print_theme = $type->data['theme']['custom_theme'];
    }

    // Swap the theme out to something else.
    if ($print_theme) {
      $original_theme = $theme;
      $original_theme_key = $theme_key;

      $themes = list_themes();
      $theme = $type->data['theme']['custom_theme'];

      // Store the identifier for retrieving theme settings with.
      $theme_key = $theme;

      // Find all our ancestor themes and put them in an array.
      $base_theme = array();
      $ancestor = $theme;
      while ($ancestor && isset($themes[$ancestor]->base_theme)) {
        $ancestor = $themes[$ancestor]->base_theme;
        $base_theme[] = $themes[$ancestor];
      }
      $base_theme = array_reverse($base_theme);

      $theme_info = $themes[$theme];
      $base_theme_info = $base_theme;

      $theme_path = dirname($theme_info->filename);

      $theme_engine = NULL;

      // Initialize the theme.
      if (isset($theme_info->engine)) {
        // Include the engine.
        include_once DRUPAL_ROOT . '/' . $theme_info->owner;

        $theme_engine = $theme_info->engine;
        if (function_exists($theme_engine . '_init')) {
          foreach ($base_theme as $base) {
            call_user_func($theme_engine . '_init', $base);
          }
          call_user_func($theme_engine . '_init', $theme);
        }
      }
      else {
        // include non-engine theme files
        foreach ($base_theme as $base) {
          // Include the theme file or the engine.
          if (!empty($base->owner)) {
            include_once DRUPAL_ROOT . '/' . $base->owner;
          }
        }
        // and our theme gets one too.
        if (!empty($theme->owner)) {
          include_once DRUPAL_ROOT . '/' . $theme_info->owner;
        }
      }

      _theme_registry_callback('_theme_load_registry', array($theme_info, $base_theme, $theme_engine));

      // Themes can have alter functions, so reset the drupal_alter() cache.
      drupal_static_reset('drupal_alter');
    }

    parent::prepare($communication);

    // Reset the theme to what it was originally.
    if ($print_theme) {
      $theme = $original_theme;
      $theme_key = $original_theme_key;

      // Find all our ancestor themes and put them in an array.
      $base_theme = array();
      $ancestor = $theme;
      while ($ancestor && isset($themes[$ancestor]->base_theme)) {
        $ancestor = $themes[$ancestor]->base_theme;
        $base_theme[] = $themes[$ancestor];
      }
      $base_theme = array_reverse($base_theme);

      $theme_info = $themes[$theme];
      $base_theme_info = $base_theme;

      $theme_path = dirname($theme_info->filename);

      $theme_engine = NULL;

      // Initialize the theme.
      if (isset($theme_info->engine)) {
        // Include the engine.
        include_once DRUPAL_ROOT . '/' . $theme_info->owner;

        $theme_engine = $theme_info->engine;
        if (function_exists($theme_engine . '_init')) {
          foreach ($base_theme as $base) {
            call_user_func($theme_engine . '_init', $base);
          }
          call_user_func($theme_engine . '_init', $theme);
        }
      }
      else {
        // include non-engine theme files
        foreach ($base_theme as $base) {
          // Include the theme file or the engine.
          if (!empty($base->owner)) {
            include_once DRUPAL_ROOT . '/' . $base->owner;
          }
        }
        // and our theme gets one too.
        if (!empty($theme->owner)) {
          include_once DRUPAL_ROOT . '/' . $theme_info->owner;
        }
      }

      _theme_registry_callback('_theme_load_registry', array($theme_info, $base_theme, $theme_engine));

      // Themes can have alter functions, so reset the drupal_alter() cache.
      drupal_static_reset('drupal_alter');
    }

    $communication->is_new_revision = TRUE;
    $communication->revision_log = t('Lock Content for PDF Generation');
    $communication->revision_timestamp = REQUEST_TIME;
    $communication->save();
  }

  /**
   * {@inheritdoc}
   *
   * Here we need to apply the correct theme before applying tokens.
   *
   * @see \drupal_theme_initialize()
   * @see \_drupal_theme_initialize()
   */
  public function renderContentField($communication, $field_name, $preview = FALSE) {

    $type = $communication->getType();
    if (
      empty($type->data['theme']['custom_theme'])
      || $type->data['theme']['custom_theme'] != 'default'
    ) {
      return parent::renderContentField($communication, $field_name, $preview);
    }

    $original_theme = $theme;
    $original_theme_key = $theme_key;

    $themes = list_themes();
    $theme = $type->data['theme']['custom_theme'];

    // Store the identifier for retrieving theme settings with.
    $theme_key = $theme;

    // Find all our ancestor themes and put them in an array.
    $base_theme = array();
    $ancestor = $theme;
    while ($ancestor && isset($themes[$ancestor]->base_theme)) {
      $ancestor = $themes[$ancestor]->base_theme;
      $base_theme[] = $themes[$ancestor];
    }
    _drupal_theme_initialize($themes[$theme], array_reverse($base_theme));

    // Themes can have alter functions, so reset the drupal_alter() cache.
    drupal_static_reset('drupal_alter');

    $return = parent::renderContentField($communication, $field_name, $preview);

    $theme = $original_theme;
    $theme_key = $original_theme_key;

    // Find all our ancestor themes and put them in an array.
    $base_theme = array();
    $ancestor = $theme;
    while ($ancestor && isset($themes[$ancestor]->base_theme)) {
      $ancestor = $themes[$ancestor]->base_theme;
      $base_theme[] = $themes[$ancestor];
    }
    _drupal_theme_initialize($themes[$theme], array_reverse($base_theme));

    // Themes can have alter functions, so reset the drupal_alter() cache.
    drupal_static_reset('drupal_alter');

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function send($communication, $settings = array()) {
    $this->prepare($communication);
    try {
      $file_path = $this->generate($communication);

      // Save the file to the file field.
      // @todo: Make configurable.
      $file_storage_directory = "private://party_communication_pdf";
      file_prepare_directory($file_storage_directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $file_name = $this->getFilename($communication);
      $file_storage_path = $file_storage_directory . "/" . $file_name;
      $file = new stdClass;
      $file->uri = $file_path;
      $file->filename = drupal_basename($file->uri);
      $file->status = FILE_STATUS_PERMANENT;
      $file->filemime = file_get_mimetype($file->uri);
      $file = file_move($file, $file_storage_path, FILE_EXISTS_RENAME);

      $communication->communication_pdf[LANGUAGE_NONE][0] = array(
        'fid' => $file->fid,
        'display' => 1,
      );
      $communication->timestamp = REQUEST_TIME;
      $communication->status = 'sent';
      $communication->revision_timestamp = REQUEST_TIME;
      $communication->revision_log = t('Generate PDF');
      $communication->is_new_revision = TRUE;
      $communication->save();
    }
    catch (\Exception $e) {
      $communication->status = 'failed';
      $communication->revision_timestamp = REQUEST_TIME;
      $communication->revision_log = t('Failed to generate PDF').' '.$e->getMessage();
      $communication->is_new_revision = TRUE;
      $communication->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function labelElementTitle($communication) {
    return t('Subject');
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, &$form_state) {
    $type = $form_state['party_communication_type'];

    // Build Theme Options.
    $themes = system_rebuild_theme_data();
    foreach ($themes as $theme) {
      if (!empty($theme->info['hidden'])) {
        continue;
      }

      $theme_opts[$theme->name] = $theme->info['name'];
    }

    $form['custom_theme'] = array(
      '#type' => 'select',
      '#options' => array('default' => t('Default')) + $theme_opts,
      '#title' => t('Custom Theme'),
      '#description' => t('If you wish to render PDF\'s in a custom theme select which theme here.'),
      '#default_value' => !empty($type->data['theme']['custom_theme']) ? $type->data['theme']['custom_theme'] : '',
    );

    $form['theme_hook_suggestions'] = array(
      '#type' => 'textarea',
      '#title' => t('Theme Hook Suggestions'),
      '#description' => t('If you wish to use a different template file instead of page.tpl.php then specify the suggestions here.'),
      '#default_value' => !empty($type->data['theme']['theme_hook_suggestions']) ? implode("\n", $type->data['theme']['theme_hook_suggestions']) : '',
    );

    $default_wk_options = variable_get('party_communication_pdf_wkhtmltopdf_defaults', array(
      // Quality adjustments.
      '--disable-smart-shrinking',
      '--print-media-type',
      // Disable borders.
      '-T 0',
      '-R 0',
      '-B 0',
      '-L 0',
      // Disable outlines.
      '--no-outline',
      '--outline-depth 0',
    ));
    $form['wkhtmltopdf_options'] = array(
      '#type' => 'textarea',
      '#title' => t('WKHTMLtoPDF Options'),
      '#description' => t(
        'Options to pass to WKHTMLtoPDF. Options can contain tokens from the communication, current-user or site contexts. For more information on what options are available !link.',
        array(
          '!link' => l('click here', 'https://madalgo.au.dk/~jakobt/wkhtmltoxdoc/wkhtmltopdf-0.9.9-doc.html'),
        )
      ),
      '#default_value' => implode("\n", (!empty($type->data['wkhtmltopdf_options']) ? $type->data['wkhtmltopdf_options'] : $default_wk_options)),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    $type = $form_state['party_communication_type'];

    $type->data['theme']['custom_theme'] = $values['custom_theme'];
    $type->data['theme']['theme_hook_suggestions'] = explode("\n", $values['theme_hook_suggestions']);
    $type->data['wkhtmltopdf_options'] = explode("\n", $values['wkhtmltopdf_options']);
  }

  /**
   * Validate Download.
   */
  public function validateDownload($communication) {
    return ($communication->status == 'sent') || $this->validateSend($communication);
  }

  /**
   * Do Download.
   */
  public function download($communication) {
    if ($communication->status != 'sent') {
      $this->send($communication);
    }
  }

  /**
   * Download form callback.
   */
  public function downloadFormFinish($form, &$form_state) {
    $communication = $form_state['communication'];
    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    if (!$wrapper->communication_pdf->value()) {
      drupal_set_message(t('Unable to download document, file could not be found.'), 'error');
      return;
    }
    $url = file_create_url($wrapper->communication_pdf->file->value()->uri);
    $url = url($url, array(
      'query' => array(
        'download' => 'download',
      )
    ));

    // The iframe will trigger the download. Sneaky!
    drupal_set_message(t('If your download does not start automatically, please !link.!iframe', array(
      '!link' => l(t('click here'), $url),
      '!iframe' => '<iframe width="1" height="1" frameborder="0" src="' . $url . '" style="visibility:hidden"></iframe>',
    )));
  }

  /**
   * Build the communication entity used for forwarding.
   */
  public function forwardBuild($communication, $settings = array()) {
    if (
      !empty($settings['entity_templates']) &&
      empty($settings['entity_template']) &&
      count($settings['entity_templates']) === 1
    ) {
      $settings['entity_template'] = reset($settings['entity_templates']);
    }

    if (module_exists('entity_template') && !empty($settings['entity_template'])) {
      $template = entity_load_single('entity_template', $settings['entity_template']);
      $forward = $template->createNewEntityFromTemplate(array(
        'communication' => $communication,
      ));
    }
    else {
      $forward = entity_create('party_communication', array(
        'type' => $settings['type'],
        'label' => $communication->label,
      ));
    }

    // If the type is null it's a good sign we haven't successfully created a
    // forwarding communication.
    if (empty($forward->type)) {
      return FALSE;
    }

    // Only set the following if they haven't been set earlier by the template.
    if (empty($forward->communication_sender[LANGUAGE_NONE])) {
      $forward->communication_sender = $communication->communication_sender;
    }
    if (empty($forward->communication_recipients[LANGUAGE_NONE])) {
      $forward->communication_recipients = $communication->communication_recipients;
    }
    if (empty($forward->revision_log)) {
      $forward->revision_log = t('Automatically generated from PDF communication');
    }

    drupal_alter('party_communication_pdf_forward', $forward, $communication, $settings);

    return $forward;
  }

  /**
   * Forward this communication using another mode.
   */
  public function forward($communication, $settings = array()) {
    if ($communication->status != 'sent') {
      $communication->send();
    }

    if ($communication->status != 'sent') {
      drupal_set_message(t('Cannot send as PDF generation failed.'), 'error');
      return;
    }

    // Build the forward entity.
    $forward = $this->forwardBuild($communication, $settings);

    if (!$forward) {
      throw new Exception(t('Failed to build forward communication.'));
    }
    $communication->forwardCommunication = $forward;

    if (!empty($settings['date_scheduled'])) {
      $forward->timestamp = $settings['date_scheduled'];
      $forward->save();
      $date = date('m/d/Y g:ia T' ,$settings['date_scheduled']);
      drupal_set_message(t('Communication scheduled for ' . $date));
    }
    else {
      try {
        // Send and save the forward.
        $forward->save();
        $forward->send();

        // Set a message to display to the user
        if ($forward->status == 'sent') {
          drupal_set_message(t('Communication Successfully Sent'));
        }
      }
      catch (PartyCommunicationOperationException $e) {
        watchdog('party_communication', $e);
      }
    }
  }

  /**
   * Validate whether this can be forwarded.
   */
  public function forwardValidate($communication, $settings = array(), &$reasons = array()) {
    if (!in_array($communication->status, array('sent', 'ready'))) {
      $reasons[] = t('The PDF is not ready yet.');
    }

    if (empty($settings['enabled'])) {
      $reasons[] = t('Forwarding is not enabled for this communication type.');
    }

    if (empty($settings['entity_template']) && !empty($settings['entity_templates']) && count($settings['entity_templates']) > 1) {
      $reasons[] = t('Forwarding template has not been selected.');
    }

    $forward = $this->forwardBuild($communication, $settings);
    if (!$forward) {
      $reasons[] = t('Forward could not be built.');
    }
    else {
      $forward->status = 'ready';
      $forward->getType()->getController()->validateOperation('send', $forward, [], $reasons);
    }
  }

  /**
   * Validate whether this can be forwarded.
   */
  public function forwardFormValidate($form, &$form_state) {
    $date = strtotime($form_state['values']['date_scheduled']);
    if ($form_state['values']['scheduled']) {
      if (empty($form_state['values']['date_scheduled'])) {
        form_error($form['date_scheduled'], t('Date Scheduled is required'));
      }
      else if ($date < REQUEST_TIME) {
        form_error($form['date_scheduled'], t('Communications can not be scheduled in the past'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function forwardFormSubmit($form, &$form_state) {
    if (!empty($form_state['values']['entity_template'])) {
      $form_state['configuration']['entity_template'] = $form_state['values']['entity_template'];
    }
    if ($form_state['values']['scheduled'] && !empty($form_state['values']['date_scheduled'])) {
      $form_state['configuration']['date_scheduled'] = strtotime($form_state['values']['date_scheduled']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function forwardForm($form, &$form_state, $communication, $settings = array()) {
    $ops = $this->supportedOperations();
    $op = $ops[$form_state['op']];

    if (module_exists('entity_template') && !empty($communication->communication_template[LANGUAGE_NONE][0]['value'])) {
      $template = entity_load_single('entity_template', $communication->communication_template[LANGUAGE_NONE][0]['value']);

      if (!empty($template->template_allow_scheduling[LANGUAGE_NONE][0]['value'])) {
        $form['scheduled'] = array(
          '#type' => 'checkbox',
          '#title' => t('Schedule'),
          '#description' => t('Tick this box if you wish to delay sending this communication'),
        );
        $form['date_scheduled'] = array(
          '#type' => 'date_popup',
          '#title' => t('Scheduled date'),
          '#timepicker_options' => array(
            'showPeriod' => 1,
            'periodSeparator' => '',
            'amPmText' => array('am','pm'),
          ),
          '#states' => array(
            'visible' => array(
              ':input[name="scheduled"]' => array('checked' => TRUE),
            ),
            'required' => array(
              ':input[name="scheduled"]' => array('checked' => TRUE),
            ),
          ),
        );
      }
    }
    $settings += $this->settingsOperation($form_state['op'], $communication);
    if (!module_exists('entity_template') || !empty($settings['entity_template']) || empty($settings['entity_templates'])) {
      return $this->confirmForm($form, $form_state, $communication, $settings);
    }

    if (count($settings['entity_templates']) == 1) {
      $form_state['configuration']['entity_template'] = reset($settings['entity_templates']);
      return $this->confirmForm($form, $form_state, $communication, $settings);
    }

    $type = entity_load_single('party_communication_type', $settings['type']);
    $templates = entity_load('entity_template', $settings['entity_templates']);
    $options = array();
    foreach ($templates as $template) {
      $options[$template->name] = $template->label;
    }
    $form['entity_template'] = array(
      '#title' => t('!type Template', array('!type' => $type->label)),
      '#description' => t('Please select a template to use to create the !type', array('!type' => $type->label)),
      '#type' => 'select',
      '#options' => $options,
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function supportedOperations() {
    $ops = parent::supportedOperations();

    // Only override the send label if it is stull the default.
    if ($ops['send']['label'] == t('Send')) {
      $ops['send']['label'] = t('Generate PDF');
      $ops['send']['log_string'] = 'Generated PDF of @communication';
    }

    // Add a download operation.
    $ops['download'] = array(
      'label' => t('Download'),
      'log_string' => 'Downloaded @communication',
      'form' => array($this, 'confirmForm'),
      'formFinish' => array($this, 'downloadFormFinish'),
      'validate' => array($this, 'validateDownload'),
      'do' => array($this, 'download'),
    );

    return $ops;
  }
}
