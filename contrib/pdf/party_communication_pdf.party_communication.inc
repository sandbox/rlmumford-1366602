<?php

/**
 * @file
 *  Party Communication Hooks.
 */

/**
 * Implements hook_party_communication_mode_info().
 */
function party_communication_pdf_party_communication_mode_info() {
  $modes = array();
  $modes['pdf'] = array(
    'label' => t('PDF'),
    'class' => 'PartyCommunicationModePDF',
  );

  return $modes;
}
