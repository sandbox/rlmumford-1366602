<?php
/**
 * @file
 * Plugin to provide access control based on print page access.
 */

$plugin = array(
  'title' => t('PDF Print Page: viewable'),
  'description' => t('Control access based on whether the print page can be accessed.'),
  'callback' => 'party_communication_pdf_print_page_access_ctools_access_check',
  'summary' => 'party_communication_pdf_print_page_access_ctools_access_summary',
  'required context' => new ctools_context_required(t('Communicaton'), 'entity:party_communication'),
);

/**
 * Check access.
 */
function party_communication_pdf_print_page_access_ctools_access_check($conf, $context) {
  global $user;
  $comm = $context->data;
  if (empty($comm)) {
    return FALSE;
  }
  return party_communication_pdf_print_page_access($comm);
}

/**
 * Summarise.
 */
function party_communication_pdf_print_page_access_ctools_access_summary() {
  return t('PDF print page is viewable.');
}
