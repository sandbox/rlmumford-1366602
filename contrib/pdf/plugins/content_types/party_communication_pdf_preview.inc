<?php
/**
 * Plugin for specially rendered content.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Communication PDF Preview'),
  'description' => t('Rendered PDF Preview.'),
  'required context' => new ctools_context_required(t('Communication'), 'party_communication'),
  'category' => t('Communications'),
);

function party_communication_pdf_party_communication_pdf_preview_content_type_render($subtype, $conf, $panel_args, &$context) {
  $communication = $context->data;
  $path = party_communication_pdf_communication_print_path($communication, TRUE);
  $output = '<iframe src="/' . $path . '" width="100%" height="800px" id="pdf-preview"></iframe>';

  $block = new stdClass();
  $block->module = 'party_communication_pdf';
  $block->delta = 'preview';
  $block->content = $output;

  return $block;
}

function party_communication_pdf_party_communication_pdf_preview_content_type_admin_title($subtype, $conf, $context) {
  return t('Communication PDF Preview');
}

function party_communication_pdf_party_communication_pdf_preview_content_type_edit_form($form, &$form_state) {
  return $form;
}
