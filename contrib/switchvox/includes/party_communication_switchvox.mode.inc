<?php

/**
 * @file
 *   Contains mode class for switchvox.
 */

class PartyCommunicationModeSwitchVox extends PartyCommunicationModeDefault {

  /**
   * {@inheritdoc}
   */
  public function getParticipationRoles(): array {
    return [
      'caller' => [
        'label' => t('Caller'),
        'supported_primitives' => ['phone'],
        'required' => TRUE,
        'cardinality' => 1,
      ],
      'recipient' => [
        'label' => t('Recipient'),
        'supported_primitives' => ['phone'],
        'required' => TRUE,
        'cardinality' => 1,
      ],
      'participant' => [
        'label' => t('Other Participant'),
        'supported_primitives' => ['phone'],
        'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      ],
    ];
  }

  /**
   * {inheritdoc}
   */
  public function createType($bundle) {
    parent::createType($bundle);

    // Set up some defaults.
    $field_defaults = array(
      'type' => 'text',
      'cardinality' => 1,
    );
    $instance_defaults = array(
      'entity_type' => 'party_communication',
      'bundle' => $bundle,
      'settings' => array(
        'text_processing' => 0,
      ),
    );

    // Map to CALLER_ID_NUMBER
    // This is the phone number of the person making the call (the sender)
    if (!field_info_field('switchvox_caller_number')) {
      $field = array(
        'field_name' => 'switchvox_caller_number',
      ) + $field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_caller_number', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_caller_number',
        'label' => t('Caller Number'),
      ) + $instance_defaults;
      field_create_instance($instance);
    }

    // Map to CALLER_ID_NAME
    // This is the name of the person making the call taken from phone did (the
    // sender)
    if (!field_info_field('switchvox_caller_name')) {
      $field = array(
        'field_name' => 'switchvox_caller_name',
      ) + $field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_caller_name', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_caller_name',
        'label' => t('Caller Name'),
      ) + $instance_defaults;
      field_create_instance($instance);
    }

    // The number called by the caller.
    // Map to INCOMING_DID on switchvox event.
    // Use to find appropriate party.
    if (!field_info_field('switchvox_called_number')) {
      $field = array(
        'field_name' => 'switchvox_called_number',
      ) + $field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_called_number', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_called_number',
        'label' => t('Called Number'),
      ) + $instance_defaults;
      field_create_instance($instance);
    }

    // The extension of the recipient.
    if (!field_info_field('switchvox_called_extension')) {
      $field = array(
        'field_name' => 'switchvox_called_extension',
      ) + $field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_called_extension', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_called_extension',
        'label' => t('Called Extension'),
      ) + $instance_defaults;
      field_create_instance($instance);
    }

    // Map to JOB_ID
    // Unique ID of the call.
    if (!field_info_field('switchvox_call_id')) {
      $field = array(
        'field_name' => 'switchvox_call_id',
      ) + $field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_call_id', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_id',
        'label' => t('Call ID'),
      ) + $instance_defaults;
      field_create_instance($instance);
    }

    // Events
    if (!field_info_field('switchvox_call_events')) {
      $field = array(
        'field_name' => 'switchvox_call_events',
        'type' => 'text_long',
      ) + $field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_call_events', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_events',
        'label' => t('Events'),
        'settings' => array(
          'text_processing' => 1,
        ),
      ) + $instance_defaults;
      field_create_instance($instance);
    }

    // Duration in seconds
    if (!field_info_field('switchvox_call_duration')) {
      $field = array(
        'field_name' => 'switchvox_call_duration',
        'type' => 'number_integer',
      ) + $field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_call_duration', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_duration',
        'label' => t('Total Duration'),
        'settings' => array(
          'min' => 0,
          'suffix' => 's',
        ),
      ) + $instance_defaults;

      if (module_exists('timeperiod')) {
        $instance['display']['default'] = array(
          'label' => 'inline',
          'type' => 'timeperiod_default',
          'settings' => array(
            'granularity' => 2,
            'prefix_suffix' => TRUE,
          ),
        );
      }
      field_create_instance($instance);
    }

    // Talking duration in seconds
    if (!field_info_field('switchvox_call_talk_duration')) {
      $field = array(
        'field_name' => 'switchvox_call_talk_duration',
        'type' => 'number_integer',
      ) + $field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_call_talk_duration', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_talk_duration',
        'label' => t('Talk Duration'),
        'settings' => array(
          'min' => 0,
          'suffix' => 's',
        ),
      ) + $instance_defaults;

      if (module_exists('timeperiod')) {
        $instance['display']['default'] = array(
          'label' => 'inline',
          'type' => 'timeperiod_default',
          'settings' => array(
            'granularity' => 2,
            'prefix_suffix' => TRUE,
          ),
        );
      }
      field_create_instance($instance);
    }

    // Whether the call went to voicemail
    if (!field_info_field('switchvox_call_voicemail')) {
      $field = array(
        'field_name' => 'switchvox_call_voicemail',
        'type' => 'list_boolean',
        'settings' => array(
          'allowed_values' => array(
            0 => 'No',
            1 => 'Yes',
          ),
        ),
      ) + $field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_call_voicemail', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_voicemail',
        'label' => t('Voicemail'),
      ) + $instance_defaults;
      field_create_instance($instance);
    }

    // The summary.
    if (!field_info_field('switchvox_call_summary')) {
      $field = array(
        'field_name' => 'switchvox_call_summary',
        'type' => 'text_long',
        'cardinality' => 1,
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_call_summary', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_summary',
        'label' => t('Summary'),
        'settings' => array(
          'text_processing' => 1,
        ),
      ) + $instance_defaults;
      field_create_instance($instance);
    }

    // The answer time.
    if (!field_info_field('switchvox_call_answered')) {
      $field = array(
        'field_name' => 'switchvox_call_answered',
        'type' => 'date',
        'cardinality' => 1,
        'settings' => array(
          'granularity' => drupal_map_assoc(array(
            'year', 'month', 'day', 'hour', 'minute', 'second',
          )),
          'tz_handling' => 'user',
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_call_answered', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_answered',
        'label' => t('Answered Time'),
        'widget' => array(
          'type' => 'date_popup',
        ),
      ) + $instance_defaults;
      field_create_instance($instance);
    }

    // The hang up time.
    if (!field_info_field('switchvox_call_ended')) {
      $field = array(
        'field_name' => 'switchvox_call_ended',
        'type' => 'date',
        'cardinality' => 1,
        'settings' => array(
          'granularity' => drupal_map_assoc(array(
            'year', 'month', 'day', 'hour', 'minute', 'second',
          )),
          'tz_handling' => 'user',
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_call_ended', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_ended',
        'label' => t('Call Ended Time'),
        'widget' => array(
          'type' => 'date_popup',
        ),
      ) + $instance_defaults;
      field_create_instance($instance);
    }

    if (!field_info_field('switchvox_call_conversations')) {
      $field = array(
        'field_name' => 'switchvox_call_conversations',
        'type' => 'field_collection',
        'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      );
      field_create_field($field);

      $item_bundle = 'switchvox_call_conversations';
      $item_inst_def = array(
        'entity_type' => 'field_collection_item',
        'bundle' => 'switchvox_call_conversations',
      );

      if (!field_info_field('conversation_caller_number')) {
        $field = array(
          'field_name' => 'conversation_caller_number',
          'type' => 'text',
          'cardinality' => 1,
        );
        field_create_field($field);
      }
      if (!field_info_instance('field_collection_item', 'conversation_caller_number', $item_bundle)) {
        $instance = array(
          'field_name' => 'conversation_caller_number',
          'label' => t('Caller Number'),
        ) + $item_inst_def;
        field_create_instance($instance);
      }
      if (!field_info_field('conversation_called_number')) {
        $field = array(
          'field_name' => 'conversation_called_number',
          'type' => 'text',
          'cardinality' => 1,
        );
        field_create_field($field);
      }
      if (!field_info_instance('field_collection_item', 'conversation_called_number', $item_bundle)) {
        $instance = array(
          'field_name' => 'conversation_called_number',
          'label' => t('Called Number'),
        ) + $item_inst_def;
        field_create_instance($instance);
      }
      if (!field_info_field('conversation_called_extension')) {
        $field = array(
          'field_name' => 'conversation_called_extension',
          'type' => 'text',
          'cardinality' => 1,
        );
        field_create_field($field);
      }
      if (!field_info_instance('field_collection_item', 'conversation_called_extension', $item_bundle)) {
        $instance = array(
          'field_name' => 'conversation_called_extension',
          'label' => t('Called Number'),
        ) + $item_inst_def;
        field_create_instance($instance);
      }
      if (!field_info_field('conversation_caller')) {
        $field = array(
          'field_name' => 'conversation_caller',
          'type' => 'entityreference',
          'cardinality' => 1,
          'settings' => array(
            'target_type' => 'party',
          ),
        );
        field_create_field($field);
      }
      if (!field_info_instance('field_collection_item', 'conversation_caller', $item_bundle)) {
        $instance = array(
          'field_name' => 'conversation_caller',
          'label' => t('Caller'),
        ) + $item_inst_def;
        field_create_instance($instance);
      }
      if (!field_info_field('conversation_recipient')) {
        $field = array(
          'field_name' => 'conversation_recipient',
          'type' => 'entityreference',
          'cardinality' => 1,
          'settings' => array(
            'target_type' => 'party',
          ),
        );
        field_create_field($field);
      }
      if (!field_info_instance('field_collection_item', 'conversation_recipient', $item_bundle)) {
        $instance = array(
          'field_name' => 'conversation_recipient',
          'label' => t('Caller'),
        ) + $item_inst_def;
        field_create_instance($instance);
      }
      if (!field_info_field('conversation_ended')) {
        $field = array(
          'field_name' => 'conversation_ended',
          'type' => 'date',
          'cardinality' => 1,
          'settings' => array(
            'granularity' => drupal_map_assoc(array('year', 'month', 'day', 'hour', 'minute', 'second')),
          ),
          'tz_handling' => 'user',
        );
        field_create_field($field);
      }
      if (!field_info_instance('field_collection_item', 'conversation_ended', $item_bundle)) {
        $instance = array(
          'field_name' => 'conversation_ended',
          'label' => t('End Time'),
          'widget' => array(
            'type' => 'date_popup',
          ),
        ) + $item_inst_def;
        field_create_instance($instance);
      }
      if (!field_info_field('conversation_started')) {
        $field = array(
          'field_name' => 'conversation_started',
          'type' => 'date',
          'cardinality' => 1,
          'settings' => array(
            'granularity' => drupal_map_assoc(array('year', 'month', 'day', 'hour', 'minute', 'second')),
          ),
          'tz_handling' => 'user',
        );
        field_create_field($field);
      }
      if (!field_info_instance('field_collection_item', 'conversation_started', $item_bundle)) {
        $instance = array(
          'field_name' => 'conversation_started',
          'label' => t('Start Time'),
          'widget' => array(
            'type' => 'date_popup',
          ),
        ) + $item_inst_def;
        field_create_instance($instance);
      }
      if (!field_info_field('conversation_duration')) {
        $field = array(
          'field_name' => 'conversation_duration',
          'type' => 'number_integer',
          'cardinality' => 1,
        );
        field_create_field($field);
      }
      if (!field_info_instance('field_collection_item', 'conversation_duration', $item_bundle)) {
        $instance = array(
          'field_name' => 'conversation_duration',
          'label' => t('Conversation Duration'),
          'settings' => array(
            'min' => 0,
            'suffix' => 's',
          ),
        ) + $item_inst_def;

        if (module_exists('timeperiod')) {
          $instance['display']['default'] = array(
            'label' => 'inline',
            'type' => 'timeperiod_default',
            'settings' => array(
              'granularity' => 2,
              'prefix_suffix' => TRUE,
            ),
          );
        }
        field_create_instance($instance);
      }
      // The transcript.
      if (!field_info_field('conversation_transcript')) {
        $field = array(
          'field_name' => 'conversation_transcript',
          'type' => 'text_long',
          'cardinality' => 1,
        );
        field_create_field($field);
      }
      if (!field_info_instance('field_collection_item', 'conversation_transcript', $item_bundle)) {
        $instance = array(
          'field_name' => 'conversation_transcript',
          'label' => t('Transcript'),
          'settings' => array(
            'text_processing' => 1,
          ),
        ) + $item_inst_def;
        field_create_instance($instance);
      }
      if (!field_info_field('conversation_recording')) {
        $field = array(
          'field_name' => 'conversation_recording',
          'type' => 'file',
          'cardinality' => 1,
          'settings' => array(
            'uri_scheme' => 'private',
          ),
        );
        field_create_field($field);
      }
      if (!field_info_instance('field_collection_item', 'conversation_recording', $item_bundle)) {
        $instance = array(
          'field_name' => 'conversation_recording',
          'label' => t('Call Recording'),
          'description' => t('The maximum size allowed to upload a file is 100 MB.'),
          'widget' => array(
            'weight' => '55',
            'type' => 'dragndrop_upload_file',
            'module' => 'dragndrop_upload_file',
            'active' => 1,
            'settings' => array(
              'progress_indicator' => 'throbber',
              'upload_event' => 'manual',
              'upload_button_text' => 'Upload',
              'droppable_area_text' => 'Drop files here to upload',
              'standard_upload' => 1,
              'allow_replace' => 0,
              'multiupload' => 0,
              'media_browser' => 0,
            ),
          ),
          'settings' => array(
            'file_directory' => 'communications/call_recordings',
            'file_extensions' => 'mp3 wav wma ogg avi flv wmv mov mp4',
            'max_filesize' => '',
          ),
        ) + $item_inst_def;
        field_create_instance($instance);
      }
    }
    if (!field_info_instance('party_communication', 'switchvox_call_conversations', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_conversations',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Conversations'),
      );
      field_create_instance($instance);
    }
    if (!field_info_field('switchvox_call_vm_id')) {
      $field = array(
        'field_name' => 'switchvox_call_vm_id',
        'type' => 'text',
        'cardinality' => 1,
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_call_vm_id', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_vm_id',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Voicemail ID'),
      );
      field_create_instance($instance);
    }
    // Duration in seconds
    if (!field_info_field('switchvox_call_vm_duration')) {
      $field = array(
        'field_name' => 'switchvox_call_vm_duration',
        'type' => 'number_integer',
      ) + $field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_call_vm_duration', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_vm_duration',
        'label' => t('Voicemail Duration'),
        'settings' => array(
          'min' => 0,
          'suffix' => 's',
        ),
      ) + $instance_defaults;

      if (module_exists('timeperiod')) {
        $instance['display']['default'] = array(
          'label' => 'inline',
          'type' => 'timeperiod_default',
          'settings' => array(
            'granularity' => 2,
            'prefix_suffix' => TRUE,
          ),
        );
      }
      field_create_instance($instance);
    }
    if (!field_info_field('switchvox_call_vm_recording')) {
      $field = array(
        'field_name' => 'switchvox_call_vm_recording',
        'type' => 'file',
        'cardinality' => 1,
        'settings' => array(
          'uri_scheme' => 'private',
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_call_vm_recording', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_vm_recording',
        'label' => t('Voicemail Recording'),
        'description' => t('The maximum size allowed to upload a file is 100 MB.'),
        'widget' => array(
          'weight' => '55',
          'type' => 'dragndrop_upload_file',
          'module' => 'dragndrop_upload_file',
          'active' => 1,
          'settings' => array(
            'progress_indicator' => 'throbber',
            'upload_event' => 'manual',
            'upload_button_text' => 'Upload',
            'droppable_area_text' => 'Drop files here to upload',
            'standard_upload' => 1,
            'allow_replace' => 0,
            'multiupload' => 0,
            'media_browser' => 0,
          ),
        ),
        'settings' => array(
          'file_directory' => 'communications/vm_recordings',
          'file_extensions' => 'mp3 wav wma ogg avi flv wmv mov mp4',
          'max_filesize' => '',
        ),
      ) + $instance_defaults;
      field_create_instance($instance);
    }
    if (!field_info_field('switchvox_call_vm_recipient')) {
      $field = array(
        'field_name' => 'switchvox_call_vm_recipient',
        'type' => 'entityreference',
        'cardinality' => 1,
        'settings' => array(
          'target_type' => 'party',
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'switchvox_call_vm_recipient', $bundle)) {
      $instance = array(
        'field_name' => 'switchvox_call_vm_recipient',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Voicemail Recipient'),
      );
      field_create_instance($instance);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function send($communication, $settings = array()) {
    $this->prepare($communication);
    $communication->save();
  }

  /**
   * {@inheritdoc}
   */
  public function validateSend($communication, $settings = array(), &$reasons = array()) {
    parent::validateSend($communication, $settings, $reasons);

    if (!$this->getRecipientNumber($communication)) {
      $reasons[] = t('The Recipient has no phone number.');
    }

    if (empty($reasons)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Get the phone number from the recipient.
   */
  public function getRecipientNumber($communication) {
    if (!empty($communication->switchvox_called_number[LANGUAGE_NONE][0]['value'])) {
      return $communication->switchvox_called_number[LANGUAGE_NONE][0]['value'];
    }

    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    $party = $wrapper->communication_recipients;

    try {
      // Loop over the phone numbers and get the most appropriate.
      foreach ($party->profile2_party_indiv->field_contact_phone as $phone_wrapper) {
        $number = $phone_wrapper->field_phone_number->value();
        if (!empty($number)) {
          return $number;
        }
      }
    }
    catch (Exception $e) {
      return NULL;
    }
  }

  /**
   * Turn off all supported operations.
   */
  public function supportedOperations() {
    return array(
      'incoming_call' => array(
        'do' => array($this, 'incomingCallProcess'),
        'log_string' => 'Incoming Call from @sender',
        'label' => t('Incoming Call'),
      ),
      'outgoing_call' => array(
        'do' => array($this, 'outgoingCallProcess'),
        'log_string' => 'Outgoing Call to @recipient',
        'label' => t('Outgoing Call'),
      ),
      'routed' => array(
        'do' => array($this, 'callRoutedProcess'),
        'log_string' => 'Routed Call to @recipient',
        'label' => t('Routed Call'),
      ),
      'call_answered' => array(
        'do' => array($this, 'callAnsweredProcess'),
        'label' => t('Call Answered'),
      ),
      'call_hung_up' => array(
        'do' => array($this, 'callHungUpProcess'),
        'label' => t('Call Hung Up'),
      ),
      'new_voicemail' => array(
        'do' => array($this, 'newVoicemailProcess'),
        'label' => t('New Voicemail'),
      ),
    );
  }

  /**
   * Process routing of a call.
   */
  public function callRoutedProcess($communication, $settings = array()) {
    $this->terminateOpenConversation($communication);
    $wrapper = entity_metadata_wrapper('party_communication', $communication);

    $communication->is_new_revision = TRUE;
    $communication->revision_log = t('Call routed to extension @extension', array('@extension' => $settings['extension']));

    if ($settings['extension_type'] == 'send_to_vm') {
      $communication->revision_log .= ' voicemail';
    }
    $communication->save();
  }

  /**
   * Process answering of a call.
   */
  public function callAnsweredProcess($communication, $settings = array()) {
    $time = isset($settings['timestamp']) ? (int) $settings['timestamp'] : REQUEST_TIME;

    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    if (!$wrapper->switchvox_call_answered->value()) {
      $wrapper->switchvox_call_answered = $time;
    }

    $conversation = entity_create('field_collection_item', array('field_name' => 'switchvox_call_conversations'));
    $conversation->setHostEntity('party_communication', $communication);
    $conv_wrapper = $conversation->wrapper();
    $conv_wrapper->conversation_started = $time;

    if ($communication->inoutbound == PARTY_COMMUNICATION_INBOUND) {
      $conv_wrapper->conversation_caller = $wrapper->communication_sender->value();
      $conv_wrapper->conversation_caller_number = $wrapper->switchvox_caller_number->value();

      $conv_wrapper->conversation_called_number = $settings['extension'];
      $conv_wrapper->conversation_called_extension = $settings['extension'];

      try {
        if ($recipient = $this->acquireParty($communication, $settings['extension'])) {
          $conv_wrapper->conversation_recipient = $recipient;
          $wrapper->communication_recipients = $recipient;
        }
      }
      catch (\Exception $exception) {
        // Do nothing and let conversation generation continue.
      }
    }
    else {
      $conv_wrapper->conversation_recipient = $wrapper->communication_recipients->value();
      $conv_wrapper->conversation_called_number = $wrapper->switchvox_called_number->value();

      if ($settings['extension'] == 's') {
        $conv_wrapper->conversation_caller_number = $wrapper->switchvox_caller_number->value();
        $caller = $wrapper->communication_sender->value();
      }
      else {
        $conv_wrapper->conversation_caller_number = $settings['extension'];
        try {
          $caller = $this->acquireParty($communication, $settings['extension']);
        }
        catch (\Exception $exception) {
          // Do nothing and let conversation generation continue.
        }
      }

      if (!empty($caller)) {
        $conv_wrapper->conversation_caller = $caller;
        $wrapper->communication_sender = $caller;
      }
    }

    try {
      module_invoke_all('party_communication_switchvox_call_answered', $communication, $settings);
      rules_invoke_event_by_args('party_communication_switchvox_call_answered', array($communication));
    }
    catch (\Exception $exception) {
      watchdog_exception('switchvox', $exception);
    }

    // Get the answerer party.
    $label = !empty($settings['caller_name']) ? $settings['caller_name'] : 'Anonymous';
    if ($label == 'Anonymous' && $answerer = $this->acquireParty($communication, $settings['caller_number'])) {
      $label = $answerer->label;
    }

    $communication->is_new_revision = TRUE;
    $communication->revision_log = t('Call answered by @label', array('@label' => $label));
    $wrapper->save();
  }

  /**
   * Process hanging up a call.
   */
  public function callHungUpProcess($communication, $settings = array()) {
    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    $wrapper->switchvox_call_ended = isset($settings['timestamp']) ? (int) $settings['timestamp'] : REQUEST_TIME;

    $answered = $wrapper->switchvox_call_answered->value();
    $wrapper->switchvox_call_duration = !empty($answered) ? (isset($settings['timestamp']) ? (int) $settings['timestamp'] : REQUEST_TIME) - $wrapper->switchvox_call_answered->value() : 0;

    $this->terminateOpenConversation($communication);

    module_invoke_all('party_communication_switchvox_call_hung_up', $communication, $settings);
    rules_invoke_event_by_args('party_communication_switchvox_call_hung_up', array($communication));

    $communication->is_new_revision = TRUE;
    $communication->revision_log = t(
      '%op Communication for Switchvox Hung Up Event',
      array('%op' => !empty($communication->is_new) ? 'Created' : 'Updated')
    );
    $wrapper->save();
  }

  /**
   * Process incoming call.
   */
  public function incomingCallProcess($communication, $settings = array()) {
    // Terminate an open conversation.
    if ($this->terminateOpenConversation($communication)) {
      $communication->is_new_revision = TRUE;
      $communication->revision_log = t('Conversation ended.');
      $communication->save();
    }

    // Don't do anything if this communication already exists.
    if (empty($communication->is_new)) {
      return;
    }

    try {
      $communication->inoutbound = PARTY_COMMUNICATION_INBOUND;
      $communication->timestamp = isset($settings['timestamp']) ? (int) $settings['timestamp'] : REQUEST_TIME;
      $wrapper = entity_metadata_wrapper('party_communication', $communication);
      $values_map = array(
        'caller_number' => 'switchvox_caller_number',
        'caller_name' => 'switchvox_caller_name',
        'called_number' => 'switchvox_called_number',
      );
      foreach ($values_map as $setting => $field) {
        if (!empty($settings[$setting])) {
          $wrapper->{$field} = $settings[$setting];
        }
      }

      // Prepare initial response.
      $response = new stdClass();

      // Attempt to acquire contact information.
      if ($caller_party = $this->acquireParty($communication, $settings['caller_number'])) {
        $wrapper->communication_sender = $caller_party;
        $response->caller_id_name = $caller_party->label;
      }

      if ($called_party = $this->acquireParty($communication, $settings['extension'])) {
        $wrapper->communication_recipients = $called_party;
      }

      $wrapper->switchvox_response = $response;
      module_invoke_all('party_communication_switchvox_incoming_call', $communication, $settings);
      rules_invoke_event_by_args('party_communication_switchvox_incoming_call', array($communication));

      if (empty($communication->is_new)) {
        $communication->is_new_revision = TRUE;
      }
      $communication->revision_log = t(
        '%op Communication for Switchvox Incoming Call Event',
        array('%op' => !empty($communication->is_new) ? 'Created' : 'Updated')
      );
      $communication->save();

      // Set the response display_url.
      if (!$wrapper->switchvox_response->display_url->value()) {
        $uri = entity_uri('party_communication', $wrapper->value());
        $wrapper->switchvox_response->display_url = is_array($uri) ? url($uri['path'], array('absolute' => TRUE) + $uri['options']) : url($uri, array('absolute' => TRUE));
      }

      if ($response = $wrapper->switchvox_response->value()) {
        $output = "<response><result><call_info>";
        foreach ($response as $name => $value) {
          if (empty($value)) {
            continue;
          }
          $output .= "<{$name}>$value</{$name}>";
        }
        $output .= "</call_info></result></response>";
        print $output;
      }
    }
    catch (Exception $e) {
      watchdog_exception('switchvox', $e);
    }
  }

  /**
   * Process outgoing calls.
   */
  public function outgoingCallProcess($communication, $settings = array()) {
    // Terminate an open conversation.
    if ($this->terminateOpenConversation($communication)) {
      $communication->is_new_revision = TRUE;
      $communication->revision_log = t('Conversation ended.');
      $communication->save();
    }

    // Don't do anything if this communication already exists.
    if (empty($communication->is_new)) {
      return;
    }

    $communication->inoutbound = PARTY_COMMUNICATION_OUTBOUND;
    $communication->timestamp = isset($settings['timestamp']) ? (int) $settings['timestamp'] : REQUEST_TIME;
    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    $values_map = array(
      'caller_number' => 'switchvox_caller_number',
      'caller_name' => 'switchvox_caller_name',
      'extension' => 'switchvox_called_number',
    );
    foreach ($values_map as $setting => $field) {
      if (!empty($settings[$setting])) {
        $wrapper->{$field} = $settings[$setting];
      }
    }

    if ($caller_party = $this->acquireParty($communication, $settings['caller_number'])) {
      $wrapper->communication_sender = $caller_party;
    }

    if ($called_party = $this->acquireParty($communication, $settings['extension'])) {
      $wrapper->communication_recipients = $called_party;
    }

    module_invoke_all('party_communication_switchvox_outgoing_call', $communication, $settings);

    if (empty($communication->is_new)) {
      $communication->is_new_revision = TRUE;
    }
    $communication->revision_log = t(
      '%op Communication for Switchvox Outgoing Call Event',
      array('%op' => !empty($communication->is_new) ? 'Created' : 'Updated')
    );
    $communication->save();
  }

  /**
   * Voicemail processing.
   */
  public function newVoicemailProcess($communication, $settings = array()) {
    $communication->wrapper()->switchvox_call_vm_id = $settings['vm_id'];
    $communication->wrapper()->switchvox_call_vm_duration = ceil($settings['vm_duration']);

    if ($recipient = $this->acquireParty($communication, $settings['extension'])) {
      $communication->wrapper()->switchvox_call_vm_recipient = $recipient;
    }

    $communication->is_new_revision = TRUE;
    $communication->revision_log = t(
      'New voicemail received at extension @extension',
      array('@extension' => $settings['extension'])
    );
    $communication->save();
  }

  /**
   * Terminate any open conversation.
   */
  protected function terminateOpenConversation($communication) {
    if (empty($communication->switchvox_call_conversations[LANGUAGE_NONE])) {
      return;
    }

    $ended_conversation = FALSE;
    foreach ($communication->switchvox_call_conversations[LANGUAGE_NONE] as &$item) {
      $entity = field_collection_field_get_entity($item);
      if (!empty($entity->conversation_ended[LANGUAGE_NONE][0]['value'])) {
        continue;
      }

      $item['entity'] = $entity;
      $entity->wrapper()->conversation_ended = REQUEST_TIME;
      $entity->wrapper()->conversation_duration = $entity->wrapper()->conversation_ended->value() - $entity->wrapper()->conversation_started->value();

      $talk_duration = $communication->wrapper()->switchvox_call_talk_duration->value();
      $talk_duration += $entity->wrapper()->conversation_duration->value();
      $communication->wrapper()->switchvox_call_talk_duration = $talk_duration;
      $ended_conversation = TRUE;
      break;
    }

    return $ended_conversation;
  }

  /**
   * Acquire a party for the sender if we only have an email address.
   */
  public function acquireParty($communication, $phone) {
    $context = array(
      'name' => 'party_communication_inbound',
      'class' => 'PartyCommunicationContactAcquisition',
      'primitive' => 'phone',
      'communication' => $communication,
      'behavior' => PartyAcquisitionInterface::BEHAVIOR_NOTHING,
      'party_hat' => array(
        'add' => array(
          'party_indiv',
        ),
      ),
    );
    $values['phone'] = $phone;

    $party = party_acquire($values, $context, $method);
    if ($method == 'create') {
      $party->save();
      foreach ($party->data_set_controllers as $controller) {
        $controller->save(TRUE);
      }
    }

    return $party;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, &$form_state) {
    $type = $form_state['party_communication_type'];
    $form['sync_on_cron'] = array(
      '#type' => 'checkbox',
      '#title' => t('Sync Calls from the SwitchVox API on Cron'),
      '#description' => t('When this is checked completed calls will be loaded from SwitchVox on cron.'),
      '#default_value' => !empty($type->data['sync_on_cron']),
    );

    $form['ignore_incoming_ext'] = array(
      '#type' => 'textarea',
      '#title' => t('Ignore Incoming by Extension'),
      '#description' => t('Ignore incoming based on the extension. Enter one extension per line.'),
      '#default_value' => !empty($type->data['ignore_incoming_ext']) ? implode("\n", $type->data['ignore_incoming_ext']) : '',
    );

    $form['switchvox'] = array(
      '#type' => 'fieldset',
      '#title' => t('Credentials'),
      '#states' => array(
        'visible' => array(
          ':input[name="mode_settings[sync_on_cron]"]' => array('checked' => TRUE),
        ),
        'required' => array(
          ':input[name="mode_settings[sync_on_cron]"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['switchvox']['host'] = array(
      '#type' => 'textfield',
      '#title' => t('Switchvox Host'),
      '#description' => t('The URL of the switchvox host.'),
      '#default_value' => !empty($type->data['switchvox']['host']) ? $type->data['switchvox']['host'] : '',
    );
    $form['switchvox']['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => !empty($type->data['switchvox']['username']) ? $type->data['switchvox']['username'] : '',
    );
    $form['switchvox']['password_input'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#element_validate' => array(array($this, 'passwordInputValidate')),
      '#description' => t(
        'The current password is !pass, leave blank to keep using this password.',
        array(
          '!pass' => !empty($type->data['switchvox']['password']) ? str_repeat("*", strlen($type->data['switchvox']['password'])) : 'empty',
        )
      ),
    );
    $form['switchvox']['password'] = array(
      '#type' => 'value',
      '#value' => !empty($type->data['switchvox']['password']) ? $type->data['switchvox']['password'] : '',
    );

    /*
    $form['sync_on_hang_up'] = array(
      '#type' => 'checkbox',
      '#title' => t('Sync Calls from the SwitchVox API when they hang up.'),
      '#description' => t('When this is checked completed calls will be loaded from SwitchVox on cron.'),
      '#default_value' => FALSE,
    );
     */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormValidate($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    if ($values['sync_on_cron']) {
      if (empty($values['switchvox']['host'])) {
        form_error($form['switchvox']['host'], t('Host is required to use web service functions.'));
      }
      if (empty($values['switchvox']['username'])) {
        form_error($form['switchvox']['username'], t('Username is required to use web service functions.'));
      }
      if (empty($values['switchvox']['password'])) {
        form_error($form['switchvox']['password_input'], t('Password is required to use web service functions.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    $type = $form_state['party_communication_type'];

    $type->data['sync_on_cron'] = $values['sync_on_cron'];

    $type->data['ignore_incoming_ext'] = array();
    $ignored_exts = explode("\n", $values['ignore_incoming_ext']);
    foreach ($ignored_exts as $ext) {
      $type->data['ignore_incoming_ext'][] = trim($ext);
    }

    $type->data['switchvox'] = array_filter($values['switchvox']);
  }
}


