<?php

/**
 * @file
 *  Party Communication Hooks.
 */

/**
 * Implements hook_party_communication_status_info().
 */
function party_communication_switchvox_party_communication_status_info() {
  $statuses = array();
  $statuses['switchvox_answered'] = array(
    'label' => t('Call Answered'),
    'manual' => TRUE,
    'modes' => array(
      'switchvox' => 'switchvox',
    ),
  );
  $statuses['switchvox_missed'] = array(
    'label' => t('Call Missed'),
    'manual' => TRUE,
    'participation_locked' => TRUE,
    'modes' => array(
      'switchvox' => 'switchvox',
    ),
  );
  $statuses['switchvox_voicemail'] = array(
    'label' => t('Voicemail'),
    'manual' => TRUE,
    'modes' => array(
      'switchvox' => 'switchvox',
    ),
  );
  return $statuses;
}

/**
 * Implements hook_party_communication_status_info_alter().
 */
function party_communication_switchvox_party_communication_status_info_alter(&$statuses) {
  unset($statuses['draft']['modes']['switchvox']);
  $statuses['failed']['overrides']['switchvox']['label'] = t('Call Failed');
}

/**
 * Implements hook_party_communication_mode_info().
 */
function party_communication_switchvox_party_communication_mode_info() {
  $modes = array();
  $modes['switchvox'] = array(
    'label' => t('SwitchVox'),
    'class' => 'PartyCommunicationModeSwitchVox',
  );

  return $modes;
}
