<?php
/**
 * @file
 *  Rules integration.
 */

/**
 * Implements hook_rules_event_info().
 */
function party_communication_switchvox_rules_event_info() {
  $variables = array(
    'communication' => array(
      'label' => t('Phone Call'),
      'type' => 'party_communication',
      'skip save' => TRUE,
    ),
  );
  $events['party_communication_switchvox_incoming_call'] = array(
    'label' => t('When a new call starts'),
    'group' => t('SwitchVox'),
    'variables' => $variables,
  );
  $events['party_communication_switchvox_call_answered'] = array(
    'label' => t('When a call is answered'),
    'group' => t('SwitchVox'),
    'variables' => $variables,
  );
  $events['party_communication_switchvox_call_hung_up'] = array(
    'label' => t('When a call is hung up'),
    'group' => t('SwitchVox'),
    'variables' => $variables,
  );
  return $events;
}
