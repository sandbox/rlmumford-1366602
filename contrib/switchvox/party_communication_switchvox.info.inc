<?php
/**
 * @file
 *   Entity property info for switchvox communications.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function party_communication_switchvox_entity_property_info_alter(&$info) {
  $property = array(
    'type' => 'struct',
    'label' => t('Switchvox Response'),
    'description' => t('Switchvox Response Information'),
    'getter callback' => 'entity_property_verbatim_get',
    'setter callback' => 'entity_property_verbatim_set',
    'property info' => array(
      'caller_id_number' => array(
        'type' => 'text',
        'label' => t('Caller ID Number'),
        'description' => t('Set Caller ID Number to this value.'),
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      ),
      'caller_id_name' => array(
        'type' => 'text',
        'label' => t('Caller ID Name'),
        'description' => t('Set Caller ID Name to this value.'),
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      ),
      'transfer_extension' => array(
        'type' => 'text',
        'label' => t('Transfer Extension'),
        'description' => t('Transfer call to the extension of this value.'),
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      ),
      'display_url' => array(
        'type' => 'text',
        'label' => t('Display URL'),
        'description' => t('Display this URL in the Switchboard as a clickable icon.'),
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
      ),
    ),
  );

  foreach (entity_load('party_communication_type') as $type) {
     if (!($type->getController() instanceof PartyCommunicationModeSwitchVox)) {
       continue;
     }

     $info['party_communication']['bundles'][$type->name]['properties']['switchvox_response'] = $property;
  }
}
