<?php

/**
 * @file
 *  Party Communication Hooks.
 */

/**
 * Implements hook_party_communication_mode_info().
 */
function party_communication_email_party_communication_mode_info() {
  $modes = array();
  $modes['email'] = array(
    'label' => t('Email'),
    'class' => 'PartyCommunicationModeEmail',
  );

  return $modes;
}

/**
 * Implements hook_party_communication_status_info().
 */
function party_communication_email_party_communication_status_info() {
  $statuses = array();
  $statuses['email_opened'] = array(
    'label' => t('Email Opened'),
    'manual' => TRUE,
    'participation_locked' => TRUE,
    'modes' => array(
      'email' => 'email',
    ),
  );
  $statuses['email_delivered'] = array(
    'label' => t('Email Delivered'),
    'manual' => TRUE,
    'participation_locked' => TRUE,
    'modes' => array(
      'email' => 'email',
    ),
  );
  $statuses['email_failed'] = array(
    'label' => t('Email Failed'),
    'manual' => TRUE,
    'participation_locked' => TRUE,
    'modes' => array(
      'email' => 'email',
    ),
  );
  return $statuses;
}
