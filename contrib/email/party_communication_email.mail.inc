<?php

/**
 * Party Communication Mail system.
 */
class PartyCommunicationMailSystem implements MailSystemInterface {

  /**
   * The system that would be used if party communication were not enabled.
   *
   * @var MailSystemInterface
   */
  protected $defaultSystem;

  /**
   * Construct a new PartyCommunication mailsystem.
   */
  public function __construct() {
    $this->defaultSystem = drupal_mail_system('party_communication_email', 'default');
    if ($this->defaultSystem instanceof PartyCommunicationMailSystem) {
      if (module_exists('mimemail')) {
        $this->defaultSystem = new MimeMailSystem();
      }
      else {
        $this->defaultSystem = new DefaultMailSystem();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function format(array $message) {
    if (is_array($message['body'])) {
      $message['body'] = implode("\n\n", $message['body']);
    }

    if (substr($message['id'], 0, 9) != 'mimemail_') {
      $message['plain_text'] = TRUE;
      $message['body'] = drupal_html_to_text($message['body']);
    }

    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function mail(array $message) {
    $type = variable_get('party_communication_email_system_type', FALSE);
    if (empty($type)) {
      throw new Exception('No communication type set for mail system emails.');
    }

    $communication = entity_create('party_communication', array('type' => $type));
    unset($communication->communication_sender);
    $communication->label = $message['subject'];
    $communication->inoutbound = PARTY_COMMUNICATION_OUTBOUND;

    // Handle to email address.
    if (is_array($message['to'])) {
      $tos = $message['to'];
    }
    else {
      $tos = explode(',', $message['to']);
    }
    foreach ($tos as $to) {
      $communication->communication_email_to[LANGUAGE_NONE][] = array(
        'email' => $to,
      );
    }

    // Handle from email address.
    $from = mailparse_rfc822_parse_addresses($message['from']);
    $from = reset($from);
    if (!empty($from)) {
      $communication->communication_email_sender[LANGUAGE_NONE][0]['email'] = $from['address'];
      $communication->communication_email_sender_name[LANGUAGE_NONE][0]['value'] = $from['display'];
    }

    // Handle other email addresses.
    $address_headers = array(
      'cc' => 'Cc',
      'bcc' => 'Bcc',
      'reply-to' => 'Reply-to',
    );
    foreach ($address_headers as $param_key => $header_key) {
      $header = array();
      if (!empty($message['headers'][$header_key])) {
        $header = $message['headers'][$header_key];
      }
      else if (!empty($message['params'][$param_key])) {
        $header = $message['params'][$param_key];
      }

      if (!is_array($header)) {
        $header = explode(',', $header);
      }

      $field_name = 'communication_email_'.str_replace('-','_', $param_key);
      foreach ($header as $item) {
        $communication->{$field_name}[LANGUAGE_NONE][] = array(
          'email' => $item,
        );
      }
    }

    // Handle any attachments.
    $attachments = $message['params']['attachments'];
    $public_realpath = drupal_realpath('public://').'/';
    $private_realpath = drupal_realpath('private://').'/';
    foreach ($attachments as $attachment) {
      $realpath = $attachment['filepath'];
      $uri = str_replace(array($public_realpath, $private_realpath), array('public://', 'private://'), $realpath);

      $fid = db_select('file_managed', 'f')
        ->condition('uri', $uri)
        ->fields('f', array('fid'))
        ->execute()
        ->fetchField();

      if ($fid) {
        $communication->communication_email_attachments[LANGUAGE_NONE][] = array(
          'fid' => $fid,
          'display' => 1,
        );
      }
    }

    // Handle body.
    if (!empty($message['plain_text'])) {
      $communication->communication_email_body_plain[LANGUAGE_NONE][0]['value'] = $message['body'];
    }
    else {
      $communication->communication_email_body[LANGUAGE_NONE][0] = array(
        'value' => $message['body'],
        'format' => variable_get('mimemail_format', filter_fallback_format()),
      );
      if (!empty($message['params']['plain'])) {
        $communication->communication_email_body_plain[LANGUAGE_NONE][0] = array(
          'value' => $message['params']['plain'],
        );
      }
    }

    $communication->save();
    $communication->send();

    return ($communication->status == 'sent');
  }
}
