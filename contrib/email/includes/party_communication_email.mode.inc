<?php

/**
 * @file
 *   Contains mode class for twilio.
 */

class PartyCommunicationModeEmail extends PartyCommunicationModeLongText  {

  /**
   * Get mailgun credentials.
   *
   * @param \PartyCommunication $communication
   *   The communication.
   *
   * @return array
   *   Array with keys 'domain' and 'api_key'
   */
  public function getMailgunCredentials(PartyCommunication $communication) {
    $type = $communication->getType();
    $creds = [
      'domain' => $type->data['mailgun']['domain_name'],
      'api_key' => $type->data['mailgun']['api_key'],
    ];

    drupal_alter('party_communication_mailgun_credentials', $creds, $communication);

    return $creds;
  }

  /**
   * {@inheritdoc}
   */
  public function getParticipationRoles(): array {
    return [
      'sender' => [
        'label' => t('From'),
        'supported_primitives' => ['email'],
        'required' => TRUE,
        'cardinality' => 1,
      ],
      'to' => [
        'label' => t('To'),
        'supported_primitives' => ['email'],
        'required' => TRUE,
        'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      ],
      'cc' => [
        'label' => t('Carbon-Copy'),
        'supported_primitives' => ['email'],
        'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      ],
      'bcc' => [
        'label' => t('Blind Carbon-Copy'),
        'supported_primitives' => ['email'],
        'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      ],
      'reply_to' => [
        'label' => t('Reply-To'),
        'supported_primitives' => ['email'],
        'cardinality' => 1,
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function supportedOperations() {
    $operations = parent::supportedOperations();
    $operations['track_open'] = array(
      'do' => array($this, 'trackOpen'),
      'log_string' => 'Opened by @recipient',
      'label' => 'Track Open',
      'mailgun_event' => 'opened',
    );
    $operations['track_delivery'] = array(
      'do' => array($this, 'trackDelivery'),
      'log_string' => 'Delivered to @recipient',
      'label' => 'Track Delivery',
      'mailgun_event' => 'delivered',
    );
    $operations['track_failure'] = array(
      'do' => array($this, 'trackFailure'),
      'log_string' => 'Failed to deliver',
      'label' => 'Track Failure',
      'mailgun_event' => 'failed',
    );

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function communicationContentFields($context = 'build') {
    $fields = array(
      'communication_email_body' => t('Body'),
      'communication_email_body_plain' => t('Body (Plain Text)'),
    );

    // Do not render the email or body when the we're previewing or sending
    // content.
    if ($context != 'build') {
      unset($fields['communication_email_body_plain']);
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function createType($bundle) {
    parent::createType($bundle);
    $email_field_defaults = array(
      'type' => 'email',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    );
    $email_instance_defaults = array(
      'entity_type' => 'party_communication',
      'bundle' => $bundle,
    );

    if (!field_info_field('communication_email_sender')) {
      $field = array(
        'field_name' => 'communication_email_sender',
        'cardinality' => 1,
      ) + $email_field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_email_sender', $bundle)) {
      $instance = array(
        'field_name' => 'communication_email_sender',
        'label' => t('Sender Email'),
        'description' => t('Email Address of the Sender'),
      ) + $email_instance_defaults;
      field_create_instance($instance);
    }
    if (!field_info_field('communication_email_sender_name')) {
      $field = array(
        'field_name' => 'communication_email_sender_name',
        'cardinality' => 1,
        'type' => 'text',
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_email_sender_name', $bundle)) {
      $instance = array(
        'field_name' => 'communication_email_sender_name',
        'label' => t('Sender Name'),
        'description' => t('Name of the Sender'),
      ) + $email_instance_defaults;
      field_create_instance($instance);
    }
    if (!field_info_field('communication_email_to')) {
      $field = array(
        'field_name' => 'communication_email_to',
      ) + $email_field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_email_to', $bundle)) {
      $instance = array(
        'field_name' => 'communication_email_to',
        'label' => t('To'),
        'description' => t('Email Addresses the email was sent to.'),
        'widget' => array(
          'type' => 'field_extrawidgets_read_only',
        ),
      ) + $email_instance_defaults;
      field_create_instance($instance);
    }
    if (!field_info_field('communication_email_cc')) {
      $field = array(
        'field_name' => 'communication_email_cc',
      ) + $email_field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_email_cc', $bundle)) {
      $instance = array(
        'field_name' => 'communication_email_cc',
        'label' => t('CC'),
        'description' => t('Email Addresses to send Carbon Copies.'),
      ) + $email_instance_defaults;
      field_create_instance($instance);
    }
    if (!field_info_field('communication_email_bcc')) {
      $field = array(
        'field_name' => 'communication_email_bcc',
      ) + $email_field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_email_bcc', $bundle)) {
      $instance = array(
        'field_name' => 'communication_email_bcc',
        'label' => t('BCC'),
        'description' => t('Email Addresses to send Blind Carbon Copies.'),
      ) + $email_instance_defaults;
      field_create_instance($instance);
    }
    if (!field_info_field('communication_email_attachments')) {
      $field = array(
        'field_name' => 'communication_email_attachments',
        'type' => 'file',
        'cardinality' => FIELD_CARDINALITY_UNLIMITED,
        'settings' => array(
          'uri_scheme' => 'private',
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_email_attachments', $bundle)) {
      $instance = array(
        'field_name' => 'communication_email_attachments',
        'label' => t('Attachments'),
        'description' => t('Any attachments to send with the email.'),
        'settings' => array(
          'file_extensions' => 'jpg png txt doc docx xls xlsx pdf csv zip gz',
          'file_directory' => 'communication/email/attachements',
        ),
      ) + $email_instance_defaults;
      field_create_instance($instance);
    }
    if (!field_info_field('communication_email_reply_to')) {
      $field = array(
        'field_name' => 'communication_email_reply_to',
        'cardinality' => 1,
      ) + $email_field_defaults;
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_email_reply_to', $bundle)) {
      $instance = array(
        'field_name' => 'communication_email_reply_to',
        'label' => t('Reply-To'),
        'description' => t('Where replies to this email should be sent.'),
      ) + $email_instance_defaults;
      field_create_instance($instance);
    }
    if (!field_info_field('communication_email_id')) {
      $field = array(
        'field_name' => 'communication_email_id',
        'type' => 'text',
        'cardinality' => 1,
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_email_id', $bundle)) {
      $instance = array(
        'field_name' => 'communication_email_id',
        'label' => t('Email ID'),
        'description' => t('Internal Email ID. Used to track emails against external webservices.'),
      ) + $email_instance_defaults;
      field_create_instance($instance);
    }

    $date_fields = array(
      'opened' => t('Opened Date'),
      'delivered' => t('Delivery Date'),
    );
    foreach ($date_fields as $name => $label) {
      $field_name = 'communication_email_'.$name;
      if (!field_info_field($field_name)) {
        $field = array(
          'field_name' => $field_name,
          'label' => $label,
          'type' => 'datetime',
          'settings' => array(
            'granularity' => drupal_map_assoc(array('month', 'day', 'year', 'minute', 'second', 'hour')),
            'tz_handling' => 'user',
          ),
          'cardinality' => 1,
        );
        field_create_field($field);
      }
      if (!field_info_instance('party_communication', $field_name, $bundle)) {
        $instance = array(
          'field_name' => $field_name,
          'entity_type' => 'party_communication',
          'bundle' => $bundle,
          'label' => $label,
        );
        field_create_instance($instance);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function prepare($communication, $settings = array()) {
    parent::prepare($communication, $settings);

    $settings += array(
      'email_preference' => array(),
    );

    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    if (count($wrapper->communication_email_to) < 1
        && $wrapper->communication_recipients->value()) {
      $email_keys = $settings['email_preference'];
      $email_data = PartyCommunicationContact::getData(
        $wrapper->communication_recipients->value(),
        'email',
        $email_keys
      );
      if ($email_data && $email_data->getData()) {
        $communication->communication_email_to[LANGUAGE_NONE][0]['email'] = $email_data->getData();
      }
    }

    if (!$wrapper->communication_email_sender->value() && $wrapper->communication_sender->value()) {
      $communication->communication_email_sender[LANGUAGE_NONE][0]['email']
        = $wrapper->communication_sender->email->value();
    }

    if (!$wrapper->communication_email_sender_name->value() && $wrapper->communication_sender->value()) {
      $communication->communication_email_sender_name[LANGUAGE_NONE][0]['value']
        = $wrapper->communication_sender->label->value();
    }
  }

  /**
   * Concatenate email addresses.
   */
  public function concatEmails(\PartyCommunication $communication, $type = 'to') {
    if ($communication->getParticipations($type)) {
      $emails = [];
      foreach ($communication->getParticipations($type) as $participation) {
        $emails[] = $participation->contactName() ?
          $participation->contactName() . ' <' . $participation->contactData()->getData() . '>' :
          $participation->contactData()->getData();
      }
      return !empty($emails) ? implode(', ', $emails) : NULL;
    }

    // @todo: Remove once old fields are dealt with.
    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    $field_name = 'communication_email_'.$type;

    $emails = array();
    foreach ($wrapper->{$field_name} as $email) {
      $emails[] = $email->value();
    }
    return !empty($emails) ?  implode(', ', $emails) : NULL;
  }

  /**
   * Concatenate email addresses.
   */
  public function prepareFrom(\PartyCommunication $communication) {
    if ($sender = $communication->getParticipation('sender')) {
      return $sender->contactName() ?
        $sender->contactName() . ' <' . $sender->contactData()->getData() . '>' :
        $sender->contactData()->getData();
    }

    // @todo: Remove once confident that a sender participation is always made.
    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    $from = NULL;
    if ($wrapper->communication_email_sender->value()) {
      $from = $wrapper->communication_email_sender->value();
      if ($name = $wrapper->communication_email_sender_name->value()) {
        $from = "{$name} <{$from}>";
      }
    }
    return $from;
  }

  /**
   * {@inheritdoc}
   */
  public function validateSend($communication, $settings = array(), &$reasons = array()) {
    $comm_for_validate = clone $communication;
    $this->prepare($comm_for_validate, $settings);

    if (empty($comm_for_validate->communication_email_to[LANGUAGE_NONE][0]['email'])) {
      $reasons[] = t('Could not find email address for recipient.');
      return FALSE;
    }

    $email = $comm_for_validate->communication_email_to[LANGUAGE_NONE][0]['email'];
    $provided_addresses = array_map(fn($e) => (is_array($e) ? $e['address'] : $e),  mailparse_rfc822_parse_addresses($email));
    $valid_emails = array_filter($provided_addresses, [$this, 'validateEmailAddress']);

    $validator = new PartyCommunicationEmailValidator($this->config['mailgun']['api_key']);
    $validation_results = $validator->validateAddresses($valid_emails);
    foreach ($valid_emails as $key => $address) {
      if (empty($validation_results[$key]['allow_send'])) {
        unset($valid_emails[$key]);
      }
    }

    if (empty($valid_emails)) {
      $message = count($provided_addresses) > 1 ?
        '!email does not contain a valid email address.' :
        '!email is not a valid email address.';
      $reasons[] = t($message, array('!email' => $email));
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Validate an email address.
   *
   * @param string|array $email
   *   Either an email address or an array from mailparse_rfc822_parse_addresses().
   *
   * @return boolean
   *   True if the email is valid, false otherwise.
   */
  public function validateEmailAddress($email) {
    if (is_array($email)) {
      $email = $email['address'];
    }

    return filter_var($email, FILTER_VALIDATE_EMAIL);
  }

  /**
   * {@inheritdoc}
   */
  public function send($communication, $settings = array()) {
    if (empty($settings['skip_validation']) && !$this->validateSend($communication, $settings, $reasons)) {
      $communication->revision_log = t('Failed to send:')."\n".implode("\n", $reasons);
      $communication->status = 'failed';
    }
    else {
      $this->prepare($communication, $settings);

      $type = $communication->getType();
      $engine = !empty($type->data['mail_engine']) ? $type->data['mail_engine'] : 'mimemail';

      if (call_user_func([$this, 'send' . $engine], $communication)) {
        $communication->revision_log = t('Sent Email');
        $communication->status = 'sent';
      }
      else {
        $communication->status = 'failed';
      }
    }

    $communication->is_new_revision = TRUE;
    $communication->revision_timestamp = REQUEST_TIME;
    $communication->timestamp = REQUEST_TIME;
    $communication->save();
  }

  /**
   * Send with mailgun API.
   */
  public function sendMailgun(\PartyCommunication $communication) {
    $wrapper = entity_metadata_wrapper('party_communication', $communication);

    // Build postdata.
    $params = array();
    $params['from'] = $this->prepareFrom($communication);
    $params['to'] = $this->concatEmails($communication);
    $params['cc'] = $this->concatEmails($communication, 'cc');
    $params['bcc'] = $this->concatEmails($communication, 'bcc');

    // reply to
    if ($reply_to = $communication->getRoleParticipation('reply_to')) {
      $params['h:Reply-To'] = $reply_to->contactData()->getData();
    }
    else if ($wrapper->communication_email_reply_to->value()) {
      $params['h:Reply-To'] = $wrapper->communication_email_reply_to->value();
    }

    // Email Subject.
    $params['subject'] = $wrapper->label->value();

    // Email Content.
    $plain_text = $communication->communication_email_body_plain[LANGUAGE_NONE][0]['value'];
    if (!empty($communication->communication_email_body[LANGUAGE_NONE][0]['value'])) {
      $build = entity_view('party_communication', array($wrapper->getIdentifier() => $wrapper->value()), 'send');
      $content = drupal_render($build);
      if (empty($plain_text)) {
        $plain_text = drupal_html_to_text($content);
      }

      $params['html'] = $content;

      // Expand all local links.
      $pattern = '/(<a[^>]+href=")([^"]*)/mi';
      $params['html'] = preg_replace_callback($pattern, array($this, 'expandLinks'), $params['html']);
    }
    $params['text'] = $plain_text;

    // Threading of emails.
    if ($wrapper->communication_in_reply_to->value() && $wrapper->communication_in_reply_to->communication_email_id->value()) {
      $params['h:In-Reply-To'] = $wrapper->communication_in_reply_to->communication_email_id->value();
    }

    // Attachments
    $params['attachment'] = array();
    $delta = 1;
    foreach ($wrapper->communication_email_attachments as $attachment) {
      $uri = $attachment->file->value()->uri;
      $path = drupal_realpath($uri);

      if (is_readable($path)) {
        $params["attachment[{$delta}]"] = new CURLFile($path, $attachment->file->value()->filemime);
        $delta++;
      }
    }

    // Make sure the communication is saved before it is sent.
    if (empty($communication->id)) {
      $communication->is_new_revision = TRUE;
      $communication->revision_log = t('Saving to generate ID number for mailgun.');
      $communication->save();
    }
    $params['v:communication-id'] = $communication->id;

    drupal_alter('party_communication_mailgun_params', $params, $communication);

    // Remove cc and bcc if there is nothing in them.
    $params = array_filter($params);
    $credentials = $this->getMailgunCredentials($communication);

    if (!variable_get('party_communication_email_mailgun_enabled', TRUE)) {
      $params['o:testmode'] = TRUE;
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.mailgun.net/v3/{$credentials['domain']}/messages");
    curl_setopt($ch, CURLOPT_USERPWD, "api:{$credentials['api_key']}");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $response = curl_exec($ch);
      curl_close($ch);

    $response = json_decode($response);
    if (!empty($response->id)) {
      $communication->communication_email_id[LANGUAGE_NONE][0]['value'] = $response->id;
      return TRUE;
    }
    else {
      $communication->revision_log = t('Failed to send after sending to mailgun. @json', ['@json' => json_encode($response)]);
      $communication->status = 'failed';
    }

    return FALSE;
  }

  /**
   * Expand links for emails.
   *
   * @see _mimemail_expand_links.
   */
  public function expandLinks($matches) {
     return $matches[1] . $this->expandLinkUrl($matches[2]);
  }

  /**
   * Expand link for email.
   *
   * @see _mimemail_url
   */
  public function expandLinkUrl($url, $to_embed = NULL) {
    global $base_url;
    $url = urldecode($url);

    $to_link = variable_get('mimemail_linkonly', 0);
    $is_image = preg_match('!\.(png|gif|jpg|jpeg)!i', $url);
    $is_absolute = file_uri_scheme($url) != FALSE || preg_match('!(mailto|callto|tel)\:!', $url);

    if (!$to_embed) {
      if ($is_absolute) {
        return str_replace(' ', '%20', $url);
      }
    }
    else {
      $url = preg_replace('!^' . base_path() . '!', '', $url, 1);
      if ($is_image) {
        // Remove security token from URL, this allows for styled image embedding.
        // @see https://drupal.org/drupal-7.20-release-notes
        $url = preg_replace('/\\?itok=.*$/', '', $url);
        if ($to_link) {
          // Exclude images from embedding if needed.
          $url = file_create_url($url);
          $url = str_replace(' ', '%20', $url);
        }
      }
      return $url;
    }

    $url = str_replace('?q=', '', $url);
    @list($url, $fragment) = explode('#', $url, 2);
    @list($path, $query) = explode('?', $url, 2);

    // If we're dealing with an intra-document reference, return it.
    if (empty($path)) {
      return '#' . $fragment;
    }

    // Get a list of enabled languages.
    $languages = language_list('enabled');
    $languages = $languages [1];

    // Default language settings.
    $prefix = '';
    $language = language_default();

    // Check for language prefix.
    $path = trim($path, '/');
    $args = explode('/', $path);
    foreach ($languages as $lang) {
      if (!empty($args) && $args [0] == $lang->prefix) {
        $prefix = array_shift($args);
        $language = $lang;
        $path = implode('/', $args);
        break;
      }
    }

    $options = array(
      'query' => ($query) ? drupal_get_query_array($query) : array(),
      'fragment' => $fragment,
      'absolute' => TRUE,
      'language' => $language,
      'prefix' => $prefix,
    );

    $url = url($path, $options);

    // If url() added a ?q= where there should not be one, remove it.
    if (preg_match('!^\?q=*!', $url)) {
      $url = preg_replace('!\?q=!', '', $url);
    }

    $url = str_replace('+', '%2B', $url);
    return $url;
  }

  /**
   * Send with built in drupal mimemail.
   */
  public function sendMimemail($communication) {
    $wrapper = entity_metadata_wrapper('party_communication', $communication);

    // Build Necessaries.
    $to = $this->concatEmails($communication);
    $from = $this->prepareFrom($communication);
    $params['cc'] = $this->concatEmails($communication, 'cc');
    $params['bcc'] = $this->concatEmails($communication, 'bcc');

    // reply to
    if ($wrapper->communication_email_reply_to->value()) {
      $params['reply-to'] = $wrapper->communication_email_reply_to->value();
    }

    $attachment_paths = array();
    foreach ($wrapper->communication_email_attachments as $attachment) {
      $attachment_paths[] = $attachment->file->value()->uri;
    }
    if (!empty($attachment_paths)) {
      $params['attachments'] = implode("\n", $attachment_paths);
    }

    $params['context']['subject'] = $wrapper->label->value();
    $build = entity_view('party_communication', array($wrapper->getIdentifier() => $wrapper->value()), 'send');
    $content = drupal_render($build);
    if ($content) {
      $params['context']['body'] = $content;
      $params['plaintext'] = strip_tags($wrapper->communication_email_body_plain->value()['value']);
    }
    else {
      $params['context']['body'] = $wrapper->communication_email_body_plain->value()['value'];
    }
    drupal_mail('party_communication_email', 'mimemail', $to, language_default(), $params, $from);
    return TRUE;
  }

  /**
   * Receive an email.
   */
  public function receive($type) {
    $communication = entity_create('party_communication', array(
      'type' => $type->name,
      'inoutbound' => PARTY_COMMUNICATION_INBOUND,
      'status' => 'received',
    ));
    $communication->is_new_revision = TRUE;
    $communication->revision_log = t('Received Email');
    $communication->revision_timestamp = REQUEST_TIME;

    $engine = !empty($type->data['mail_engine']) ? $type->data['mail_engine'] : 'mimemail';
    $output = call_user_func(array($this, 'receive'.$engine), $communication);
    drupal_alter('party_communication_email_inbound', $communication);

    $communication->save();
    return $output;
  }

  /**
   * Receive a mailgun email.
   */
  public function receiveMailgun(\PartyCommunication $communication) {
    // Test that the email has not been received already.
    $query = db_select('field_data_communication_email_id');
    $query->condition('communication_email_id_value', $_REQUEST['Message-Id']);
    if ($query->countQuery()->execute()->fetchField()) {
      throw new \Exception("Message {$_REQUEST['Message-Id']} has already been received.");
    }

    $type = $communication->getType();
    $wrapper = entity_metadata_wrapper('party_communication', $communication);
    $communication->label = $_REQUEST['subject'];
    $communication->timestamp = $_REQUEST['timestamp'];

    // Parse from and store.
    $from = $this->parseAddressList($_REQUEST['from']);
    $from = reset($from);
    $communication->addParticipation(PartyCommunicationParticipation::create(
      $communication,
      'sender',
      'sender',
      PartyCommunicationContact::createData('email', $from['email']),
      $from['name'] ?: NULL
    )->setNeedsAcquisition(TRUE, [
      'behavior' => \PartyAcquisitionInterface::BEHAVIOR_CREATE,
      'defaults' => $from['name'] && ($name_parts = preg_split('/\s+/', $from['name'])) ? [
        'profile2_party_indiv' => [
          'party_indiv_name' => [
            'given' => array_shift($name_parts),
            'family' => array_pop($name_parts),
            'middle' => implode(' ', $name_parts),
          ],
        ],
      ] : [],
    ]));

    $communication->addParticipation(PartyCommunicationParticipation::create(
      $communication,
      'to',
      'to',
      PartyCommunicationContact::createData('email', $_REQUEST['recipient']),
    )->setNeedsAcquisition(TRUE, [
      'behavior' => \PartyAcquisitionInterface::BEHAVIOR_NOTHING,
    ]));

    // Get messages and try to strip out <html> and <head> tags.
    $html_content = $_REQUEST['stripped-html'];
    $html = new DOMDocument();
    $html->loadHtml(mb_convert_encoding($html_content, 'HTML-ENTITIES', 'UTF-8'));
    if ($body = $html->getElementsByTagName("body")->item(0)) {
      $subset = new DOMDocument();
      foreach ($body->childNodes as $element) {
        $subset->appendChild($subset->importNode($element->cloneNode(TRUE), TRUE));
      }
      $html_content = $subset->saveHtml();
    }

    $communication->communication_email_body[LANGUAGE_NONE][0] = array(
      'value' => $html_content,
      'format' => 'panopoly_wysiwyg_text',
    );
    $communication->communication_email_body_plain[LANGUAGE_NONE][0] = array(
      'value' => $_REQUEST['stripped-text'],
      'format' => 'plain_text',
    );

    // Set the communication id.
    $communication->communication_email_id[LANGUAGE_NONE][0]['value'] = $_REQUEST['Message-Id'];

    // Work out the in-reply to.
    if (!empty($_REQUEST['In-Reply-To'])) {
      $query = new EntityFieldQuery();
      $query->fieldCondition('communication_email_id', 'value', $_REQUEST['In-Reply-To']);
      $query->entityCondition('entity_type', 'party_communication');
      $result = $query->execute();
      if (!empty($result['party_communication'])) {
        $ids = array_keys($result['party_communication']);
        $communication->communication_in_reply_to[LANGUAGE_NONE][0]['target_id'] = reset($ids);
      }
    }

    // Do attachments.
    $content_id_map = json_decode($_REQUEST['content-id-map'], TRUE) ?: [];
    $cid_to_url = [];
    for ($i = 1; $i <= $_REQUEST['attachment-count']; $i++) {
      if (empty($_FILES['attachment-'.$i]) || !is_array($_FILES['attachment-'.$i])) {
        continue;
      }

      $file_info = &$_FILES['attachment-'.$i];
      if (module_exists('transliteration') && variable_get('transliteration_file_uploads', TRUE)) {
        $file_info['orig_name'] = $file_info['name'];
        $file_info['name'] = transliteration_clean_filename($file_info['orig_name']);
      }

      // Handle errors
      switch ($file_info['error']) {
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_PARTIAL:
        case UPLOAD_ERR_NO_FILE:
          continue 2;
        case UPLOAD_ERR_OK:
          if (!is_uploaded_file($file_info['tmp_name'])) {
            continue 2;
          }
          break;
        default:
          continue 2;
      }

      // Build the file entity.
      $file = new stdClass();
      $file->uid = 0;
      $file->status = 1;
      $file->filename = trim(drupal_basename($file_info['name']));
      $file->uri = $file_info['tmp_name'];
      $file->filemime = file_get_mimetype($file->filename);
      $file->filesize = $file_info['size'];
      $file->source = 'email attachment';

      $extensions = 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp';
      if (!empty($type->data['mailgun']['attachment_extensions'])) {
        $extensions = $type->data['mailgun']['attachment_extensions'];
      }
      $file->filename = file_munge_filename($file->filename, $extensions);

      // Consider folders per email.
      $destination = "private://attachments";
      file_prepare_directory($destination, FILE_CREATE_DIRECTORY);
      $file->destination = file_destination($destination . '/' .$file->filename, FILE_EXISTS_RENAME);
      $file->uri = $file->destination;
      if (!drupal_move_uploaded_file($file_info['tmp_name'], $file->uri)) {
        continue;
      }
      drupal_chmod($file->uri);
      $file = file_save($file);

      if ($file->fid) {
        $file_item = array(
          'fid' => $file->fid,
          'display' => 1,
          'description' => '',
        );
        $communication->communication_email_attachments[LANGUAGE_NONE][] = $file_item;

        if ($cid = array_search('attachment-'.$i, $content_id_map)) {
          $cid_to_url['cid:'.$cid] = file_create_url($file->uri);
        }
      }
    }

    if (!empty($cid_to_url)) {
      $communication->communication_email_body[LANGUAGE_NONE][0]['value'] = str_replace(
        array_keys($cid_to_url),
        array_values($cid_to_url),
        $communication->communication_email_body[LANGUAGE_NONE][0]['value']
      );
    }
  }

  /**
   * Parse from.
   *
   * @return array
   *   With the following keys 'name', 'email'.
   */
  public function parseAddressList($list) {
    $pattern = '/^(?:"?((?:[^"\\<>]|\\\\.)+)"?(\s)+)?<?(([a-z0-9._%\-+]+)@[a-z0-9.\-]+\.[a-z]{2,10})>?$/i';

    $parts = str_getcsv($list);
    $result = array();
    foreach($parts as $part) {
      $part = trim($part);
      if (preg_match($pattern, $part, $matches)) {
        $item = array();
        $item['name'] = trim(stripcslashes($matches[1])) ?: trim($matches[4]);
        $item['email'] = trim($matches[3]);
        $result[] = $item;
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function labelElementTitle($communication) {
    return t('Subject');
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, &$form_state) {
    $type = $form_state['party_communication_type'];
    $data = $type->data;

    $form['mail_engine'] = array(
      '#type' => 'select',
      '#title' => t('E-mail Engine'),
      '#options' => array(
        'mimemail' => t('Mimemail'),
        'mailgun' => t('Mailgun'),
      ),
      '#default_value' => !empty($data['mail_engine']) ? $data['mail_engine'] : 'mimemail',
    );

    $form['mailgun'] = array(
      '#type' => 'container',
      '#states' => array(
        'visible' => array(
          ':input[name="mode_settings[mail_engine]"]' => array('value' => 'mailgun'),
        ),
      ),
    );
    $form['mailgun']['domain_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Domain Name'),
      '#description' => t('Domain name to send emails to and from.'),
      '#default_value' => !empty($data['mailgun']['domain_name']) ? $data['mailgun']['domain_name'] : '',
    );

    $form['mailgun']['api_key_input'] = array(
      '#type' => 'password',
      '#title' => t('API Key'),
      '#description' => t(
        'The current API key is !pass, leave blank to keep using this key.',
        array(
          '!pass' => !empty($type->data['mailgun']['api_key']) ? str_repeat("*", strlen($type->data['mailgun']['api_key'])) : 'empty',
        )
      ),
      '#element_validate' => array(array($this, 'passwordInputValidate')),
    );
    $form['mailgun']['api_key'] = array(
      '#type' => 'value',
      '#value' => !empty($type->data['mailgun']['api_key']) ? $type->data['mailgun']['api_key'] : '',
    );
    $form['mailgun']['help'] = array(
      '#markup' => t(
        'To configure incoming email, go to <em>https://mailgun.com/app/routes/new</em> to create a "Route" that forwards emails to %url.',
        array(
          '%url' => url('party_communication_email/'.$type->name, array('absolute' => TRUE)),
        )
      ),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    $type = $form_state['party_communication_type'];

    $type->data['mail_engine'] = $values['mail_engine'];

    if ($values['mail_engine'] != 'mailgun') {
      unset($type->data['mailgun']['domain_name']);
      unset($type->data['mailgun']['api_key']);
    }
    else {
      $type->data['mailgun'] = array_filter($values['mailgun']);
    }
  }

  /**
   * Track when the email was opened
   */
  public function trackOpen($communication, $settings = array()) {
    if ($communication->status != 'email_opened') {
      $communication->wrapper()->communication_email_opened = floor(!empty($settings['timestamp']) ? $settings['timestamp'] : REQUEST_TIME);
      $communication->status = 'email_opened';
      $communication->revision_log = "Recording Opening: ".json_encode($settings);
      $communication->is_new_revision = TRUE;
      $communication->save();
    }
  }

  /**
   * Track when the email was delivered.
   */
  public function trackDelivery($communication, $settings = array()) {
    if ($communication->status == 'sent') {
      $communication->wrapper()->communication_email_delivered = floor(!empty($settings['timestamp']) ? $settings['timestamp'] : REQUEST_TIME);
      $communication->status = 'email_delivered';
      $communication->is_new_revision = TRUE;
      $communication->revision_log = "Recording Delivery: ".json_encode($settings);
      $communication->save();
    }
  }

  /**
   * Track when the email has failed.
   */
  public function trackFailure($communication, $settings = array()) {
    if ($communication->status == 'sent') {
      $communication->is_new_revision = TRUE;
      $communication->revision_log = "Reason: {$settings['reason']} \nDescription: {$settings['delivery-status']['description']}";
      $communication->status = 'email_failed';
      $communication->save();
    }
  }

  /**
   * Verify Event.
   */
  public function validateEvent($settings = array(), &$reasons = array()) {
    // Only support verify event for mailgun
    if ($this->config['mail_engine'] != 'mailgun') {
      $reasons[] = 'Incorrect Mail Engine';
    }

    $signature = $settings['signature'] ?: [
      'token' => $settings['token'],
      'timestamp' => $settings['timestamp'],
      'signature' => $settings['signature'],
    ];
    $event = $settings['event-data'] ?: $settings;

    // Check we support the operation.
    $supported = FALSE;
    foreach ($this->supportedOperations() as $op) {
      if (!empty($op['mailgun_event']) && ($op['mailgun_event'] == $event['event'])) {
        $supported = TRUE;
      }
    }
    if (!$supported) {
      $reasons[] = 'The event '.$event['event'].' is not supported.';
    }

    // Check the timestamp is not too far out.
    if (\abs(REQUEST_TIME - $signature['timestamp']) > 15) {
      $reasons[] = 'The event message is too delayed';
    }

    // Check the access is correct.
    if (!\hash_equals(\hash_hmac('sha256', $signature['timestamp'] . $signature['token'], $this->config['mailgun']['api_key']), $signature['signature'])) {
      $reasons[] = 'Invalid security signature';
    }

    // Try and find a matching communication.
    if (!empty($event['user-variables']['communication-id'] ?? $settings['communication-id']) && ($comm = entity_load_single('party_communication', $event['user-variables']['communication-id'] ?? $settings['communication-id']))) {
      if (empty($reasons)) {
        return TRUE;
      }
    }
    else {
      $reasons[] = 'Could not find communication '.($event['user-variables']['communication-id'] ?? $settings['communication-id']);
    }

    return FALSE;
  }

  /**
   * Handle Event.
   */
  public function handleEvent($settings = array()) {
    $reasons = array();
    if (!$this->validateEvent($settings, $reasons)) {
      $message = "Invalid Event";
      if (!empty($reasons)) {
        $message .= ": ".implode(", ", $reasons);
      }
      throw new Exception($message);
    }

    $event = $settings['event-data'] ?? $settings;
    if (!empty($event['user-variables']['communication-id'] ?? $settings['communication-id']) && ($comm = entity_load_single('party_communication', $event['user-variables']['communication-id'] ?? $settings['communication-id']))) {
      $op = NULL;
      foreach ($this->supportedOperations() as $op_name => $supported_op) {
        if (!empty($supported_op['mailgun_event']) && ($supported_op['mailgun_event'] == $event['event'])) {
          $op = $op_name;
        }
      }

      if (empty($op)) {
        return;
      }
      $this->doOperation($op, $comm, $event);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postTypeSave($type) {
    parent::postTypeSave($type);

    // Mailgun Webhooks.
    $mailgun_webhooks = array('delivered', 'opened', 'permanent_fail', 'temporary_fail');

    // When the type is save, if it is mailgun then create the webhooks.
    if ($this->config['mail_engine'] != 'mailgun') {
      // If there is an original then we shall delete the webhooks.
      if (!empty($type->original->data['mail_engine']) && ($type->original->data['mail_engine'] == 'mailgun')) {
        foreach ($mailgun_webhooks as $hook) {
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, "https://api.mailgun.net/v3/domains/{$type->original->data['mailgun']['domain_name']}/webhooks/{$hook}");
          curl_setopt($ch, CURLOPT_USERPWD, "api:{$type->original->data['mailgun']['api_key']}");
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_exec($ch);
          curl_close($ch);
        }
      }
      return;
    }

    $domain = $this->config['mailgun']['domain_name'];
    $api_key = $this->config['mailgun']['api_key'];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.mailgun.net/v3/domains/{$domain}/webhooks");
    curl_setopt($ch, CURLOPT_USERPWD, "api:{$api_key}");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);

    $webhooks = json_decode($response, TRUE);
    foreach ($mailgun_webhooks as $hook) {
      if (empty($webhooks['webhooks'][$hook])) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.mailgun.net/v3/domains/{$domain}/webhooks");
        curl_setopt($ch, CURLOPT_USERPWD, "api:{$api_key}");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
          'id' => $hook,
          'url' => url("party_communication_email/{$type->name}/event", array('absolute' => TRUE)),
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
      }
    }
  }

}

