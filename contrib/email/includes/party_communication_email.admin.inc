<?php

/**
 * Form to configure party communication email system.
 */
function party_communication_email_settings_form($form, &$form_state) {
  $mailsystem_configuration = variable_get('mail_system', array('default-system' => 'DefaultMailSystem'));

  $form['use_party_communication_mail_system'] = array(
    '#title' => t('Use the Communication system for all email'),
    '#description' => t('This will set the default mailsystem to use the party communication email system.'),
    '#type' => 'checkbox',
    '#default_value' => ($mailsystem_configuration['default-system'] == 'PartyCommunicationMailSystem'),
  );

  $form['party_communication_email_system'] = array(
    '#title' => t('Communication Mail System'),
    '#description' => t('Mailsystem to use when party communication is sending the email.'),
    '#type' => 'textfield',
    '#default_value' => $mailsystem_configuration['party_communication_email'],
  );

  if (function_exists('mailsystem_get_classes')) {
    $options = mailsystem_get_classes();
    unset($options['PartyCommunicationMailSystem']);
    $form['party_communication_email_system']['#type'] = 'select';
    $form['party_communication_email_system']['#options'] = $options;
  }

  $type_options = db_select('party_communication_type', 't')
    ->condition('mode', 'email')
    ->fields('t', array('name', 'label'))
    ->execute()
    ->fetchAllKeyed();
  $form['party_communication_email_system_type'] = array(
    '#type' => 'select',
    '#title' => t('Communication Type'),
    '#description' => t('Select the communication type to use for emails.'),
    '#options' => $type_options,
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save Configuration'),
    ),
  );

  return $form;
}

/**
 * Validate callback.
 */
function party_communication_email_settings_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['use_party_communication_mail_system'])) {
    if (empty($form_state['values']['party_communication_email_system'])) {
      form_error($form['party_communication_email_system'], t('You must specify a mailsystem to use for communication emails.'));
    }
    if (empty($form_state['values']['party_communication_email_system_type'])) {
      form_error($form['party_communication_email_system_type'], t('You must specify an email communication type.'));
    }
  }
}

/**
 * Submit callback.
 */
function party_communication_email_settings_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['use_party_communication_mail_system'])) {
    $mailsystem_configuration = variable_get('mail_system', array('default-system' => 'DefaultMailSystem'));
    $mailsystem_configuration['default-system'] = 'PartyCommunicationMailSystem';
    if (!empty($mailsystem_configuration['mimemail'])) {
      $mailsystem_configuration['mimemail'] = 'PartyCommunicationMailSystem';
    }

    $mailsystem_configuration['party_communication_email'] = $form_state['values']['party_communication_email_system'];
    variable_set('mail_system', $mailsystem_configuration);

    variable_set('party_communication_email_system_type', $form_state['values']['party_communication_email_system_type']);
  }
}
