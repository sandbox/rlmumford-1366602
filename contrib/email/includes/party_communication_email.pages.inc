<?php
/**
 * @file
 * Contains incoming email handling.
 */

/**
 * Page callback for handling an incoming message.
 */
function party_communication_email_incoming($type) {
  try {
    $output = $type->getController()->receive($type);
    drupal_add_http_header('Status', 200);
  }
  catch (Exception $e) {
    watchdog_exception('inbound email', $e);
    drupal_add_http_header('Status', 406);
    print $e->getMessage();
  }
  drupal_exit();
}

/**
 * Page callback for handling mail events.
 */
function party_communication_email_event($type) {
  try {
    $settings = $_POST;
    if ($_SERVER['HTTP_CONTENT_TYPE'] === "application/json") {
      $settings = json_decode(file_get_contents("php://input"), TRUE);
    }
    $output = $type->getController()->handleEvent($settings);
    drupal_add_http_header('Status', 200);
  }
  catch (Exception $e) {
    watchdog_exception('email event', $e);
    drupal_add_http_header('Status', 406);
    print "Access Denied";
  }
  drupal_exit();
}
