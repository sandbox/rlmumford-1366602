<?php

use Mailgun\Exception\HttpClientException;
use Mailgun\Mailgun;
use Psr\Http\Client\ClientExceptionInterface;

/**
 * Class PartyCommunicationEmailValidator
 *
 * The PartyCommunicationEmailValidator class is responsible for validating email addresses
 * using the Mailgun API and storing the validation results in a database table.
 */
class PartyCommunicationEmailValidator {

  /**
   * Result Constants.
   */
  const DELIVERABLE = 0;
  const UNDELIVERABLE = 1;
  const DO_NOT_SEND = 2;
  const CATCH_ALL = 3;
  const UNKNOWN = 4;

  /**
   * Risk constants.
   */
  const RISK_UNKNOWN = 0;
  const RISK_LOW = 1;
  const RISK_MEDIUM = 2;
  const RISK_HIGH = 3;

  /**
   * @var \Mailgun\Mailgun|null $mailgunClient
   *   An instance of the Mailgun API client class.
   */
  protected ?Mailgun $mailgunClient = NULL;

  /**
   * Constructor for the class.
   *
   * @param \Mailgun\Mailgun|string|null $mailgun_client
   *   The Mailgun client. Can be an instance of the Mailgun class or the string API key.
   */
  public function __construct($mailgun_client = NULL) {
    if ($mailgun_client && class_exists('\Mailgun\Mailgun') && $mailgun_client instanceof Mailgun) {
      $this->mailgunClient = $mailgun_client;
    }
    else if (is_string($mailgun_client) && class_exists('\Mailgun\Mailgun')) {
      $this->mailgunClient = Mailgun::create($mailgun_client);
    }
  }

  /**
   * Returns the Mailgun client.
   *
   * The Mailgun client is created using the provided API key obtained from the party communication type entity.
   * If the Mailgun client has already been instantiated, it will be returned instead of creating a new instance.
   * To ensure the Mailgun class exists, it is checked using the class_exists function.
   *
   * @return Mailgun|null The instantiated Mailgun client.
   */
  protected function getMailgunClient(): ?Mailgun {
    if (
      !$this->mailgunClient && class_exists('\Mailgun\Mailgun') &&
      ($type = variable_get('party_communication_email_system_type', FALSE)) &&
      ($type = entity_load_single('party_communication_type', $type)) &&
      !empty($type->data['mailgun']['api_key'])
    ) {
      $this->mailgunClient = Mailgun::create($type->data['mailgun']['api_key']);
    }

    return $this->mailgunClient;
  }

  /**
   * Validates the given email addresses.
   *
   * @param array $addresses
   *   An array of email addresses to validate.
   *
   * @return array
   *   An array of validation results for each email address. The array has the following structure:
   *   - [email_address_1_key] =>
   *     - email: The email address being validated.
   *     - validation_result: The result of the email address validation. Possible values are:
   *       - 0: deliverable
   *       - 1: undeliverable
   *       - 2: do_not_send
   *       - 3: catch_all
   *       - 4: unknown
   *     - allow_send: Indicates whether the email address is allowed to receive email. (TRUE or FALSE)
   *     - risk: The risk level associated with the email address. Possible values are:
   *       - 0: unknown
   *       - 1: low
   *       - 2: medium
   *       - 3: high
   *     - did_you_mean: The suggested alternate email address, if any.
   *     - reasons: A pipe-separated string of reasons for the validation result.
   */
  public function validateAddresses($addresses = []) {
    $key_map = array_flip($addresses);

    // First check the static cache.
    $results = $remaining_addresses = [];
    $scache = &drupal_static(__CLASS__.__METHOD__, []);
    $static_cached = array_intersect_key($scache, $key_map);

    foreach ($key_map as $address => $key) {
      if ($static_cached[$address]) {
        $results[$key] = $static_cached[$address];
      }
      else {
        $remaining_addresses[$address] = $address;
      }
    }

    if (empty($remaining_addresses)) {
      return $results;
    }

    $query_results = db_select('party_communication_email_validation', 'v')
      ->fields('v')
      ->condition('email', array_values($remaining_addresses))
      ->execute()
      ->fetchAllAssoc('email', PDO::FETCH_ASSOC);
    foreach ($query_results as $address => $result) {
      $static_cached[$address] = $results[$key_map[$address]] = [
        'validation_result' => (int) $result['validation_result'],
        'allow_send' => (bool) $result['allow_send'],
        'risk' => (int) $result['risk'],
      ] + $result;
      unset($remaining_addresses[$address]);
    }

    if (empty($remaining_addresses)) {
      return $results;
    }

    $to_insert = [];
    foreach ($remaining_addresses as $k => $address) {
      if (!filter_var($address, FILTER_VALIDATE_EMAIL)) {
        $results[$key_map[$address]] = $scache[$address] = [
          'email' => $address,
          'validation_result' => PartyCommunicationEmailValidator::UNDELIVERABLE,
          'allow_send' => FALSE,
          'risk' => PartyCommunicationEmailValidator::RISK_UNKNOWN,
          'did_you_mean' => NULL,
          'reasons' => 'invalid_content',
        ];
        unset($remaining_addresses[$k]);
      }
    }

    if ($mg = $this->getMailgunClient()) {
      $mg_result_map = [
        'deliverable' => static::DELIVERABLE,
        'undeliverable' => static::UNDELIVERABLE,
        'do_not_send' => static::DO_NOT_SEND,
        'catch_all' => static::CATCH_ALL,
        'unknown' => static::UNKNOWN,
      ];
      $mg_risk_map = [
        'unknown' => static::RISK_UNKNOWN,
        'low' => static::RISK_LOW,
        'medium' => static::RISK_MEDIUM,
        'high' => static::RISK_HIGH,
      ];

      foreach ($remaining_addresses as $address) {
        try {
          $result = $mg->emailValidationV4()->validate($address);

          $scache[$address] = $results[$key_map[$address]] = $to_insert[] = [
            'email' => $address,
            'validation_result' => $mg_result_map[$result->getResult()],
            'allow_send' => !in_array($mg_result_map[$result->getResult()], [1,2]),
            'risk' => $mg_risk_map[$result->getRisk()],
            'did_you_mean' => $result->getDidYouMean(),
            'reasons' => implode('|', $result->getReason()),
          ];
          unset($remaining_addresses[$address]);
        }
        catch (ClientExceptionInterface|HttpClientException $e) {
          continue;
        }
      }
    }

    if (!empty($to_insert)) {
      $q = db_insert('party_communication_email_validation')
        ->fields(['email', 'validation_result', 'allow_send', 'risk', 'did_you_mean', 'reasons']);
      foreach ($to_insert as $row) {
        $row['allow_send'] = (int) $row['allow_send'];
        $q->values($row);
      }

      try {
        $q->execute();
      }
      catch (Exception $e) {
        // Do nothing. This has probably happened as two processes validate simultaneously and in this case its not a
        // big deal if we lose a row.
      }
    }

    if (!empty($remaining_addresses)) {
      foreach ($remaining_addresses as $address) {
        $results[$key_map[$address]] = [
          'email' => $address,
          'validation_result' => static::UNKNOWN,
          'allow_send' => TRUE,
          'risk' => static::RISK_UNKNOWN,
          'did_you_mean' => NULL,
          'reasons' => NULL,
        ];
      }
    }

    return $results;
  }

  /**
   * Validates the given address.
   *
   * @param string $address
   *   The address to be validated.
   *
   * @return array
   *   The validation result of the address.
   *
   * @see static::validateAddresses()
   */
  public function validateAddress($address) {
    return $this->validateAddresses([$address])[0];
  }
}
