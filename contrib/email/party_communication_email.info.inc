<?php

/**
 * Implements hook_entity_property_info_alter().
 */
function party_communication_email_entity_property_info_alter(&$info) {
  $types = db_select('party_communication_type', 't')
    ->fields('t', ['name'])
    ->condition('mode', 'email')
    ->execute()->fetchCol();

  foreach ($types as $type) {
    $info['party_communication']['bundles'][$type]['properties']['communication_sender']['getter callback'] = 'party_communication_property_participation_party_get';
    $info['party_communication']['bundles'][$type]['properties']['communication_sender']['role'] = 'sender';

    $info['party_communication']['bundles'][$type]['properties']['communication_recipients']['getter callback'] = 'party_communication_property_participation_party_get';
    $info['party_communication']['bundles'][$type]['properties']['communication_recipients']['role'] = 'to';

    $info['party_communication']['bundles'][$type]['properties']['communication_email_sender']['getter callback'] = 'party_communication_email_property_participation_info_get';
    $info['party_communication']['bundles'][$type]['properties']['communication_email_sender']['role'] = 'sender';
    $info['party_communication']['bundles'][$type]['properties']['communication_email_sender']['info'] = 'contact_data';

    $info['party_communication']['bundles'][$type]['properties']['communication_email_sender_name']['getter callback'] = 'party_communication_email_property_participation_info_get';
    $info['party_communication']['bundles'][$type]['properties']['communication_email_sender_name']['role'] = 'sender';
    $info['party_communication']['bundles'][$type]['properties']['communication_email_sender_name']['info'] = 'contact_name';

    $info['party_communication']['bundles'][$type]['properties']['communication_email_to']['getter callback'] = 'party_communication_email_property_participation_info_get';
    $info['party_communication']['bundles'][$type]['properties']['communication_email_to']['role'] = 'to';
    $info['party_communication']['bundles'][$type]['properties']['communication_email_to']['info'] = 'contact_data';

    $info['party_communication']['bundles'][$type]['properties']['communication_email_cc']['getter callback'] = 'party_communication_email_property_participation_info_get';
    $info['party_communication']['bundles'][$type]['properties']['communication_email_cc']['role'] = 'cc';
    $info['party_communication']['bundles'][$type]['properties']['communication_email_cc']['info'] = 'contact_data';

    $info['party_communication']['bundles'][$type]['properties']['communication_email_bcc']['getter callback'] = 'party_communication_email_property_participation_info_get';
    $info['party_communication']['bundles'][$type]['properties']['communication_email_bcc']['role'] = 'bcc';
    $info['party_communication']['bundles'][$type]['properties']['communication_email_bcc']['info'] = 'contact_data';
  }
}
