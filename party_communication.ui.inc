<?php
/**
 * @file
 *   Extra UI Elements for Party Communications.
 */

/**
 * Form that only shows the special content fields.
 */
function party_communication_long_text_content_form($form, &$form_state, $communication) {
  form_load_include($form_state, 'inc', 'party_communication', 'party_communication.ui');
  form_load_include($form_state, 'inc', 'party_communication', 'party_communication.admin');

  $form['#prefix'] = '<div id="communication-'.$communication->id.'-content-form">';
  $form['#suffix'] = '</div>';
  $form['#attributes']['class'][] = 'party-communication-long-text-content-form';

  $form_state['party_communication'] = $communication;

  field_attach_form('party_communication', $communication, $form, $form_state);

  // Limit to fields that are long text fields.
  $controller = $communication->getType()->getController();
  if ($controller instanceof PartyCommunicationModeLongText) {
    $fields = $controller->communicationContentFields();
    foreach (element_children($form) as $key) {
      if (empty($fields[$key])) {
        $form[$key]['#access'] = FALSE;
      }
    }
  }

  $form['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();
  $validate = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
    $validate += $form['#validate'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => $submit + array('party_communication_form_submit'),
    '#validate' => $validate + array('party_communication_form_validate'),
  );
  $form['actions']['submit_and_edit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and Keep Editing'),
    '#submit' => $submit + array('party_communication_form_submit', 'party_communication_form_submit_rebuild'),
    '#validate' => $validate + array('party_communication_form_validate'),
    '#ajax' => array(
      'wrapper' => 'communication-'.$communication->id.'-content-form',
      'callback' => 'party_communication_long_text_content_form_ajax_submit',
    ),
  );

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'party_communication_form_validate';

  return $form;
}

/**
 * Submit callback that sets rebuild to true.
 */
function party_communication_form_submit_rebuild($form, &$form_state) {
  $form_state['build_info']['args'][0] = $form_state['party_communication'];

  // Make sure all values are refreshed.
  $form_state['input'] = array();
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax submit.
 */
function party_communication_long_text_content_form_ajax_submit($form, $form_state) {
  return $form;
}

/**
 * Build a thread view of communication.
 *
 * @param \PartyCommunication $communication
 * @param array $options
 *   An array option for displays. Options include:
 *   - include_replies: Boolean. True if replies to this communication should be
 *     included.
 */
function party_communication_thread_build($communication, $options = array()) {
  // Default options.
  $options += array(
    'include_replies' => TRUE,
    'max_replies' => 10,
    'max_ancestors' => 10,
  );

  $initial_communication = $communication;
  $communications = array($initial_communication);
  $num_ancestors = 0;
  while ($parent = $communication->wrapper()->communication_in_reply_to->value()) {
    $num_ancestors++;
    $communication = $parent;
    $communications[] = $communication;

    if ($num_ancestors == $options['max_ancestors']) {
      break;
    }
  }

  if ($options['include_replies']) {
    $num_replies = 0;
    $level_comm_ids = array($initial_communication->id);
    $last_level_comm_ids = array($initial_communication->id);
    $replies = array();
    while (!empty($level_comm_ids)) {
      $query = db_select('field_data_communication_in_reply_to', 'irt');
      $query->condition('irt.communication_in_reply_to_target_id', $level_comm_ids);
      $query->addField('irt', 'entity_id', 'id');
      $level_comm_ids = $query->execute()->fetchCol();

      if ($num_replies < $options['max_replies']) {
        $replies += entity_load('party_communication', $level_comm_ids);
        $last_level_comm_ids = $level_comm_ids;
      }
      $num_replies += count($level_comm_ids);
    }

    $num_shown_replies = count($replies);
    usort($replies, function($a, $b) {
      if ($a->timestamp < $b->timestamp) {
        return -1;
      }
      else if ($a->timestamp > $b->timestamp) {
        return 1;
      }

      return 0;
    });

    foreach ($replies as $reply) {
      array_unshift($communications, $reply);
    }
  }

  // Loop over communications and add them to the build array.
  $build = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('communication-thread-wrapper'),
    ),
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'party_communication').'/js/party_communication.ui.js',
      ),
      'css' => array(
        drupal_get_path('module', 'party_communication').'/css/party_communication.thread.css',
      ),
    ),
    'thread' => array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('communication-thread'),
      ),
    ),
  );
  if ($options['include_replies'] && ($num_replies != $num_shown_replies)) {
    $build['more_replies'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('communication-thread-more-replies'),
      ),
      '#weight' => -1,
      'content' => array(
        '#theme' => 'link',
        '#path' => 'party_communication/ajax/more_replies/'.$options['max_replies'].'/'.implode(',', $last_level_comm_ids),
        '#text' => t('Show :num more replies', array(
          ':num' => min($num_replies - $num_shown_replies, $options['max_replies']),
        )),
        '#options' => array(
          'html' => TRUE,
          'attributes' => array(
            'class' => array('use-ajax'),
          ),
        ),
      ),
    );
  }
  foreach ($communications as $communication) {
    $access = entity_access('view', 'party_communication', $communication);

    /** @var \PartyCommunication $communication */
    // @todo: Build the thread for communications
    $comm_build = array(
      '#type' => 'container',
      '#attributes' => array(
        'data-id' => $communication->id,
        'class' => array(
          'communication-thread-communication',
          'communication-thread-'.$communication->type,
          'communication-thread-'.($communication->id == $initial_communication->id ? 'expanded' : 'collapsed'),
          'communication-thread-access-'.($access ? 'allowed' : 'denied'),
        ),
      ),
      'header' => array(
        '#type' => 'container',
        '#attributes'=> array(
          'class' => array('communication-header'),
        ),
        'sender' => array(
          '#theme' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => array(
            'class' => array('sender'),
          ),
          '#value' => $access ? $communication->wrapper()->communication_sender->value()->label : t('Sender Redacted'),
        ),
        'date' => array(
          '#theme' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => array(
            'class' => array('date'),
          ),
          '#value' => DateTime::createFromFormat('U', $communication->timestamp)->format('j M Y, H:i'),
        ),
        'subject' => array(
          '#theme' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => array(
            'class' => array('subject'),
          ),
          '#value' => $access ? $communication->label : t('Subject Redacted'),
        ),
      ),
      'body' => array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array('communication-body'),
        ),
        'content' => $access ? $communication->view('thread') : array('#markup' => ''),
      ),
    );

    $build['thread'][$communication->id] = $comm_build;
  }

  return $build;
}

/**
 * Ajax page callback for returning more replies to a set of communications.
 */
function party_communication_thread_more_replies_callback($max_replies, $last_level) {
  $level_comm_ids = explode(',', $last_level);
  $commands = array();

  $num_replies = 0;
  $communications = array();
  $last_level_comm_ids = $level_comm_ids;
  $replies = array();
  while (!empty($level_comm_ids)) {
    $query = db_select('field_data_communication_in_reply_to', 'irt');
    $query->condition('irt.communication_in_reply_to_target_id', $level_comm_ids);
    $query->addField('irt', 'entity_id', 'id');
    $level_comm_ids = $query->execute()->fetchCol();

    if ($num_replies < $max_replies) {
      $replies += entity_load('party_communication', $level_comm_ids);
      $last_level_comm_ids = $level_comm_ids;
    }
    $num_replies += count($level_comm_ids);
  }

  $num_shown_replies = count($replies);
  usort($replies, function($a, $b) {
    if ($a->timestamp < $b->timestamp) {
      return -1;
    }
    else if ($a->timestamp > $b->timestamp) {
      return 1;
    }

    return 0;
  });

  foreach ($replies as $reply) {
    array_unshift($communications, $reply);
  }

  // Loop over communications and add them to the build array.
  $build = array(
    'thread' => array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('communication-thread'),
      ),
    ),
  );
  if (($num_replies != $num_shown_replies)) {
    $build['more_replies'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('communication-thread-more-replies'),
      ),
      '#weight' => -1,
      'content' => array(
        '#theme' => 'link',
        '#path' => 'party_communication/ajax/more_replies/'.$max_replies.'/'.implode(',', $last_level_comm_ids),
        '#text' => t('Show :num more replies', array(
          ':num' => min($num_replies - $num_shown_replies, $max_replies),
        )),
        '#options' => array(
          'html' => TRUE,
          'attributes' => array(
            'class' => array('use-ajax'),
          ),
        ),
      ),
    );
  }
  foreach ($communications as $communication) {
    $access = entity_access('view', 'party_communication', $communication);

    /** @var \PartyCommunication $communication */
    // @todo: Build the thread for communications
    $comm_build = array(
      '#type' => 'container',
      '#attributes' => array(
        'data-id' => $communication->id,
        'class' => array(
          'communication-thread-communication',
          'communication-thread-'.$communication->type,
          'communication-thread-access-'.($access ? 'allowed' : 'denied'),
          'communication-thread-collapsed',
        ),
      ),
      'header' => array(
        '#type' => 'container',
        '#attributes'=> array(
          'class' => array('communication-header'),
        ),
        'sender' => array(
          '#theme' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => array(
            'class' => array('sender'),
          ),
          '#value' => $access ? $communication->wrapper()->communication_sender->value()->label : t('Sender Redacted'),
        ),
        'date' => array(
          '#theme' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => array(
            'class' => array('date'),
          ),
          '#value' => DateTime::createFromFormat('U', $communication->timestamp)->format('j M Y, H:i'),
        ),
        'subject' => array(
          '#theme' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => array(
            'class' => array('subject'),
          ),
          '#value' => $access ? $communication->label : t('Subject Redacted'),
        ),
      ),
      'body' => array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array('communication-body'),
        ),
        'content' => $access ? $communication->view('thread') : array('#markup' => ''),
      ),
    );

    $build['thread'][$communication->id] = $comm_build;
  }
  $commands[] = ajax_command_replace('.communication-thread-more-replies', drupal_render($build));

  return array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );
}

/**
 * Page callback to add a participation of a particular role.
 */
function party_communication_ui_add_participation($js, $communication, $role) {
  $defaults = [];
  if (isset($_GET['party_id'])) {
    $defaults['party_id'] = $_GET['party_id'];

    if (isset($_GET['source'])) {
      $defaults['source'] = $_GET['source'];
    }
  }

  if ($js) {
    // This is a modal request, so do the magic.
    ctools_include('ajax');
    ctools_include('modal');

    $form_state = [
      'title' => t('Add Participation'),
      'ajax' => TRUE,
      'modal' => TRUE,
      'build_info' => [
        'args' => [$communication, $role, $defaults],
      ],
    ];
    $commands = ctools_modal_form_wrapper('party_communication_add_participation_form', $form_state);

    if (!empty($form_state['executed']) && empty($form_state['rebuild'])) {
      // The form has been executed, so let's redirect to the destination page.
      $commands = $form_state['commands'] ?? [];
      $commands[] = ctools_modal_command_dismiss();
    }

    ajax_deliver(array(
      '#type' => 'ajax',
      '#commands' => $commands,
    ));
    drupal_exit();
  }
  else {
    return drupal_get_form('party_communication_add_participation_form', $communication, $role, $defaults);
  }
}

/**
 * Form to add a participation to a communication.
 *
 * @param $form
 * @param $form_state
 * @param \PartyCommunication $communication
 * @param string $role
 */
function party_communication_add_participation_form($form, &$form_state, $communication, $role, $defaults = []) {
  $form_state['communication'] = $communication;
  if (empty($form_state['participation'])) {
    if (isset($defaults['source']) && isset($defaults['party_id'])) {
      [$primitive, $key] = explode(':', $defaults['source'], 2);
      $contact_data = PartyCommunicationContact::getData(party_load($defaults['party_id']), $primitive, [$key]);
    }

    $form_state['participation'] = PartyCommunicationParticipation::create(
      $communication,
      $role,
      uniqid($role . '_'),
      $contact_data ?? NULL,
      NULL,
      $defaults['party_id'] ?? NULL,
    );
  }

  $form = $form_state['participation']->buildForm($form, $form_state);

  $form['actions'] = [
    '#type' => 'actions',
    'submit' => [
      '#type' => 'submit',
      '#value' => t('Add @role', ['@role' => $communication->getType()->getController()->getParticipationRoles()[$role]['label']]),
      '#validate' => [
        'party_communication_add_participation_form_validate',
      ],
      '#submit' => [
        'party_communication_add_participation_form_submit',
      ],
      '#ajax' => [
        'callback' => 'party_communication_add_participation_form_submit_ajax',
      ]
    ],
  ];

  return $form;
}

/**
 * Validate the participation add form.
 *
 * @param $form
 * @param $form_state
 *
 * @return void
 */
function party_communication_add_participation_form_validate($form, &$form_state) {
  $form_state['participation']->validateForm($form, $form_state);
}

/**
 * Submit the participation add form.
 *
 * @param $form
 * @param $form_state
 *
 * @return void
 */
function party_communication_add_participation_form_submit($form, &$form_state) {
  $form_state['participation']->submitForm($form, $form_state);

  $form_state['communication']->addParticipation($form_state['participation']);
  /** @var \PartyCommunicationController $controller */
  $controller = entity_get_controller('party_communication');
  $controller->saveParticipations($form_state['communication'], [$form_state['participation']]);
}

/**
 * @param $form
 * @param $form_state
 *
 * @return array|mixed
 */
function party_communication_add_participation_form_submit_ajax($form, &$form_state) {
  if (empty($form_state['executed']) || !empty($form_state['rebuild'])) {
    return $form;
  }

  ctools_include('ajax');
  ctools_include('modal');
  return [
    '#type' => 'ajax',
    '#commands' => [
      ctools_modal_command_dismiss(),
    ],
  ];
}
