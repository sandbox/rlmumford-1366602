(function($) {
  Drupal.behaviors.party_communication_ui = {
    attach: function(context, settings) {
      $('.communication-thread-communication', context).click(function() {
        $(this).toggleClass('communication-thread-expanded').toggleClass('communication-thread-collapsed');
      });
    }
  };
})(jQuery);
