<?php

/**
 * Views data controller for party communications.
 */
class PartyCommunicationViewsController extends EntityDefaultViewsController {

  public function views_data() {
    $data = parent::views_data();

    $data['party_communication_participation'] = [
      'table' => [
        'group' => t('Communication'),
        'base' => [
          'field' => 'participation_id',
          'title' => t('Participations'),
          'weight' => 11,
          'defaults' => [
            'field' => 'participation_id',
          ],
        ],
        'join' => [
          'party_communication' => [
            'left_field' => 'vid',
            'field' => 'vid',
          ],
          'party_communication_revision' => [
            'left_field' => 'vid',
            'field' => 'vid',
          ],
        ],
      ],
      'id' => [
        'title' => t('Communication'),
        'help' => t('The communication this relates to.'),
        'relationship' => [
          'title' => 'Communication',
          'handler' => 'views_handler_relationship',
          'base' => 'party_communication',
          'field' => 'id',
        ],
        'filter' => [
          'handler' => 'ck_opencrm_views_filter_handler_entity_id_autocomplete',
          'entity_type' => 'party_communication',
        ],
        'argument' => [
          'handler' => 'views_handler_argument_numeric',
        ],
        'field' => [
          'handler' => 'ck_opencrm_views_handler_field_entity_label',
          'entity_type' => 'party_communication',
        ],
      ],
      'participation_id' => [
        'title' => t('Participation Id'),
        'description' => t('Participation Id'),
        'field' => [
          'handler' => 'views_handler_field',
        ],
        'filter' => [
          'handler' => 'views_handler_filter_string',
        ],
        'argument' => [
          'handler' => 'views_handler_argument_string'
        ],
      ],
      'participation_role' => [
        'title' => t('Participation Role'),
        'description' => t('Participation Role'),
        'field' => [
          'handler' => 'views_handler_field',
        ],
        'filter' => [
          'handler' => 'views_handler_filter_string',
        ],
        'argument' => [
          'handler' => 'views_handler_argument_string',
        ],
      ],
      'party' => [
        'title' => t('Party'),
        'help' => t('The contact involved in the communication.'),
        'relationship' => [
          'title' => 'Party',
          'handler' => 'views_handler_relationship',
          'base' => 'party',
          'field' => 'pid',
        ],
        'filter' => [
          'handler' => 'ck_opencrm_views_filter_handler_entity_id_autocomplete',
          'entity_type' => 'party',
        ],
        'argument' => [
          'handler' => 'views_handler_argument_numeric',
        ],
        'field' => [
          'handler' => 'ck_opencrm_views_handler_field_entity_label',
          'entity_type' => 'party',
        ],
      ],
      'contact_name' => [
        'title' => t('Contact Name'),
        'description' => t('Contact Name'),
        'field' => [
          'handler' => 'views_handler_field',
        ],
        'filter' => [
          'handler' => 'views_handler_filter_string',
        ],
        'argument' => [
          'handler' => 'views_handler_argument_string',
        ],
      ],
      'contact_data' => [
        'title' => t('Contact Data'),
        'description' => t('Contact data'),
        'field' => [
          'handler' => 'party_communication_views_handler_field_participation_contact_data',
        ],
        'filter' => [
          'handler' => 'views_handler_filter_string',
        ],
        'argument' => [
          'handler' => 'views_handler_argument_string',
        ],
      ],
      'participation_status' => [
        'title' => t('Participation Status'),
        'description' => t('Status'),
        'field' => [
          'handler' => 'views_handler_field',
        ],
        'filter' => [
          'handler' => 'views_handler_filter_string',
        ],
        'argument' => [
          'handler' => 'views_handler_argument_string',
        ],
      ]
    ];

    $data['party_communication']['participations'] = [
      'title' =>  t('Participations'),
      'description' => t('Participations'),
      'field' => [
        'handler' => 'party_communication_views_handler_field_participations',
        'real field' => 'id',
      ],
      'relationship' => [
        'handler' => 'party_communication_views_handler_relationship_participations',
        'base' => 'party_communication_participation',
        'field' => 'vid',
      ],
    ];

    return $data;
  }

}
