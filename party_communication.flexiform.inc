<?php
/**
 * @file
 * Flexiform hooks.
 */

/**
 * Implements hook_flexiform_element_info().
 */
function party_communication_flexiform_element_info() {
  $elements = array();

  /** @var \PartyCommunicationType $type */
  foreach (entity_load('party_communication_type') as $type) {
    foreach ($type->getController()->getParticipationRoles() as $role => $role_info) {
      $elements['party_communication'][$type->name]['participation__' . $role] = [
        'label' => t('@role Participation', ['@role' => $role_info['label']]),
        'class' => 'PartyCommunicationParticipationsElement',
        'type' => 'party_communication_participations',
        'group' => 'Participation',
        'participation_role' => $role,
      ];
    }
  }

  return $elements;
}
