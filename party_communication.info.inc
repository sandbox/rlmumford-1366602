<?php

/**
 * @file
 * Entity Metadata Info.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function party_communication_entity_property_info_alter(&$info) {
  $props = &$info['party_communication_type']['properties'];
  $props['mode']['options list'] = 'party_communication_mode_options';

  $props = &$info['party_communication']['properties'];
  $props['timestamp']['type'] = 'date';
  $props['timestamp']['setter callback'] = 'entity_property_verbatim_set';
  $props['created']['type'] = 'date';
  $props['type']['required'] = TRUE;

  $props['label']['setter callback'] = 'entity_property_verbatim_set';

  $props['status']['setter callback'] = 'entity_property_verbatim_set';
  $props['status']['options list'] = 'party_communication_property_status_options';

  $props['inoutbound']['setter callback'] = 'entity_property_verbatim_set';
  $props['inoutbound']['options list'] = 'party_communication_inoutbound_options';
  $props['inoutbound']['required'] = TRUE;

  $props['internal_participant'] = $props['external_participant'] = array(
    'type' => 'party',
    'label' => t('Internal Participant'),
    'description' => t('For inbound communications, the recipient, for outbound communications, the sender.'),
    'getter callback' => 'party_communication_participant_property_get',
    'computed' => TRUE,
    'entity views field' => TRUE,
  );
  $props['external_participant']['label'] = t('External Participant');
  $props['external_participant']['description'] = t('For inbound communications, the sender, for outbound communications, the recipient.');

  $props['creator']['type'] = 'party';
  $props['creator']['setter callback'] = 'entity_property_verbatim_set';
  $props['updater'] = array(
    'type' =>  'party',
    'label' => t('Updater'),
    'schema field' => 'updater',
    'description' => t('Party that updated the communication last.'),
  );

  $props['revision_log'] = array(
    'type' => 'text',
    'label' => t('Revision Log'),
    'schema field' => 'revision_log',
    'description' => t('Log message describing revision.'),
  );
  $props['revision_timestamp'] = array(
    'type' => 'date',
    'label' => t('Revision Date'),
    'schema field' => 'revision_timestamp',
    'description' => t('The date this revision was created.'),
  );

  $props['event_timeline'] = [
    'type' => 'structure',
    'label' => t('Event Timeline'),
    'property info' => [
      'events' => [
        'type' => 'list<party_communication_event>',
        'label' => t('Events'),
        'property info' => [
          'uuid' => [
            'label' => t('UUID'),
            'type' => 'text',
            'getter callback' => 'party_communication_event_timeline_event_property_get',
          ],
          'communication' => [
            'label' => t('Communication'),
            'type' => 'party_communication',
            'getter callback' => 'party_communication_event_timeline_event_property_get',
          ],
          'participation' => [
            'label' => t('Participation'),
            'type' => 'party_communication_participation',
            'property info' => PartyCommunicationParticipation::structurePropertyInfo(),
            'getter callback' => 'party_communication_event_timeline_event_property_get',
          ],
          'datetime' => [
            'label' => t('Date/Time'),
            'type' => 'date',
            'getter callback' => 'party_communication_event_timeline_event_property_get',
          ],
          'event' => [
            'label' => t('Event'),
            'type' => 'token',
            'getter callback' => 'party_communication_event_timeline_event_property_get',
          ],
          'label' => [
            'label' => t('Label'),
            'type' => 'text',
            'getter callback' => 'party_communication_event_timeline_event_property_get',
          ],
          'rendered_plain' => [
            'label' => t('Rendered (Plain Text)'),
            'type' => 'text',
            'getter callback' => 'party_communication_event_timeline_event_property_get',
          ],
          'rendered_inline' => [
            'label' => t('Rendered (Inline HTML)'),
            'type' => 'text',
            'getter callback' => 'party_communication_event_timeline_event_property_get',
          ],
          'rendered' => [
            'label' => t('Rendered (Full HTML)'),
            'type' => 'text',
            'getter callback' => 'party_communication_event_timeline_event_property_get',
          ],
        ],
        'getter callback' => 'party_communication_event_timeline_property_get',
      ],
      'rendered_plain' => [
        'label' => t('Rendered (Plain Text)'),
        'type' => 'text',
        'getter callback' => 'party_communication_event_timeline_property_get',
      ],
      'rendered_inline' => [
        'label' => t('Rendered (Inline HTML)'),
        'type' => 'text',
        'getter callback' => 'party_communication_event_timeline_property_get',
      ],
      'rendered' => [
        'label' => t('Rendered (Full HTML)'),
        'type' => 'text',
        'getter callback' => 'party_communication_event_timeline_property_get',
      ],
    ],
    'getter callback' => 'party_communication_event_timeline_property_get',
  ];

  /** @var \PartyCommunicationType $type */
  foreach (entity_load('party_communication_type') as $type) {
    $props = &$info['party_communication']['bundles'][$type->name]['properties'];

    foreach ($type->getController()->getParticipationRoles() as $role => $participation_info) {
      $props[$role] = [
        'type' => $participation_info['cardinality'] === 1 ? 'party_communication_participation' : 'list<party_communication_participation>',
        'label' => $participation_info['label'],
        'description' => $participation_info['description'] ?? '',
        'property info' => [$participation_info['class'] ?? PartyCommunicationParticipation::class, 'structurePropertyInfo'](),
        'getter callback' => 'party_communication_participation_property_get',
      ];
    }
  }
}
