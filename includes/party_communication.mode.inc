<?php

/**
 * @file
 *   Contains default mode class.
 */

/**
 * Interface for modes.
 */
interface PartyCommunicationModeInterface {

  /**
   * Get the participation roles associated with this mode.
   *
   * @return array
   *   An array of roles, each role contains the following:
   *     - label
   *     - supported_primitives
   *     - required
   *     - cardinality
   */
  public function getParticipationRoles() : array;

  /**
   * Perform some logic when a communication type is made for this mode.
   */
  public function createType($bundle);

  /**
   * Validate whether a communication is ready to send.
   *
   * @param $communication
   *   The communication entity.
   * @param array $reasons (options)
   *   An array of reasons passed bu reference.
   *
   * @return boolean
   *   True if the communication can be sent.
   */
  public function validateSend($communication, $settings = array(), &$reasons = array());

  /**
   * Send the communication.
   *
   * @param \PartyCommunication $communication
   *   The communication to send.
   * @param array $settings
   *   Settings for sending the communication.
   */
  public function send($communication, $settings = array());

  /**
   * Get a settings form for this mode.
   *
   * @return $form
   *   A fieldset containing the settings.
   */
  public function settingsForm($form, &$form_state);

  /**
   * Validate the settings form for this mode.
   */
  public function settingsFormValidate($form, &$form_state);

  /**
   * Submit the settings form for this mode.
   */
  public function settingsFormSubmit($form, &$form_state);

  /**
   * Do an operation on a communication.
   *
   * @param $op string
   *   The name of the operation.
   * @param $communication PartyCommunication
   *   The communication to do the operation on.
   */
  public function doOperation($op, $communication);

  /**
   * Validate whether an operation can be done to a communication.
   *
   * @param $op string
   *   The name of the operation.
   * @param $communication PartyCommunication
   *   The communication to do the operation on.
   */
  public function validateOperation($op, $communication);

  /**
   * Get the settings for a given operation.
   *
   * @param string $op
   *   The operation to get the settings for.
   * @param PartyCommunication $communication
   *   The communication entity.
   *
   * @return array
   *   An array of settings for the operation containing:
   *   - op: The name of the operation.
   *   - Any settings defined under the 'settings' key of the op definition.
   *   - Any settings returned from the 'settingsCallback'.
   *   The settings are added to the array in the above order, so settings
   *   returned from the callback will override settings from the op
   *   definition that have the same key.
   */
  public function settingsOperation($op, $communication);

  /**
   * Get the list of operations available.
   *
   * @return array
   *   And array of operations keyed by a machine name. Each item should have
   *   the following keys:
   *     - (callable) 'do': A callback to perform the operation. See send.
   *     - (callable) 'validate': A callback to validate whether the operation
   *       can be performed. See 'validateSend'.
   *     - (string) 'label': A human readable label for the operation.
   */
  public function supportedOperations();

  /**
   * Get the communication label label.
   *
   * Return the label element title for the communication form.
   */
  public function labelElementTitle($communication);

  /**
   * Do things before a communication is saved.
   *
   * @param \PartyCommunication $communication
   *
   * @return vaid
   */
  public function onCommunicationPreSave($communication);

  /**
   * Do things after a communication is saved.
   *
   * @param \PartyCommunication $communication
   * @param bool $update
   *
   * @return void
   */
  public function onCommunicationPostSave($communication, bool $update);

}

/**
 * Default Mode Class.
 */
abstract class PartyCommunicationModeDefault implements PartyCommunicationModeInterface {

  /**
   * Mode Configuration.
   *
   * @var array
   */
  protected $config = array();

  /**
   * Construct a new Mode Handler.
   */
  public function __construct($config = array()) {
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public abstract function getParticipationRoles() : array;

  /**
   * {@inheritdoc}
   */
  public function createType($bundle) {
    // Sender Field.
    if (!field_info_field('communication_sender')) {
      $field = array(
        'field_name' => 'communication_sender',
        'type' => 'entityreference',
        'cardinality' => 1,
        'settings' => array(
          'target_type' => 'party',
          'handler_settings' => array(
            'behaviors' => array(
              'views-autocomplete' => array(
                'status' => 1,
              ),
            ),
          ),
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_sender', $bundle)) {
      $instance = array(
        'field_name' => 'communication_sender',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Sender'),
      );
      field_create_instance($instance);
    }

    // Recipients Field
    if (!field_info_field('communication_recipients')) {
      $field = array(
        'field_name' => 'communication_recipients',
        'type' => 'entityreference',
        'cardinality' => FIELD_CARDINALITY_UNLIMITED,
        'settings' => array(
          'target_type' => 'party',
          'handler_settings' => array(
            'behaviors' => array(
              'views-autocomplete' => array(
                'status' => 1,
              ),
            ),
          ),
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_recipients', $bundle)) {
      $instance = array(
        'field_name' => 'communication_recipients',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Recipients'),
      );
      field_create_instance($instance);
    }
  }

  /**
   * {inheritdoc}
   */
  public function validateSend($communication, $settings = array(), &$reasons = array()) {
    if (empty($communication->communication_recipients)) {
      $reasons[] = t('No Recipients');
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Prepare the communication for sending.
   */
  protected function prepare($communication) {
    module_invoke_all('party_communication_prepare', $communication);
  }

  /**
   * {@inheritdoc}
   */
  public abstract function send($communication, $settings = array());

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, &$form_state) {
    $form['message']['#markup'] = t('This mode has no settings.');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormValidate($form, &$form_state) {}

  /**
   * {@inheritdoc}
   */
  public function settingsFormSubmit($form, &$form_state) {}

  /**
   * Confirm Form.
   */
  public function confirmForm($form, $form_state, $communication, $settings = array()) {
    $ops = $communication->getType()->getController()->supportedOperations();
    $form['message'] = array(
      '#markup' =>  t('Are you sure you wish to !op @label?', array(
          '!op' => $ops[$form_state['op']]['label'],
          '@label' => $communication->label,
        )),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function doOperation($op, $communication, $options = array()) {
    $ops = $this->supportedOperations();

    if (empty($ops[$op]) || empty($ops[$op]['do']) || !is_callable($ops[$op]['do'])) {
      throw new Exception(t('Cannot perform operation %op on communication %comm: Invalid operation.', array(
        '%op' => $op,
        '%comm' => $communication->label,
      )));
    }

    // Get the operation settings.
    // Make sure options passed in override settings from callback.
    $settings = $options + $this->settingsOperation($op, $communication);

    module_invoke_all('party_communication_pre_operation', $op, $communication, $settings);
    call_user_func($ops[$op]['do'], $communication, $settings);
    module_invoke_all('party_communication_do_operation', $op, $communication, $settings);
    module_invoke_all('party_communication_post_operation', $op, $communication, $settings);
  }

  /**
   * {@inheritdoc}
   */
  public function validateOperation($op, $communication, array $options = [], &$reasons = []) {
    $ops = $this->supportedOperations();

    if (empty($ops[$op])) {
      $reasons[] = t("Operation %op does not exist.", array('%op' => $op));
      return FALSE;
    }

    if (empty($ops[$op]['do']) || !is_callable($ops[$op]['do'])) {
      $reasons[] = t("Invalid do callback for %op.", array('%op' => $op['label']));
    }

    if (!empty($ops[$op]['validate']) && is_callable($ops[$op]['validate'])) {
      call_user_func_array($ops[$op]['validate'], array($communication, $options + $this->settingsOperation($op, $communication), &$reasons));
    }

    return empty($reasons);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsOperation($op, $communication) {
    $ops = $this->supportedOperations();

    $settings = array();
    $settings['op'] = $op;
    if (!empty($ops[$op]['settings']) && is_array($ops[$op]['settings'])) {
      $settings = $ops[$op]['settings'] + $settings;
    }
    if (!empty($ops[$op]['settingsCallback']) && is_callable($ops[$op]['settingsCallback'])) {
      $settings = call_user_func($ops[$op]['settingsCallback'], $communication, $settings) + $settings;
    }

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function supportedOperations() {
    $ops = array(
      'send' => array(
        'label' => t('Send'),
        'log_string' => 'Sent @communication to @recipient',
        'do' => array($this, 'send'),
        'validate' => array($this, 'validateSend'),
        'form' => array($this, 'confirmForm'),
      ),
    );

    drupal_alter('party_communication_mode_operations', $ops, $this);
    return $ops;
  }

  /**
   * {@inheritdoc}
   */
  public function labelElementTitle($communication) {
    return t('Label');
  }

  /**
   * Prepare the content of a text field.
   *
   * @param string $content
   *   The content to prepare.
   * @param PartyCommunication $communication
   *   The Communication Entity.
   */
  public function prepareContent($content, $communication, $options = array()) {
    global $user;

    ctools_include('context');
    $contexts = array(
      'site' => ctools_context_create('token'),
      'current-user' => ctools_context_create('entity:user', $user),
      'communication' => ctools_context_create('entity:party_communication', $communication),
    );
    $contexts['site']->keyword = 'site';
    $contexts['site']->identifier = t('Global Tokens');
    $contexts['current-user']->keyword = 'current-user';
    $contexts['current-user']->identifier = t('Logged-in user');
    $contexts['communication']->keyword = 'communication';
    $contexts['communication']->identifier = t('Communication');

    return ctools_context_keyword_substitute($content, array(), $contexts, $options);
  }

  /**
   * Validate the password input.
   */
  public function passwordInputValidate($element, &$form_state, $form) {
    $input_parents = $parents = $element['#parents'];
    $input_value = drupal_array_get_nested_value($form_state['values'], $parents);

    $name = array_pop($parents);
    array_push($parents, substr($name, 0, -6));
    if (!empty($input_value)) {
      drupal_array_set_nested_value($form_state['values'], $parents, $input_value);
    }

    drupal_array_set_nested_value($form_state['values'], $input_parents, NULL);
  }

  /**
   * Act when the process type is saved.
   */
  public function postTypeSave($type) {}

  /**
   * {@inheritdoc}
   */
  public function onCommunicationPreSave($communication) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function onCommunicationPostSave($communication, bool $update) {
    // Do nothing.
  }

}

abstract class PartyCommunicationModeLongText extends PartyCommunicationModeDefault {

  /**
   * Return a list of field names that should be treated as content fields.
   *
   * @param string $context
   *   What the fields are getting used for.
   *   Can be 'build', 'preview', 'send'.
   *
   * @return Array
   *   An array of field names.
   */
  public function communicationContentFields($context = 'build') {
    return array(
      'communication_body' => t('Body'),
    );
  }

  /**
   * {@inheritdoc}
   *
   * Add a long text field for each of the field names returned from
   * communicationContentFields.
   */
  public function createType($bundle) {
    parent::createType($bundle);
    if (!field_info_field('communication_content_vis')) {
      $field = array(
        'field_name' => 'communication_content_vis',
        'type' => 'list_text',
        'cardinality' => 1,
        'settings' => array(
          'allowed_values' => array(
            'no-one' => t('No One'),
            'administrator' => t('Administrator Only'),
            'participant' => t('Participant'),
            'unrestricted' => t('Unrestricted'),
          ),
        ),
      );
      field_create_field($field);
    }
    if (!field_info_instance('party_communication', 'communication_content_vis', $bundle)) {
      $instance = array(
        'field_name' => 'communication_content_vis',
        'entity_type' => 'party_communication',
        'bundle' => $bundle,
        'label' => t('Content Visibility'),
        'description' => t('Who can see the content of the communication.'),
        'widget' => array(
          'type' => 'options_select',
        ),
      );
      field_create_instance($instance);
    }
    foreach ($this->communicationContentFields() as $field_name => $label) {
      if (!field_info_field($field_name)) {
        $field = array(
          'field_name' => $field_name,
          'type' => 'text_long',
          'cardinality' => 1,
        );
        field_create_field($field);
      }
      if (!field_info_instance('party_communication', $field_name, $bundle)) {
        $instance = array(
          'field_name' => $field_name,
          'label' => $label,
          'entity_type' => 'party_communication',
          'bundle' => $bundle,
          'settings' => array(
            'text_processing' => 1,
          ),
        );
        field_create_instance($instance);
      }
    }
  }

  /**
   * Render a content field.
   *
   * If this is not a preview we write prepared content back to the field. The
   * sending method or trigger is responsible to saving this.
   *
   * @param PartyCommunication $communication
   *   The communication entity.
   * @param string $field_name
   *   The particular field name to prepare.
   * @param bool $preview (optional)
   *   Whether this a preview or the real thing. Default: FALSE
   *
   * @return string
   *   Prepared content for this field.
   */
  public function renderContentField($communication, $field_name, $preview = FALSE) {
    $item = $communication->{$field_name}[LANGUAGE_NONE][0];
    $content = $this->prepareContent($item['value'], $communication);
    $output = check_markup($content, $item['format'], LANGUAGE_NONE);

    $context = array(
      'communication' => $communication,
      'field_name' => $field_name,
      'preview' => $preview,
    );
    drupal_alter('party_communication_render_content_field', $output, $context);

    if (!$preview) {
      $communication->{$field_name}[LANGUAGE_NONE][0]['value'] = $content;
    }

    return $output;
  }

  /**
   * Validate a content field.
   *
   * If any tokens are necessary and missing then fail validation.
   *
   * @param PartyCommunication $communication
   *   The communication entity.
   * @param string $field_name
   *   The particular field name to validate.
   * @param array $missing_tokens
   *   The tokens that are missing data.
   *
   * @return boolean
   *   Whether the content is valid.
   */
  public function validateContentField($communication, $field_name, &$missing_tokens = array()) {
    global $user;
    $item = $communication->{$field_name}[LANGUAGE_NONE][0];

    ctools_include('context');
    $contexts = array(
      'site' => ctools_context_create('token'),
      'current-user' => ctools_context_create('entity:user', $user),
      'communication' => ctools_context_create('entity:party_communication', $communication),
    );
    $contexts['site']->keyword = 'site';
    $contexts['site']->identifier = t('Global Tokens');
    $contexts['current-user']->keyword = 'current-user';
    $contexts['current-user']->identifier = t('Logged-in user');
    $contexts['communication']->keyword = 'communication';
    $contexts['communication']->identifier = t('Communication');

    $content = ctools_context_keyword_substitute($item['value'], array(), $contexts, array('party_communication_content_validate' => TRUE));
    $output = check_markup($content, $item['format'], LANGUAGE_NONE);

    preg_match_all("/\!MISSING(?P<token>[\w:]+)!/", $output, $matches);
    if (empty($matches['token'])) {
      return TRUE;
    }
    else {
      $missing_tokens += $matches['token'];
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}.
   */
  protected function prepare($communication) {
    parent::prepare($communication);

    // Prepare the content fields. We do this first to avoid overwriting
    // later.
    foreach ($this->communicationContentFields() as $field_name => $label) {
      $this->renderContentField($communication, $field_name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateSend($communication, $settings = array(), &$reasons = array()) {
    global $user;

    $parent_result = parent::validateSend($communication, $settings, $reasons);

    $missing_tokens = array();
    $tokens_valid = TRUE;
    foreach ($this->communicationContentFields() as $field_name => $label) {
      $tokens_valid = $tokens_valid && $this->validateContentField($communication, $field_name, $missing_tokens);
    }

    if (!$tokens_valid) {
      foreach ($missing_tokens as $missing_token) {
        $parts = explode(':', $missing_token);
        $context = array_shift($parts);

        $label = '';
        /* @var EntityStructureWrapper */
        $wrapper = NULL;
        if ($context == 'communication') {
          $label = 'Communication';
          $wrapper = entity_metadata_wrapper('party_communication', $communication);
        }
        else if ($context == 'current-user') {
          $label = 'Current User';
          $wrapper = entity_metadata_wrapper('user', $user);
        }
        else if ($context == 'site') {
          $label = 'Site Information';
          $wrapper = entity_metadata_wrapper('site', FALSE, array(
            'type' => 'site',
            'label' => t('Site Information'),
            'description' => t('Site-wide settings and other global information.'),
            'property info alter' => array('RulesData', 'addSiteMetadata'),
            'property info' => array(),
            'optional' => TRUE,
          ));
        }

        try {
          while (is_callable(array($wrapper, 'getPropertyInfo')) && ($property_info = $wrapper->getPropertyInfo(reset($parts)))) {
            $part = array_shift($parts);
            $wrapper = $wrapper->{$part};

            if ($property_info['label']) {
              $label .= ' > ' . $property_info['label'];
            }
            else if ($wrapper->label()) {
              $label .= ' > ' . $wrapper->label();
            } else {
              $label .= ' > ' . ucfirst($part);
            }
          }
        }
        catch (EntityMetadataWrapperException $e) {}

        if (!empty($parts)) {
          foreach ($parts as $part) {
            $label .= ' > '.ucfirst($part);
          }
        }

        $reasons[] = t('Missing required information: @token', array('@token' => $label));
      }
    }

    return $parent_result && $tokens_valid;
  }
}
