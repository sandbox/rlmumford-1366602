<?php

class PartyCommunicationEvent {

  /**
   * Render formats.
   *
   * @todo: Consider moving rendering to a difference service.
   */
  public const FORMAT_PLAIN = 'plain';
  public const FORMAT_INLINE = 'inline';
  public const FORMAT_BLOCK = 'block';

  protected \PartyCommunication $communication;

  protected ?string $participationId = NULL;

  protected \DateTime $dateTime;

  protected string $eventType;

  protected string $label;

  protected array $data = [];

  protected ?PartyCommunicationParticipation $_participation;

  protected ?string $uuid;

  public static function create(\PartyCommunication $communication, string $eventType, string $label, array $data = [], $participationId = NULL, ?\DateTime $dateTime = NULL, ?string $uuid = NULL) : PartyCommunicationEvent {
    $class = PartyCommunicationEvent::class;
    if (class_exists("PartyCommunicationEvent_" . $eventType)) {
      $class = "PartyCommunicationEvent_" . $eventType;
    }

    return new $class($communication, $eventType, $label, $data, $participationId, $dateTime);
  }

  public function __construct(\PartyCommunication $communication, string $eventType, string $label, array $data = [], $participationId = NULL, ?\DateTime $dateTime = NULL, ?string $uuid = NULL) {
    $this->communication = $communication;
    $this->participationId = $participationId;
    $this->dateTime = $dateTime ?? new \DateTime();
    $this->eventType = $eventType;
    $this->label = $label;
    $this->data = $data;
    $this->uuid = $uuid;
  }

  public function label() : string {
    return $this->label;
  }

  public function communication() : PartyCommunication {
    return $this->communication;
  }

  public function participationId() : ?string {
    return $this->participationId;
  }

  public function participation() : ?PartyCommunicationParticipation {
    if (!isset($this->_participation)) {
      $this->_participation = $this->participationId ? $this->communication->getParticipation($this->participationId) : NULL;
    }

    return $this->_participation;
  }

  public function eventType() : string {
    return $this->eventType;
  }

  public function dateTime() : \DateTime {
    return $this->dateTime;
  }

  public function &data() : array {
    return $this->data;
  }

  public function uuid(): ?string {
    return $this->uuid;
  }

  /**
   * Render the participation.
   *
   * @param $format
   * @param $options
   *
   * @return array
   */
  public function render($format = self::FORMAT_INLINE, $options = []) : array {
    return $format === self::FORMAT_PLAIN ?
      [
        '#markup' => $this->dateTime()->format('Y-m-d H:i:s') . ": " . $this->label(),
      ] :
      [
        '#theme' => 'html_tag',
        '#tag' => $format === self::FORMAT_INLINE ? 'span' : 'div',
        '#attributes' => [
          'class' => ['party-communication-event', 'party-communication-event--' . $this->eventType()],
        ],
        'participation' => [
          '#type' => 'html_tag',
          '#tag' => $format === self::FORMAT_INLINE ? 'span' : 'div',
          '#attributes' => [
            'class' => ['party-communication-event-participation'],
          ],
          'label' => $this->participation() ? $this->participation()->render(PartyCommunicationParticipation::FORMAT_LABEL) : [
            '#markup' => '',
          ],
          '#access' => !empty($this->participation()),
        ],
        'datetime' => [
          '#theme' => 'html_tag',
          '#tag' => $format === self::FORMAT_INLINE ? 'span' : 'div',
          '#attributes' => [
            'class' => ['party-communication-event-datetime'],
          ],
          '#value' => $this->dateTime()->format('Y-m-d H:i:s'),
        ],
        'label' => [
          '#type' => 'html_tag',
          '#tag' => $format === self::FORMAT_INLINE ? 'span' : 'div',
          '#attributes' => [
            'class' => ['party-communication-event-label'],
          ],
          '#value' => $this->label(),
        ]
      ];
  }

}
