<?php
/**
 * @file
 * Contact Info Sources classes.
 */

/**
 * Default class for contact info sources.
 */
class PartyCommunicationContactSource {

  /**
   * Definition info.
   *
   * @var array
   */
  protected $info;

  /**
   * Primitive.
   *
   * @var string
   */
  protected $primitive;

  /**
   * Static function to create new instances.
   */
  public static function create(array $info, $primitive) {
    return new static($info, $primitive);
  }

  /**
   * Default Constructor.
   */
  public function __construct(array $info, $primitive) {
    $this->info = $info;
    $this->primitive = $primitive;
  }

  /**
   * The label.
   *
   * @return string
   */
  public function label() : string {
    return $this->info['label'];
  }

  /**
   * The primitive.
   *
   * @return string
   */
  public function primitive() : string {
    return $this->primitive;
  }

  /**
   * Get the default priority of this source.
   */
  public function defaultPriority() {
    return !empty($this->info['priority']) ? $this->info['priority'] : 0;
  }

  /**
   * Get the priority of the source.
   */
  public function priority(Party $party, array $data = array()) {
    return $this->defaultPriority();
  }

  /**
   * Find a party that matches some info.
   */
  public function findParty(array $values, array $context = []) {
    $query = $this->buildQuery($values, $context);
    $pids = $query->fetchPids();

    $party = party_load(array_pop($pids));
    return $party;
  }

  /**
   * Build a query to try and find a party.
   */
  public function buildQuery(array $values, array $context = []) {
    $query = party_query();
    $query->addTag('party_communication_contact_acquisition');
    $query->addMetaData('acquisition_context', $context);
    $query->addMetaData('contact_source', $this);
    $query->condition('party.hidden', 0);
    return $query;
  }

  /**
   * Collect the contact data we have.
   *
   * @param Party $party
   *
   * @return array of contact data keyed by a unique key for each item.
   *   Each item of the array should contain a description, primitive and data
   *   keys.
   */
  public function collectData($party) {
    return array();
  }

  /**
   * Check whether a given value matches the value in some contact data.
   *
   * @param $data
   *   A piece of data found by 'collectData'.
   * @param $value
   *   A value to check against.
   *
   * @return bool
   *   True if the value matches the value stored in the data.
   */
  public function match($data, $value) {
    return $data->getData() == $value;
  }

  /**
   * Create a data item.
   */
  protected function createData($title, $data) {
    return PartyCommunicationContactData::create(
      $this->info['primitive'],
      $data,
      $title,
    )->setSource($this);
  }

  /**
   * Structure property get.
   *
   * @param \PartyCommunicationContactSource $data
   * @param array $options
   * @param $name
   * @param $type
   * @param $info
   */
  public static function structurePropertyGet($data, array $options, $name, $type, $info) {
    if (!($data instanceof PartyCommunicationContactSource)) {
      return NULL;
    }

    switch ($name) {
      case 'label':
        return $data->label();
    }

    return NULL;
  }

}

/**
 * Class for simple properties.
 */
class PartyCommunicationContactSourceProperty extends PartyCommunicationContactSource {

  public function buildQuery(array $values, array $context = []) {
    $query = parent::buildQuery($values, $context);

    $property = $this->info['property'];
    $value = $values[$this->primitive];

    if (!empty($this->info['data_set'])) {
      $query->propertyCondition($this->info['data_set'], $property, $value);
    }
    else {
      $query->condition('party.'.$property, $value);
    }

    return $query;
  }

  public function collectData($party) {
    $property = $this->info['property'];
    $wrapper = entity_metadata_wrapper('party', $party);
    $data_wrapper = NULL;
    $data = array();

    try {
      if (!empty($this->info['data_set'])) {
        $data_wrapper = $wrapper->{$this->info['data_set']}->{$property};
      }
      else {
        $data_wrapper = $wrapper->{$property};
      }
    }
    catch (Exception $e) {}

    try {
      if ($data_wrapper && $data_wrapper->value()) {
        if (!empty($this->info['label'])) {
          $title = $this->info['label'];
        }
        else {
          $title = $data_wrapper->info()['label'];
        }

        $data[] = $this->createData($title, $data_wrapper->value());
      }
    }
    catch (EntityMetadataWrapperException $exception) {}

    return $data;
  }
}

/**
 * Class for simple fields.
 */
class PartyCommunicationContactSourceField extends PartyCommunicationContactSource {

  public function buildQuery(array $values, array $context = []) {
    $query = parent::buildQuery($values, $context);

    $data_set = !empty($this->info['data_set']) ? $this->info['data_set'] : 'party';
    $field_name = $this->info['field_name'];
    $field_column = !empty($this->info['field_column']) ? $this->info['field_column'] : 'value';
    $value = $values[$this->primitive];

    $query->fieldCondition($data_set, $field_name, $field_column, $value);
    return $query;
  }

  public function collectData($party) {
    $field_name = $this->info['field_name'];
    $wrapper = entity_metadata_wrapper('party', $party);
    $data_wrapper = NULL;
    $data = array();

    // Work out the title.
    if (!empty($this->info['label'])) {
      $title = $this->info['label'];
    }
    else {
      if (empty($this->info['data_set'])) {
        $entity_type = $bundle = 'party';
      }
      else {
        $info = party_get_data_set_info($this->info['data_set']);
        $entity_type = $info['entity type'];
        $bundle = $info['entity bundle'];
      }

      $instance = field_info_instance($entity_type, $field_name, $bundle);
      $title = $instance['label'];

      if (!empty($info['label'])) {
        $title = $info['label'] . ' - ' . $title;
      }
    }

    try {
      if (!empty($this->info['data_set'])) {
        $data_wrapper = $wrapper->{$this->info['data_set']}->{$field_name};
      }
      else {
        $data_wrapper = $wrapper->{$field_name};
      }

      if ($data_wrapper) {
        if ($data_wrapper instanceof EntityListWrapper) {
          foreach ($data_wrapper as $delta => $data_item) {
            $data["{$delta}"] = $this->createData($title." (".($delta + 1).")", $data_item->value());
          }
        }
        else if ($data_wrapper->value()) {
          $data[] = $this->createData($title, $data_wrapper->value());
        }
      }
    }
    catch (Exception $e) {}

    return $data;
  }
}
