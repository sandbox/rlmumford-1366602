<?php

class PartyCommunicationParticipationsElement extends FlexiformElement {

  /**
   * {@inheritdoc}
   */
  public function form($form, &$form_state, $entity) {
    if (!isset($form_state['element_state'])) {
      $form_state['element_state'] = [];
    }

    $primitive_options = party_communication_contact_primitive_options();
    $parents = array_merge($form['#parents'], ['participation', $this->element_info['participation_role']]);

    /** @var \PartyCommunication $entity */
    $participations = $entity->getParticipations($this->element_info['participation_role']);
    $element_state = drupal_array_get_nested_value($form_state['element_state'], $parents, $key_exists);
    if (!$key_exists) {
      $element_state = [
        'communication' => $entity,
        'role_info' => $entity->getType()->getController()->getParticipationRoles()[$this->element_info['participation_role']],
      ];
    }

    $form[$this->element_namespace] = [
      '#prefix' => '<div id="' . implode('-', $parents) . '">',
      '#type' => 'fieldset',
      '#title' => $element_state['role_info']['label'],
      '#parents' => $parents,
      '#participation_role' => $this->element_info['participation_role'],
      '#suffix' => '</div>',
    ];

    $participation_role_info = $element_state['role_info'];
    $element_state['participations'] = $element_state['participations'] ?? $participations;
    foreach ($element_state['participations'] as $k => $participation) {
      $party = NULL;
      $action_base_name = implode('__', array_merge($parents, [$k])) . '__';

      $ajax = [
        'wrapper' => implode('--',array_merge($parents, [$k])),
        'method' => 'replace',
        'callback' => [$this, 'formSubmitReplaceParticipationContainerAjaxCallback'],
      ];

      $contact_data_options = [];
      if ($participation->partyId()) {
        $party = entity_load_single('party', $participation->partyId());
        foreach ($participation_role_info['supported_primitives'] as $supported_primitive) {
          foreach (PartyCommunicationContact::collectData($party, $supported_primitive) as $data_key => $data) {
            $rendered = $data->renderData(PartyCommunicationContactDataInterface::FORMAT_PLAIN);
            if (!in_array($rendered, $contact_data_options)) {
              $contact_data_options[$supported_primitive . '|' . $data_key] = $rendered;
            }
          }
        }
      }
      if (empty($contact_data_options) || !empty($this->settings['allow_custom_data'])) {
        foreach ($participation_role_info['supported_primitives'] as $supported_primitive) {
          $contact_data_options['__custom:' . $supported_primitive] = t(
            'Custom @primitive',
            ['@primitive' => $primitive_options[$supported_primitive]]
          );
        }
      }

      $form[$this->element_namespace][$k] = [
        '#type' => 'container',
        '#attributes' => [
          'id' => $ajax['wrapper'],
          'class' => [
            'party-communication-participation-form',
            'participation-role-' . $participation->role(),
          ],
        ],
        'party' => [
          'target_id' => [
            '#entity_type' => 'party_communication',
            '#entity' => $entity,
            '#type' => 'textfield',
            '#autocomplete_path' => 'flexiform/entityreference/autocomplete/single/' . $this->flexiform->form . '/' . $this->element_namespace . '/' . ($entity->id ?: 'NULL'),
            '#default_value' => !empty($party) ? $party->label() . ' (' . $party->pid . ')' : NULL,
            '#ajax' => [
              'trigger_as' => ['name' => $action_base_name . 'update_party'],
              'event' => 'change',
            ] + $ajax,
          ],
        ],
        'update_party' => [
          '#type' => 'submit',
          '#value' => t('Update Party'),
          '#name' => $action_base_name . 'update_party',
          '#participation_key' => $k,
          '#validate' => [],
          '#submit' => [
            [$this, 'formSubmitUpdateParty'],
          ],
          '#limit_validation_errors' => [
            array_merge($form[$this->element_namespace]['#parents'], [$k, 'party']),
          ],
          '#attributes' => [
            'class' => [
              'js-hide',
            ],
          ],
          '#ajax' => $ajax,
        ],
        'contact_data' => [
          '#type' => 'select',
          '#options' => $contact_data_options,
          '#default_value' => $participation->contactData()->getKey() ?: '__custom:' . $participation->contactData()->getPrimitive(),
          '#ajax' => [
            'trigger_as' => ['name' => $action_base_name . 'update_contact_data'],
          ] + $ajax,
          '#access' => count($contact_data_options) > 1 || !str_starts_with(array_keys($contact_data_options)[0], '__custom:'),
        ],
        'update_contact_data' => [
          '#type' => 'submit',
          '#value' => t('Update Party'),
          '#name' => $action_base_name . 'update_contact_data',
          '#participation_key' => $k,
          '#validate' => [],
          '#submit' => [
            [$this, 'formSubmitUpdateContactData'],
          ],
          '#limit_validation_errors' => [
            array_merge($form[$this->element_namespace]['#parents'], [$k, 'contact_data']),
          ],
          '#attributes' => [
            'class' => [
              'js-hide',
            ],
          ],
          '#ajax' => $ajax,
          '#access' => count($contact_data_options) > 1 || !str_starts_with(array_keys($contact_data_options)[0], '__custom:'),
        ],
      ];
      $context = [
        'field' => $this->getField(),
        'instance' => $this->getInstance(),
      ];
      drupal_alter('field_widget_entityreference_autocomplete_form', $form[$this->element_namespace][$k]['party'], $form_state, $context);

      if (
        empty($participation->contactData()->getKey()) ||
        str_starts_with($participation->contactData()->getKey(), '__custom')
      ) {
        $form[$this->element_namespace][$k]['custom'] = [
          '#parents' => array_merge($parents, [$k, 'custom']),
        ];
        $form[$this->element_namespace][$k]['custom'] = $participation->contactData()->buildForm(
          $form[$this->element_namespace][$k]['custom'],
          $form_state
        );
      }
    }

    if (
      $participation_role_info['cardinality'] === -1 ||
      count($element_state['participations']) < $participation_role_info['cardinality']
    ) {
      $form[$this->element_namespace]['add'] = [
        '#type' => 'submit',
        '#value' => t('+'),
        '#validate' => [],
        '#submit' => [
          [$this, 'formSubmitAddParticipation'],
        ],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'wrapper' => implode('-', $parents),
          'method' => 'replace',
          'callback' => [$this, 'formSubmitReplaceElementAjaxCallback'],
        ],
      ];
    }

    drupal_array_set_nested_value($form_state['element_state'], $parents, $element_state);
    return parent::form($form, $form_state, $entity);
  }

  /**
   * Form submit to add a participation.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   */
  public function formSubmitAddParticipation($form, &$form_state) {
    $element = drupal_array_get_nested_value($form, array_slice($form_state['triggering_element']['#array_parents'], 0, -1));
    $element_state = &drupal_array_get_nested_value($form_state['element_state'], $element['#parents']);

    $id = $this->element_info['participation_role']; $delta = 1;
    while (isset($element_state['participations'][$id])) {
      $id = $this->element_info['participation_role'] . '_' . $delta;
      $delta++;
    }
    $element_state['participations'][$id] = PartyCommunicationParticipation::create(
      $element_state['communication'],
      $this->element_info['participation_role'],
      $id,
      PartyCommunicationContact::createData(
        reset($element_state['role_info']['supported_primitives']),
        NULL
      ),
    );

    $form_state['rebuild'] = TRUE;
  }

  /**
   * Form submit to update the selected party.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   */
  public function formSubmitUpdateParty($form, &$form_state) {
    $element = drupal_array_get_nested_value($form, array_slice($form_state['triggering_element']['#array_parents'], 0, -2));
    $element_state = &drupal_array_get_nested_value($form_state['element_state'], $element['#parents']);

    $party = drupal_array_get_nested_value($form_state['values'], array_merge($element['#parents'], [$form_state['triggering_element']['#participation_key'], 'party', 'target_id']));
    preg_match('/\((?<id>[0-9]+)\)/', $party, $matches);

    /** @var \PartyCommunicationParticipation $participation */
    $participation = $element_state['participations'][$form_state['triggering_element']['#participation_key']];
    $participation->setPartyId($matches['id']);

    if ($matches['id']) {
      foreach ($element_state['role_info']['supported_primitives'] as $supported_primitive) {
        foreach (
          PartyCommunicationContact::collectData(
            entity_load_single('party', $participation->partyId()),
            $supported_primitive
          ) as $data
        ) {
          $participation->setContactData($data);
          break 2;
        }
      }
    }
    else {
      $participation->setContactData(PartyCommunicationContact::createData(
        reset($element_state['role_info']['supported_primitives']),
        NULL
      ));
    }

    $input = &drupal_array_get_nested_value($form_state['input'], array_merge($element['#parents'], [$form_state['triggering_element']['#participation_key']]));
    unset($input['custom']); unset($input['contact_data']);
    $form_state['rebuild'] = TRUE;
  }

  /**
   * Form submit to update the selected contact data.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   */
  public function formSubmitUpdateContactData($form, &$form_state) {
    $element = drupal_array_get_nested_value($form, array_slice($form_state['triggering_element']['#array_parents'], 0, -2));
    $element_state = &drupal_array_get_nested_value($form_state['element_state'], $element['#parents']);

    $contact_data = drupal_array_get_nested_value($form_state['values'], array_merge($element['#parents'], [$form_state['triggering_element']['#participation_key'], 'contact_data']));
    /** @var \PartyCommunicationParticipation $participation */
    $participation = $element_state['participations'][$form_state['triggering_element']['#participation_key']];
    if (str_starts_with($contact_data, '__custom')) {
      $participation->setContactData(PartyCommunicationContact::createData(
        substr($contact_data, strlen('__custom:')),
        NULL
      ));
    }
    else {
      [$primitive, $data_key] = explode('|', $contact_data, 2);
      $participation->setContactData(PartyCommunicationContact::getData(
        entity_load_single('party', $participation->partyId()),
        $primitive,
        [$data_key]
      ) ?: NULL);
    }

    $form_state['rebuild'] = TRUE;
  }

  public function formSubmitReplaceElementAjaxCallback($form, &$form_state) {
    return drupal_array_get_nested_value($form, array_slice($form_state['triggering_element']['#array_parents'], 0, -1));
  }

  public function formSubmitReplaceParticipationContainerAjaxCallback($form, $form_state) {
    return drupal_array_get_nested_value($form, array_slice($form_state['triggering_element']['#array_parents'], 0, -1));
  }

  /**
   * {@inheritdoc}
   */
  public function formValidate($form, &$form_state, $entity) {
    $parents = array_merge($form['#parents'], [$this->getEntityNamespace(), 'participation', $this->element_info['participation_role']]);
    $element_state = drupal_array_get_nested_value($form_state['element_state'], $parents, $key_exists);

    /** @var \PartyCommunicationParticipation $participation */
    foreach ($element_state['participations'] as $k => $participation) {
      if (empty($participation->contactData()->getKey())) {
        $participation->contactData()->validateForm($form[$this->element_namespace][$k]['custom'], $form_state);
      }
    }

    parent::formValidate($form, $form_state, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function formSubmit($form, &$form_state, $entity) {
    /** @var \PartyCommunication $entity */
    $parents = array_merge($form['#parents'], [$this->getEntityNamespace(), 'participation', $this->element_info['participation_role']]);
    $element_state = drupal_array_get_nested_value($form_state['element_state'], $parents, $key_exists);

    /** @var \PartyCommunicationParticipation $participation */
    foreach ($element_state['participations'] as $k => $participation) {
      if (empty($participation->contactData()->getKey())) {
        $participation->contactData()->submitForm($form[$this->element_namespace][$k]['custom'], $form_state);
      }

      $entity->setNeedsParticipationsSave()->addParticipation($participation);
    }
    unset($element_state['participations']);
    drupal_array_set_nested_value($form_state['element_state'], $parents, $element_state);

    parent::formSubmit($form, $form_state, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function configureForm($form, &$form_state, $flexiform) {
    $form['party'] = [
      '#type' => 'container',
      '#parents' => ['field', 'settings'],
      '#tree' => TRUE,
      '#attached' => array(
        'css' => array(drupal_get_path('module', 'entityreference') . '/entityreference.admin.css'),
      ),
      '#process' => array(
        '_entityreference_field_settings_process',
        '_entityreference_field_settings_ajax_process',
        [static::class, '_fix_entityreference_field_settings_process'],
      ),
      '#element_validate' => array('_entityreference_field_settings_validate'),
      '#field' => [
        'field_name' => 'participation__' . $this->element_info['participation_role'],
        'settings' => [
          'target_type' => 'party',
          'handler' => $this->settings['party']['handler'] ?? 'base',
          'handler_settings' => $this->settings['party']['handler_settings'] ?? [],
        ],
      ],
      '#instance' => [
        'field_name' => 'participation__' . $this->element_info['participation_role'],
        'entity_type' => 'party_communication',
        'bundle' => $this->bundle,
      ],
      '#has_data' => FALSE,
    ];

    $form['allow_custom_data'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow Custom Contact Data'),
      '#description' => t('Allow the user to enter custom contact data even when a party is selected and options are available.'),
      '#default_value' => !empty($this->settings['allow_custom_data']),
    ];

    return parent::configureForm($form, $form_state, $flexiform);
  }

  public static function _fix_entityreference_field_settings_process($form, $form_state) {
    $form['target_type']['#access'] = FALSE;
    $form['behaviors']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configureFormSubmit($form, &$form_state, $flexiform) {
    $this->settings['allow_custom_data'] = $form_state['values']['allow_custom_data'];
    $this->settings['party']['handler'] = $form_state['values']['field']['settings']['handler'];
    $this->settings['party']['handler_settings'] = $form_state['values']['field']['settings']['handler_settings'];
    unset($this->settings['party']['handler_settings']['behaviors']);

    parent::configureFormSubmit($form, $form_state, $flexiform);
  }

  /**
   * {@inheritdoc}
   */
  public function toSettingsArray() {
    $settings = parent::toSettingsArray();
    $settings['allow_custom_data'] = $this->settings['allow_custom_data'];
    $settings['party'] = $this->settings['party'];
    return $settings;
  }

  // These functions are required for the autocomplete to work.
  public function getField() {
    return [
      'type' => 'entityreference',
      'field_name' => 'participation__' . $this->element_info['participation_role'],
      'settings' => [
        'target_type' => 'party',
        'handler' => $this->settings['party']['handler'] ?? 'base',
        'handler_settings' => $this->settings['party']['handler_settings'] ?? [],
      ],
    ];
  }

  public function getInstance() {
    return [
      'field_name' => 'participation__' . $this->element_info['participation_role'],
      'entity_type' => 'party_communication',
      'bundle' => $this->bundle,
    ];
  }

}
