<?php

class party_communication_views_handler_field_participations extends views_handler_field {

  function option_definition() {
    $options = parent::option_definition();
    $options['role'] = array('default' => NULL);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $role_options = [];
    foreach (party_communication_mode_info() as $mode => $info) {
      $controller = party_communication_mode_controller($mode, []);
      foreach ($controller->getParticipationRoles() as $role => $role_info) {
        $role_options[$role] = $role_info['label'];
      }
    }

    $form['role'] = array(
      '#type' => 'select',
      '#title' => t('Which participation role should be displayed.'),
      '#options' => $role_options,
      '#default_value' => $this->options['role'],
    );
  }

  public function render($values) {
    /** @var \PartyCommunication $communication */
    $communication = entity_load_single('party_communication', $this->get_value($values));

    $controller = $communication->getType()->getController();
    $participations = $communication->getParticipations($this->options['role']);

    $build = [ ];

    foreach ($participations as $participation) {
      $build[] = $participation->render(PartyCommunicationParticipation::FORMAT_LABEL);
    }

    return $build;
  }

}
