<?php

/**
 * Views handler for the valid to send field.
 */
class party_communication_views_handler_field_communication_valid_send extends views_handler_field {
  function construct() {
    $this->additional_fields['id'] = 'id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $id = $this->get_value($values, 'id');
    $communication = entity_load_single('party_communication', $id);

    $reasons = array();
    $valid = $communication->getType()->getController()->validateSend($communication, array(), $reasons);
    if ($valid) {
      $output =array(
        '#markup' => '<div class="communication-is-valid"><span class="valid">'.t('Ready').'</span></div>',
      );
    }
    else {
      $output = array(
        '#prefix' => '<div class="communication-is-valid">',
        '#suffix' => '</div>',
        '#attached' => array(
          'css' => array(
            drupal_get_path('module', 'party_communication').'css/party_communication-valid_send.css',
          ),
        ),
        'status' => array(
          '#markup' => '<span class="invalid">'.t('Incomplete').'</span>',
        ),
        'reasons' => array(
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $reasons,
          '#attributes' => array(
            'class' => array('invalid-reasons'),
          ),
        ),
      );
    }

    return $output;
  }
}