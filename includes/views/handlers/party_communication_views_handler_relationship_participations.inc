<?php

class party_communication_views_handler_relationship_participations extends views_handler_relationship {

  function option_definition() {
    $options = parent::option_definition();
    $options['role'] = array('default' => NULL);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $role_options = [];
    foreach (party_communication_mode_info() as $mode => $info) {
      $controller = party_communication_mode_controller($mode, []);
      foreach ($controller->getParticipationRoles() as $role => $role_info) {
        $role_options[$role] = $role_info['label'];
      }
    }

    $form['role'] = array(
      '#type' => 'select',
      '#title' => t('Which participation role should be displayed.'),
      '#options' => $role_options,
      '#default_value' => $this->options['role'],
    );
  }

  public function query() {
    $this->definition['extra'][] = [
      'table' => 'party_communication_participation',
      'field' => 'participation_role',
      'value' => $this->options['role'],
    ];
    parent::query();
  }

}
