<?php

class party_communication_views_handler_field_participation_contact_data extends views_handler_field {

  public function construct() {
    parent::construct();

    $this->additional_fields['contact_data_primitive'] = 'contact_data_primitive';
    $this->additional_fields['contact_data'] = 'contact_data';
    $this->additional_fields['contact_data_key'] = 'contact_data_key';
    $this->additional_fields['data'] = 'data';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  public function render($values) {
    $data = !empty($this->get_value('data')) ? json_encode($this->get_value('data'), TRUE) : [];

    $contact_data = PartyCommunicationContact::createData(
      $this->get_value('contact_data_primitive'),
      $data['contact_data'] ?? $this->get_value('contact_data'),
      $this->get_value('contact_data_key')
    );

    return $contact_data->renderData();
  }

}
