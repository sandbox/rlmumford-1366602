<?php
/**
 * @file
 *   Views Integration.
 */

/**
 * Implements hook_views_data_alter().
 */
function party_communication_views_data_alter(&$data) {
  $data['party_communication']['type']['relationship'] = array(
    'label' => t('Communication Type'),
    'help' => t('Relate to the communication type.'),
    'base' => 'party_communication_type',
    'base field' => 'name',
  );
  $data['party_communication']['valid_send'] = array(
    'title' => t('Valid to Send'),
    'help' => t('Display whether or not the communication is valid and ready to send'),
    'field' => array(
      'handler' => 'party_communication_views_handler_field_communication_valid_send',
    ),
  );
}

