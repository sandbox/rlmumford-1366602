<?php
/**
 * @file
 * Contains PartyCommunicationContactAcquisition
 */

class PartyCommunicationContactAcquisition extends PartyAcquisition {

  /**
   * Construct a new PartyCommunicationContactAcquisition object.
   *
   * Add the primitive to the default context.
   */
  public function __construct() {
    $this->default_context['primitive'] = 'email';
  }

  /**
   * {@inheritdoc}
   */
  protected function findMatch(array $values) {
    $sources = PartyCommunicationContact::getSources($this->context['primitive']);
    PartyCommunicationContact::sortSources($sources);
    foreach ($sources as $source) {
      if ($party = $source->findParty($values, $this->context)) {
        return $party;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function acquire(array $values, array $context = NULL, &$method = '') {
    // Set up our context.
    $this->setContext($context);

    // Set up our values.
    $args = array(&$values, &$this->context);
    $this->invoke('values_alter', $args);

    // Look for a match.
    if (!empty($values)) {
      $party = $this->findMatch($values);
    }
    else {
      $this->context['fail'] = self::FAIL_NO_VALUES;
      $party = FALSE;
    }

    // If we haven't found a match, see if we should create.
    if ($party) {
      $method = 'acquire';
    }
    elseif ($this->context['behavior'] & self::BEHAVIOR_CREATE) {
      $method = 'create';
      $party = party_create();
    }

    // Fire off post acquisition hooks.
    $args = array(&$party, &$method, &$values, &$this->context);
    $this->invoke('post_acquisition', $args);

    // Return our result.
    return $party;
  }
}
