<?php

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

/**
 * Contains
 * PartyCommunicationContactDataInterface & PartyCommunicationContactDataBase.
 */

interface PartyCommunicationContactDataInterface {

  /**
   * Render format constants.
   */
  const FORMAT_PLAIN = 'plain';
  const FORMAT_INLINE = 'inline';
  const FORMAT_BLOCK = 'block';

  /**
   * Render this data into human readable format.
   *
   * @param string $format
   *   Whether to format inlilne or a block.
   */
  public function renderData($format = self::FORMAT_INLINE);

  /**
   * Format the data as a single string.
   *
   * @return string
   */
  public function stringifyData();

  /**
   * Return the data.
   */
  public function getData();

  /**
   * Get the primitive.
   */
  public function getPrimitive();

  /**
   * Get the source.
   */
  public function getSource() : ?PartyCommunicationContactSource;

  /**
   * Set the key.
   *
   * @param string $key
   *
   * @return $this
   */
  public function setKey($key);

  /**
   * Get the key.
   *
   * @return string|null
   */
  public function getKey();

  /**
   * Set the source of a piece of contact data.
   *
   * @param \PartyCommunicationContactSource $source
   *   The source.
   *
   * @return $this
   */
  public function setSource(PartyCommunicationContactSource $source) : PartyCommunicationContactDataInterface;

  public function buildForm($element, &$form_state);

  public function validateForm($element, &$form_state);

  public function submitForm($element, &$form_state);
}

class PartyCommunicationContactData implements PartyCommunicationContactDataInterface {

  protected $title;
  protected $primitive;
  protected $data;
  protected $key;
  protected $source;

  public static function create($primitive, $data, $title = NULL) {
    $primitive_info = PartyCommunicationContact::getPrimitiveInfo()[$primitive];

    $class = (
      isset($primitive_info['data_class']) &&
      ($primitive_class = $primitive_info['data_class']) &&
      class_exists($primitive_class)
    ) ? $primitive_class : 'PartyCommunicationContactData';

    return new $class($primitive, $title ?: $primitive_info['label'], $data);
  }

  public function __construct($primitive, $title, $data) {
    $this->title = $title;
    $this->primitive = $primitive;
    $this->data = $data;
  }

  public function setKey($key) {
    $this->key = $key;
    return $this;
  }

  public function getKey() {
    return $this->key;
  }

  public function getPrimitive() {
    return $this->primitive;
  }

  public function getSource() : ?PartyCommunicationContactSource {
    return $this->source;
  }

  public function setSource(PartyCommunicationContactSource $source): PartyCommunicationContactDataInterface {
    $this->source = $source;
    return $this;
  }

  public function getData() {
    return $this->data;
  }

  public function renderData($format = self::FORMAT_INLINE) {
    return $this->data ?: [];
  }

  public function stringifyData() {
    return $this->data;
  }

  public function getTitle() {
    return $this->title;
  }

  public function buildForm($element, &$form_state) {
    return [
      '#type' => 'textfield',
      '#title' => party_communication_contact_primitive_options()[$this->primitive],
      '#default_value' => $this->data,
    ] + $element;
  }

  public function validateForm($element, &$form_state) {
  }

  public function submitForm($element, &$form_state) {
    $value = drupal_array_get_nested_value($form_state['values'], $element['#parents']);
    $this->data = $value;
  }

}

/**
 * Class for representing postal address contact data.
 */
class PartyCommunicationContactDataPostalAddress extends PartyCommunicationContactData {

  public function renderData($format = self::FORMAT_INLINE) {
    if (module_exists('addressfield') && $format !== self::FORMAT_PLAIN) {
      $build = addressfield_generate($this->data, array('address', 'address-hide-country'));
      return render($build);
    }

    return implode(', ', $this->data);
  }

  public function stringifyData() {
    return implode(', ', $this->data);
  }

}

/**
 * Class for representing phone number data.
 */
class PartyCommunicationContactDataTelephoneNumber extends PartyCommunicationContactData {

  /**
   * {@inheritdoc}
   */
  public function renderData($format = self::FORMAT_INLINE) {
    try {
      $phoneUtil = PhoneNumberUtil::getInstance();
      $number = $phoneUtil->parse($this->data, "US");
      $formatted = $phoneUtil->format($number, PhoneNumberFormat::NATIONAL);
    }
    catch (NumberParseException $exception) {
      $formatted = $this->data;
    }

    switch ($format) {
      case self::FORMAT_PLAIN:
        return $formatted;
      case self::FORMAT_INLINE:
      default:
        $attributes = [
          'class' => [
            'contact-data',
            'contact-data-' . str_replace("_", "-", $this->primitive),
          ],
        ];

        return in_array($this->primitive, ['sms', 'phone']) ?
          [
            '#theme' => 'link',
            '#text' => $formatted,
            '#path' => ($this->primitive === 'sms' ? 'sms:' : 'tel:') . $formatted,
            '#options' => [
              'attributes' => $attributes,
            ],
          ] : [
            '#theme' => 'html_tag',
            '#tag' => 'span',
            '#value' => $formatted,
            '#attributes' => $attributes,
          ];
    }
  }
}

/**
 * Class for representing email address data.
 */
class PartyCommunicationContactDataEmailAddress extends PartyCommunicationContactData {

  /**
   * {@inheritdoc}
   */
  public function renderData($format = self::FORMAT_INLINE) {
    if ($format == self::FORMAT_PLAIN) {
      return $this->data;
    }

    return [
      '#theme' => 'link',
      '#text' => $this->data,
      '#path' => 'mailto:' . $this->data,
      '#options' => [
        'attributes' => [
          'class' => [
            'contact-data',
            'contact-data-' . $this->primitive,
          ],
        ],
      ],
    ];
  }
}
