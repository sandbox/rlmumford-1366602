<?php

/**
 * Component for adding a participation to a communication.
 */
class PartyCommunicationAddParticipationEntityTemplateComponent extends EntityTemplateComponentBase implements EntityTemplateComponentRequiringSetupInterface {

  /**
   * {@inheritdoc}
   */
  public function suggestConfigKey(): string {
    return $this->definition['participation_role'];
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    $structure = $this->templateConfig->getStructure();

    $role = ucfirst($this->definition['participation_role']);
    if (
      $structure instanceof EntityDrupalWrapper &&
      ($bundle = $structure->getBundle()) &&
      ($roles = entity_load_single('party_communication_type', $bundle)->getController()->getParticipationRoles()) &&
      !empty($roles[$this->definition['participation_role']])
    ) {
      $role = $roles[$this->definition['participation_role']]['label'];
    }

    return $role;
  }

  /**
   * {@inheritdoc}
   */
  public function description(): string {
    return t('Add a participation to this configuration.');
  }

  /**
   * {@inheritdoc}
   */
  public function apply(EntityMetadataWrapper $structure, array $parameters = []) {
    /** @var \PartyCommunication $communication */
    $communication = $structure->value();
    $participation_id = $this->definition['participation_role']; $delta = 1;
    while ($communication->getParticipation($participation_id)) {
      $participation_id = $this->definition['participation_role'] . '_' . $delta;
      $delta++;
    }

    if (!empty($this->configuration['party'])) {
      /** @var \EntityDrupalWrapper $party */
      $party = $this->getNestedWrapper($parameters, $this->configuration['party']);

      if ($party && $party->type() === 'party_communication_participation' && $party->value()) {
        /** @var \PartyCommunicationParticipation $source_participation */
        $source_participation = $party->value();
        $participation = PartyCommunicationParticipation::create(
          $communication,
          $this->definition['participation_role'],
          $participation_id,
          $source_participation->contactData(),
          $source_participation->contactName(),
          $source_participation->partyId(),
          'pending'
        );
      }
      else if ($party && $party->type() === 'party' && $party->value()) {
        $datas = [];
        foreach ($this->configuration['contact_data'] as $key) {
          [$primitive, $source_key] = explode('|', $key, 2);
          $datas[$primitive][$source_key] = $source_key;
        }

        foreach ($datas as $primitive => $keys) {
          $data = PartyCommunicationContact::getData($party->value(), $primitive, $keys);

          if ($data) {
            break;
          }
        }

        if ($data) {
          $participation = PartyCommunicationParticipation::create(
            $communication,
            $this->definition['participation_role'],
            $participation_id,
            $data,
            $party->label(),
            $party->getIdentifier(),
            'pending'
          );
        }
      }
    }
    else {
      // @todo: Consider token replacements for contact data.
      $participation = PartyCommunicationParticipation::create(
        $communication,
        $this->definition['participation_role'],
        $participation_id,
        PartyCommunicationContactData::create(
          $this->configuration['custom']['primitive'],
          $this->configuration['custom']['data']
        ),
        $this->configuration['contact_name'],
        NULL,
        'pending'
      );
    }

    if ($participation) {
      $communication
        ->setNeedsParticipationsSave()
        ->addParticipation($participation);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function applies(TemplateConfiguration $template_configuration): bool {
    return ($template_configuration->getStructure() instanceof EntityDrupalWrapper) &&
      $template_configuration->getStructure()->type() === 'party_communication';
  }

  /**
   * {@inheritdoc}
   */
  public function needsSetup(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setupForm($form, &$form_state) {
    /** @var \EntityDrupalWrapper $wrapper */
    $wrapper = $this->templateConfig->getStructure();

    if (!isset($form_state['definition'])) {
      $form_state['definition'] = array();
    }

    $participation_roles = entity_load_single('party_communication_type', $wrapper->getBundle())
      ->getController()
      ->getParticipationRoles();

    $form['participation_role'] = array(
      '#type' => 'select',
      '#title' => t('Participation Role'),
      '#required' => TRUE,
      '#options' => array_map(fn($role) => $role['label'], $participation_roles),
      '#default_value' => $form_state['definition']['participation_role'] ?? ($this->definition['participation_role'] ?? NULL),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setupFormValidate($form, &$form_state) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function setupFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    $this->definition['participation_role'] = $values['participation_role'];
  }

  /**
   * {@inheritdoc}
   */
  public function configForm($form, &$form_state) {
    /** @var \EntityDrupalWrapper $wrapper */
    $wrapper = $this->templateConfig->getStructure();
    $participation_role = entity_load_single('party_communication_type', $wrapper->getBundle())
      ->getController()
      ->getParticipationRoles()[$this->definition['participation_role']];

    form_load_include($form_state, 'inc', 'rules', 'ui/ui.forms');

    $tcAddr = $this->templateConfig->getFormStateAddress() ?
      implode(':', $this->templateConfig->getFormStateAddress()) :
      'null';
    $key = md5(json_encode(['party' => ['type' => 'party']]));
    $form_state['entity_template_selector_structs'][$key] = ['party' => ['type' => 'party']];
    $acp = "entity_template/selector/{$tcAddr}/struct/{$key}/party";

    $form['party'] = [
      '#type' => 'rules_data_selection',
      '#title' => t('Select'),
      '#default_value' => $this->configuration['party'] ?? [],
      '#autocomplete_path' => $acp,
      '#size' => 75,
    ];

    $contact_data_options = [];
    foreach ($participation_role['supported_primitives'] as $supported_primitive) {
      foreach (PartyCommunicationContact::getSources($supported_primitive) as $key => $source) {
        $contact_data_options["{$supported_primitive}|{$key}"] = $source->label();
      }
    }
    $party_parents = array_merge($form['#parents'], ['party']);
    $party_element_name = array_shift($party_parents) . (!empty($party_parents) ?
        '[' . implode('][', $party_parents) . ']' :
        ''
      );
    $form['contact_data'] = [
      '#type' => 'checkboxes',
      '#title' => t('Contact Data'),
      '#description' => t('Select which data sources you are willing to use. Leave blank to accept the first.'),
      '#options' => $contact_data_options,
      '#default_value' => $this->configuration['contact_data'],
      '#states' => [
        'visible' => [
          ':input[name="'.$party_element_name.'"]' => ['filled' => TRUE],
        ],
      ],
    ];

    $data_wrapper = implode('--', array_merge($form['#parents'], ['custom', 'data']));
    $update_primitive_action_name = implode('__',  array_merge($form['#parents'], ['custom', 'primitive', 'update']));
    $primitive_options = [];
    foreach (party_communication_contact_primitive_options() as $primitive => $label) {
      if (in_array($primitive, $participation_role['supported_primitives'])) {
        $primitive_options[$primitive] = $label;
      }
    }
    $form['custom'] = [
      '#type' => 'container',
      '#states' => [
        'invisible' => [
          ':input[name="'.$party_element_name.'"]' => ['filled' => TRUE],
        ],
      ],
      'contact_name' => [
        '#type' => 'textfield',
        '#title' => t('Contact Name'),
        '#default_value' => $this->configuration['custom']['contact_name'],
      ],
      'primitive' => [
        '#type' => 'select',
        '#options' => $primitive_options,
        '#default_value' => $this->configuration['custom']['primitive'] ?: reset($participation_role['supported_primitives']),
        '#access' => count($primitive_options) > 1,
        '#ajax' => [
          'wrapper' => $data_wrapper,
          'method' => 'replace',
          'callback' => [static::class, 'configFormSubmitUpdateCustomPrimitiveAjaxCallback'],
          'trigger_as' => ['name' => $update_primitive_action_name],
        ],
      ],
      'update_primitive' => [
        '#type' => 'submit',
        '#value' => t('Update Primitive'),
        '#name' => $update_primitive_action_name,
        '#validate' => [],
        '#submit' => [
          [$this, 'configFormSubmitUpdateCustomPrimitive'],
        ],
        '#access' => count($primitive_options) > 1,
        '#ajax' => [
          'wrapper' => $data_wrapper,
          'method' => 'replace',
          'callback' => [static::class, 'configFormSubmitUpdateCustomPrimitiveAjaxCallback'],
        ],
        '#attributes' => [
          'class' => ['js-hide'],
        ],
      ],
      'data' => [
        '#prefix' => '<div id="' . $data_wrapper . '">',
        '#suffix' => '</div>',
        '#parents' => array_merge($form['#parents'], ['custom', 'data']),
      ],
    ];

    $component_state = drupal_array_get_nested_value($form_state['component_state'], $form['#parents']) ?: [];
    $component_state['custom_data'] = $component_state['custom_data'] ?: PartyCommunicationContact::createData(
      $this->configuration['custom']['primitive'] ?: reset($participation_role['supported_primitives']),
      $this->configuration['custom']['data'] ?? NULL
    );
    drupal_array_set_nested_value($form_state['component_state'], $form['#parents'], $component_state);
    $form['custom']['data'] = $component_state['custom_data']->buildForm($form['custom']['data'], $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configFormSubmit($form, &$form_state) {
    $component_state = drupal_array_get_nested_value($form_state['component_state'], $form['#parents']);
    $component_state['custom_data']->submitForm($form['custom']['data'], $form_state);

    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    if ($values['party']) {
      $this->configuration['party'] = $values['party'];
      $this->configuration['contact_data'] = array_filter($values['contact_data']);
    }
    else {
      $this->configuration['custom'] = [
        'contact_name' => $values['custom']['contact_name'],
        'primitive' =>  $component_state['custom_data']->getPrimitive(),
        'data' => $component_state['custom_data']->getData(),
      ];
    }
  }

  /**
   * Update the custom primitive.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   */
  public function configFormSubmitUpdateCustomPrimitive($form, &$form_state) {
    $button = $form_state['triggering_element'];
    $component_state = drupal_array_get_nested_value($form_state['component_state'], array_slice($button['#parents'], 0, -2));
    $component_state['custom_data'] = PartyCommunicationContact::createData(
      drupal_array_get_nested_value($form_state['values'], array_splice($button['#parents'], 0, -1, ['primitive'])),
      NULL
    );
    $form_state['rebuild'] = TRUE;
  }

  /**
   * Update the custom primitive ajax callback and return the element.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return array
   */
  public static function configFormSubmitUpdateCustomPrimitiveAjaxCallback($form, &$form_state) {
    $button = $form_state['triggering_element'];
    return drupal_array_get_nested_value($form, array_splice($button['#array_parents'], 0, -1, ['data']));
  }

  /**
   * Get a nested property.
   */
  protected function getNestedWrapper($wrapped_params, $selector) {
    $parts = explode(':', $selector);
    $namespace = str_replace('-', '_', array_shift($parts));
    $selected = $wrapped_params[$namespace];

    if (empty($selected)) {
      return FALSE;
    }

    try {
      while (($sub = array_shift($parts)) !== NULL) {
        try {
          $sub = str_replace('-', '_', $sub);
          $selected = $selected->get($sub);
        }
        catch (EntityMetadataWrapperException $e) {
          return FALSE;
        }
      }
    }
    catch (Exception $e) {
      return FALSE;
    }

    return $selected;
  }
}
