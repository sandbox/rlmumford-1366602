<?php
/**
 * @file
 * Contains PartyCommunicationContact
 */

use Drupal\Component\Plugin\Exception\MissingValueContextException;

/**
 * Helper class for handling party contact information sources.
 */
class PartyCommunicationContact {

  /**
   * Get the primitive info.
   */
  public static function getPrimitiveInfo($reset = FALSE) {
    $primitives = &drupal_static(__CLASS__.'::'.__METHOD__);

    if (!isset($primitives) || $reset) {
      if (!$reset && ($cache = cache_get('party_communication:contact_primitives', 'cache'))) {
        $primitives = $cache->data;
      }
      else {
        $primitives = module_invoke_all('party_communication_contact_primitive_info');
        drupal_alter('party_communication_contact_primitive_info', $primitives);
        cache_set('party_communication:contact_primitives', $primitives);
      }
    }

    return $primitives;
  }

  /**
   * Get the source info.
   */
  public static function getSourceInfo($reset = FALSE) {
    $sources = &drupal_static(__CLASS__.'::'.__METHOD__);

    if (!isset($sources) || $reset) {
      if (!$reset && ($cache = cache_get('party_communication:contact_sources', 'cache'))) {
        $sources = $cache->data;
      }
      else {
        $sources = module_invoke_all('party_communication_contact_source_info');
        drupal_alter('party_communication_contact_source_info', $sources);
        cache_set('party_communication:contact_sources', $sources);
      }
    }

    return $sources;
  }

  /**
   * q
   * Get the source info for a particular primitive.
   *
   * @param string $primitive
   *   The primitive you want sources for.
   */
  public static function getPrimitiveSourceInfo($primitive) {
    $sources = static::getSourceInfo();
    $primitive_sources = array_filter(
      $sources,
      new PartyCommunicationContactPrimitiveFilter($primitive)
    );
    return $primitive_sources;
  }

  /**
   * Get the contact sources.
   *
   * @return \PartyCommunicationContactSource[]
   *   The communication contact source.
   */
  public static function getSources($primitive = '') {
    $source_info = !empty($primitive) ? static::getPrimitiveSourceInfo($primitive) : static::getSourceInfo();
    $sources = array();
    foreach ($source_info as $key => $info) {
      $class = !empty($info['class']) ? $info['class'] : 'PartyCommunicationContactSource';
      $sources[$key] = $class::create($info, $primitive);
    }

    return $sources;
  }

  /**
   * Sort an array of sources.
   */
  public static function sortSources(&$sources, $party = NULL) {
    uasort($sources, new PartyCommunicationContactSourceSorter($party));
  }

  /**
   * Get a specific piece of data.
   *
   * @param $party
   *   The party entity to get contact data for.
   * @param $primitive
   *   The type of contact data to collect.
   * @param $keys
   *   An optional list of keys in order of priority. If none of the keys is
   *   available, the highest priority will be taken.
   */
  public static function getData($party, $primitive = '', $keys = array()) {
    $data = static::collectData($party, $primitive);
    foreach ($keys as $key) {
      if (!empty($data[$key])) {
        return $data[$key];
      }
    }
    return reset($data);
  }

  /**
   * Check that a piece of contact data matches a given party.
   */
  public static function checkData($party, $data, $primitive = '', &$found_data = NULL) {
    $party_data = static::collectData($party, $primitive);
    foreach ($party_data as $data_piece) {
      if ($data_piece['source']->match($data_piece, $data)) {
        $found_data = $data_piece;
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Collect data.
   *
   * @return \PartyCommunicationContactDataInterface[]
   *   The data.
   */
  public static function collectData($party, $primitive = '') {
    $sources = static::getSources($primitive);
    static::sortSources($sources, $party);

    $data = array();
    foreach ($sources as $key => $source) {
      foreach ($source->collectData($party) as $source_data_key => $source_data) {
        if (is_string($source_data_key)) {
          $data_key = $key.'|'.$source_data_key;
        }
        else {
          $data_key = $key;
        }

        $source_data->setKey($data_key);
        $data[$data_key] = $source_data;
      }
    }

    return $data;
  }

  /**
   * Create a contact data.
   *
   * @param string $primitive
   * @param mixed $data
   *   The data.
   * @param string|null $key
   *   The data key.
   * @param \PartyCommunicationContactSource|null $source
   *
   * @return \PartyCommunicationContactDataInterface
   */
  public static function createData(string $primitive, $data, string $key = NULL, PartyCommunicationContactSource $source = NULL) : PartyCommunicationContactDataInterface {
    if ($key && !$source) {
      [$source_key, $data_key] = explode('|', $key);
      $source = static::getSources()[$source_key];
    }

    $contact_data = PartyCommunicationContactData::create(
      $primitive,
      $data,
      $source ? $source->label() : NULL,
    );
    if ($key) {
      $contact_data->setKey($key);
    }
    if ($source) {
      $contact_data->setSource($source);
    }

    return $contact_data;
  }

  /**
   * Structure property get.
   *
   * @param \PartyCommunicationContactData $data
   * @param array $options
   * @param $name
   * @param $type
   * @param $info
   */
  public static function structurePropertyGet($data, array $options, $name, $type, $info) {
    if (!($data instanceof PartyCommunicationContactData)) {
      return NULL;
    }

    switch ($name) {
      case 'primitive':
        return $data->getPrimitive();
      case 'raw':
        return $data->getData();
      case 'string':
        return $data->stringifyData();
      case 'formatted':
        $rendered = $data->renderData();
        return is_array($rendered) ? drupal_render($rendered) : $rendered;
      case 'key':
        return $data->getKey();
      case 'source':
        return $data->getSource();
    }

    return NULL;
  }

  /**
   * Alter the structure property info.
   *
   * @param EntityMetadataWrapper $wrapper
   *   The wrapper.
   * @param array $info
   */
  public static function structurePropertyInfoAlter($wrapper, $info) {
    // @todo: Let other primitives alter the raw data.
    try {
      if ($wrapper->value()->getPrimitive() === 'postal_address') {
        $info['properties']['raw'] = [
            'type' => 'struct',
            'property info' => addressfield_data_property_info(),
          ] + $info['properties']['raw'];
      }
    }
    catch (EntityMetadataWrapperException $e) {}

    return $info;
  }
}

/**
 * Helper class to sort specific datas.
 */
class PartyCommunicatonContactDataSorter {

  /**
   * The party we are looking for contact info for.
   *
   * @param Party $party
   */
  protected $party = NULL;

  /**
   * Construct a new sorter.
   */
  public function __construct(Party $party) {
    $this->party = $party;
  }

  /**
   * Invoke the comparison.
   *
   * If we have a party then we use the 'priority' method on the source,
   * otherwise we use the 'defaultPriority' method.
   */
  public function __invoke($data1, $data2) {
    $priority1 = $data1['source']->priority($this->party, $data1);
    $priority2 = $data2['source']->priority($this->party, $data2);

    if ($priority1 == $priority2) {
      return 0;
    }

    return $priority1 < $priority2 ? 1 : -1;
  }
}

/**
 * Helper class to sort sources.
 */
class PartyCommunicationContactSourceSorter {

  /**
   * The party we are looking for contact info for.
   *
   * @param Party $party
   */
  protected $party = NULL;

  /**
   * Construct a new sorter.
   */
  public function __construct(Party $party = NULL) {
    $this->party = $party;
  }

  /**
   * Invoke the comparison.
   *
   * If we have a party then we use the 'priority' method on the source,
   * otherwise we use the 'defaultPriority' method.
   */
  public function __invoke($source1, $source2) {
    $priority1 = !empty($this->party) ? $source1->priority($this->party) : $source1->defaultPriority();
    $priority2 = !empty($this->party) ? $source2->priority($this->party) : $source2->defaultPriority();

    if ($priority1 == $priority2) {
      return 0;
    }

    return $priority1 < $priority2 ? 1 : -1;
  }
}

/**
 * Helper class to filter sources by primitive.
 */
class PartyCommunicationContactPrimitiveFilter {

  /**
   * The primitive to filter by.
   */
  protected $primitive;

  /**
   * Construct a new object.
   *
   * @param the primitive to filter for.
   */
  public function __construct($primitive) {
    $this->primitive = $primitive;
  }

  /**
   * Invoke this method.
   */
  public function __invoke($source) {
    if ($source instanceof PartyCommunicationContactSource) {
      return ($source->getPrimitive() == $this->primitive);
    }
    else {
      return !empty($source['primitive']) && $source['primitive'] == $this->primitive;
    }
  }
}

