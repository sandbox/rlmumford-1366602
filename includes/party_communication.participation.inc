<?php

/**
 * Represents one participation in a communication.
 */
class PartyCommunicationParticipation {

  /**
   * Render formats.
   *
   * @todo: Consider moving rendering to a difference service.
   */
  public const FORMAT_PLAIN = 'plain';

  public const FORMAT_LABEL = 'label';
  public const FORMAT_INLINE = 'inline';
  public const FORMAT_BLOCK = 'block';

  protected string $id;

  protected ?string $oldId = NULL;

  protected string $role;

  protected ?int $party;

  protected ?string $status;

  protected ?string $contactName;

  protected ?PartyCommunicationContactDataInterface $contactData;

  /**
   * Data associated with this participation.
   * @var array
   */
  public array $data = [];

  /**
   * The Communication.
   *
   * @var \PartyCommunication|null
   */
  protected ?PartyCommunication $communication = NULL;

  /**
   * Whether this needs acquisition.
   *
   * @var boolean
   */
  protected bool $needsAcquisition = FALSE;

  /**
   * The acquisition context.
   *
   * @var array
   */
  protected array $acquisitionContext = [];

  /**
   * @param \PartyCommunication $communication
   * @param string $role
   * @param string|null $id
   * @param \PartyCommunicationContactDataInterface|null $contact_data
   * @param string|null $contact_name
   * @param int|null $party_id
   * @param string|null $status
   * @param array $data
   *
   * @return \PartyCommunicationParticipation
   */
  public static function create(PartyCommunication $communication, string $role, ?string $id, ?PartyCommunicationContactDataInterface $contact_data = NULL, ?string $contact_name = NULL, ?int $party_id = NULL, ?string $status = NULL, array $data = []) {
    $class = static::class;
    if (
      ($role_class = $communication->getType()->getController()->getParticipationRoles()[$role]['class']) &&
      class_exists($role_class) && is_subclass_of($role_class, PartyCommunicationParticipation::class)
    ) {
      $class = $role_class;
    }

    return (new $class(
      $id ?? uniqid($role . '_', TRUE),
      $role,
      $contact_data,
      $contact_name,
      $party_id,
      $status,
      $data
    ))->setCommunication($communication);
  }

  /**
   * Construct a new participation.
   *
   * @param string $id
   * @param string $role
   * @param \PartyCommunicationContactDataInterface|NULL $contact_data
   * @param string|NULL $contact_name
   * @param int|NULL $party_id
   * @param string|NULL $status
   * @param array $data
   */
  public function __construct(string $id, string $role, ?PartyCommunicationContactDataInterface $contact_data = NULL, ?string $contact_name = NULL, ?int $party_id = NULL, ?string $status = NULL, array $data = []) {
    $this->id = $id;
    $this->role = $role;
    $this->contactName = $contact_name;
    $this->contactData = $contact_data;
    $this->party = $party_id;
    $this->status = $status;
    $this->data = $data;
  }

  /**
   * Set the communication.
   *
   * @param \PartyCommunication|null $communication
   *   The communication.
   *
   * @return $this
   */
  public function setCommunication(?PartyCommunication $communication) : PartyCommunicationParticipation {
    $this->communication = $communication;
    return $this;
  }

  /**
   * Get the communication.
   *
   * @return \PartyCommunication|null
   */
  public function getCommunication() : ?PartyCommunication {
    return $this->communication;
  }

  /**
   * Get the id of this participation.
   *
   * @return string
   */
  public function id() : string {
    return $this->id;
  }

  /**
   * Set the id of a participation.
   *
   * @param string $new_id
   *   The new id.
   *
   * @return $this
   */
  public function setId(string $new_id) : PartyCommunicationParticipation {
    $this->oldId = $this->id;
    $this->id = $new_id;
    return $this;
  }

  /**
   * Whether the participation has an old id.
   *
   * @return bool
   */
  public function hasOldId() : bool {
    return $this->oldId !== null;
  }

  /**
   * The old id of the participation.
   *
   * @return string|null
   */
  public function oldId() : ?string {
    return $this->oldId;
  }

  /**
   * Get the role of this participation.
   *
   * @return string
   */
  public function role() : string {
    return $this->role;
  }

  public function setRole(string $role) : string {
    $this->role = $role;
    return $this->role;
  }

  /**
   * Get the contact name.
   *
   * @return string|null
   */
  public function contactName() : ?string {
    return $this->contactName ?? NULL;
  }

  /**
   * Set the contact name.
   *
   * @param string|null $name
   *
   * @return $this
   */
  public function setContactName(?string $name) : PartyCommunicationParticipation {
    $this->contactName = $name;
    return $this;
  }

  /**
   * Get the contact data associated with this participation.
   *
   * @return \PartyCommunicationContactDataInterface|null
   */
  public function contactData() : ?PartyCommunicationContactDataInterface {
    return $this->contactData ?? NULL;
  }

  /**
   * Set the contact data associated with this participation.
   *
   * @param \PartyCommunicationContactData|null $contact_data
   *
   * @return $this
   */
  public function setContactData(?PartyCommunicationContactData $contact_data = NULL) : PartyCommunicationParticipation {
    $this->contactData = $contact_data;
    return $this;
  }

  /**
   * Get the party id associated with this participation.
   *
   * @return int|null
   */
  public function partyId() : ?int {
    if ($this->party) {
      return $this->party;
    }

    // If there is no party id and this needs acquisition then do it.
    if ($this->needsAcquisition()) {
      $context = $this->acquisitionContext() + [
          'name' => 'party_communication_inbound',
          'class' => 'PartyCommunicationContactAcquisition',
          'primitive' => $this->contactData()->getPrimitive(),
          'communication' => $this->getCommunication(),
          'behavior' => \PartyAcquisitionInterface::BEHAVIOR_CREATE,
          'party_hat' => array(
            'add' => array(
              'party_indiv',
            ),
          ),
        ];
      $values[$this->contactData()->getPrimitive()] = $this->contactData()->getData();

      $party = party_acquire($values, $context, $method);
      if ($method == 'create') {
        $party->save();

        // If we extracted a party name from the email address then put it on the
        // party.
        if (!empty($context['defaults']['profile2_party_indiv']['party_indiv_name'])) {
          $indiv_profile = $party->getDataSetController('profile2_party_indiv')->getEntity(0, TRUE);
          if (empty($indiv_profile->party_indiv_name[LANGUAGE_NONE][0])) {
            $indiv_profile->party_indiv_name[LANGUAGE_NONE][0] = $context['defaults']['profile2_party_indiv']['party_indiv_name'];
          }
        }

        foreach ($party->data_set_controllers as $controller) {
          $controller->save(TRUE);
        }
      }

      if ($party) {
        $this->setPartyId($party->pid);
        $this->setContactName($party->label);
      }

      $this->setNeedsAcquisition(FALSE);
    }

    return $this->party ?? NULL;
  }

  /**
   * Set the party id.
   *
   * @param int|null $party_id
   *
   * @return $this
   */
  public function setPartyId(?int $party_id = NULL) {
    $this->party = $party_id;
    return $this;
  }

  public function status() : ?string {
    return $this->status;
  }

  /**
   * Set the participation status.
   *
   * @param string $status
   *   The status.
   *
   * @return \PartyCommunicationParticipation
   */
  public function setStatus(string $status) : PartyCommunicationParticipation {
    $this->status = $status;
    return $this;
  }

  public function data() : array {
    return $this->data ?? [];
  }

  /**
   * Set whether this needs an acquisition.
   *
   * @param bool $needs_acquisition
   *
   * @return $this
   */
  public function setNeedsAcquisition(bool $needs_acquisition = TRUE, array $acquisition_context = []) : PartyCommunicationParticipation {
    $this->needsAcquisition = $needs_acquisition;
    $this->acquisitionContext = $acquisition_context;
    return $this;
  }

  /**
   * Check whether this needs acquisition.
   *
   * @return bool
   */
  public function needsAcquisition() : bool {
    return $this->needsAcquisition;
  }

  /**
   * Get the acquisition context.
   *
   * @return array
   */
  public function acquisitionContext() : array {
    return $this->acquisitionContext;
  }

  /**
   * Render the participation.
   *
   * @param $format
   * @param $options
   *
   * @return array
   */
  public function render($format = self::FORMAT_INLINE, $options = []) : array {
    if ($format === self::FORMAT_PLAIN) {
      $label = $this->contactName();
      if ($this->partyId() && ($party = entity_load_single('party', $this->partyId()))) {
        $label = strip_tags($party->label());
      }

      $data = $this->contactData()->renderData(PartyCommunicationContactDataInterface::FORMAT_PLAIN);
      $data = is_array($data) ? drupal_render($data) : $data;
      return [
        '#markup' => $label . ($data ? " (" . $data . ")" : ''),
      ];
    }
    else if (in_array($format, [self::FORMAT_INLINE, self::FORMAT_LABEL])) {
      $build = [];
      if ($this->partyId() && ($party = entity_load_single('party', $this->partyId()))) {
        $build['name'] = entity_access('view', 'party', $party) && ($uri = entity_uri('party', $party)) ?
          [
            '#theme' => 'link',
            '#text' => $party->label(),
            '#path' => $uri['path'],
            '#options' => ($uri['options'] ?? []) + [
              'attributes' => [
                'target' => '_blank',
              ],
            ],
          ] : [
            '#theme' => 'html_tag',
            '#tag' => 'span',
            '#value' => $party->label(),
            '#attributes' => [
              'class' => 'participation-name',
            ],
          ];
      }
      else if ($name = $this->contactName()) {
        $build['name'] = [
          '#theme' => 'html_tag',
          '#tag' => 'span',
          '#value' => $name,
          '#attributes' => [
            'class' => ['participation-name'],
          ],
        ];
      }

      $contact_data = $this->contactData()->renderData($format === self::FORMAT_LABEL ? PartyCommunicationContactDataInterface::FORMAT_INLINE : $format);
      $build['contact'] = is_array($contact_data) ? $contact_data : [
        '#theme' => 'html_tag',
        '#tag' => 'span',
        '#attribtues' => [
          'class' => ['participation-data'],
        ],
        '#value' => $contact_data,
      ];

      return $build;
    }

    return [];
  }

  /**
   * Build a form to edit this participation.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return array
   */
  public function buildForm(array $form, array &$form_state) {
    drupal_array_set_nested_value($form_state, array_merge(['participations'], $form['#parents'] ?? [], ['#participation']), $this);

    $form['#tree'] = TRUE;
    $form['#attributes']['id'] = implode('-', $form['#parents'] ?? []) . '--participation-form-wrapper';
    $ajax = [
      'wrapper' => $form['#attributes']['id'],
      'callback' => [static::class, 'submitFormReloadAjaxCallback'],
    ];

    $party_info = entity_get_info('party');
    $party = $this->party ? party_load($this->party) : NULL;
    $form['party'] = [
      '#type' => 'textfield',
      '#title' => $party_info['label'],
      '#autocomplete_path' => 'party_extras/autocomplete',
      '#default_value' => $party ? t('@label (@id)', ['@label' => $party->label(), '@id' => $party->pid]) : "",
      '#element_validate' => [
        '_ck_opencrm_entity_id_autocomplete_single_validate', // @todo: Remove dependency.
      ],
      '#ajax' => $ajax + [
        'event' => 'change',
        'trigger_as' => ['name' => implode($form['#parents'] ?? [], "_") . "__update_party"],
      ],
    ];
    $context = [
      'field' => [
        'type' => 'entityreference',
        'field_name' => 'participation__' . $this->role(),
        'settings' => [
          'target_type' => 'party',
          'handler' => 'base',
          'handler_settings' => [],
        ],
      ],
      'instance' => [
        'field_name' => 'participation__' . $this->role(),
        'entity_type' => 'party_communication',
        'bundle' => $this->getCommunication()->bundle(),
      ],
    ];
    $hook_form = [
      'target_id' => [
        '#entity' => $this->getCommunication(),
        '#entity_type' => 'party_communication',
      ] + $form['party'],
    ];
    drupal_alter('field_widget_entityreference_autocomplete_form', $hook_form, $form_state, $context);
    $form['party'] = $hook_form['target_id'];
    $form['update_party'] = [
      '#type' => 'submit',
      '#name' => implode($form['#parents'] ?? [], "_") . "__update_party",
      '#value' => t('Update @label', ['@label' => $party_info['label']]),
      '#submit' => [
        [static::class, 'submitFormUpdateParty'],
      ],
      '#validate' => [],
      '#limit_validation_errors' => [
        array_merge($form['#parents'] ?? [], ['party']),
      ],
      '#ajax' => $ajax + [
        'event' => 'change',
      ],
      '#attributes' => [
        'class' => ['js-hide'],
      ],
    ];

    $primitives = $this->getCommunication()->getType()->getController()->getParticipationRoles()[$this->role]['supported_primitives'] ?? [];
    $options = [];
    if ($this->party) {
      foreach ($primitives as $primitive) {
        foreach (PartyCommunicationContact::collectData($party, $primitive) as $party_contact_data) {
          $options["{$primitive}:{$party_contact_data->getKey()}"] = $party_contact_data->stringifyData(
            ) . " (" . $party_contact_data->getSource()->label() . ")";
        }
      }
    }

    foreach ($primitives as $primitive) {
      $options["{$primitive}:__custom"] = t('Custom @label', [
        '@label' => party_communication_contact_primitive_options()[$primitive],
      ]);
    }

    $form['contact_data'] = [
      '#type' => 'select',
      '#options' => $options,
      '#empty_option' => t('- Select -'),
      '#required' => TRUE,
      '#default_value' => $this->contactData() ? $this->contactData()->getPrimitive() . ":" . ($this->contactData()->getKey() ?: '__custom') : NULL,
      '#ajax' => $ajax + [
        'trigger_as' => ['name' => implode($form['#parents'] ?? [], "_") . "__update_contact_data"],
      ],
    ];
    $form['update_contact_data'] = [
      '#type' => 'submit',
      '#name' => implode($form['#parents'] ?? [], "_") . "__update_contact_data",
      '#value' => t('Update Contact Data'),
      '#submit' => [
        [static::class, 'submitFormUpdateContactData'],
      ],
      '#validate' => [],
      '#limit_validation_errors' => [
        array_merge($form['#parents'] ?? [], ['contact_data']),
      ],
      '#ajax' => $ajax,
      '#attributes' => [
        'class' => ['js-hide'],
      ],
    ];

    if ($this->contactData() && !$this->contactData()->getKey()) {
      $form['contact_data_custom'] = [
        '#type' => 'container',
        '#parents' => array_merge($form['#parents'] ?? [], ['contact_data_custom']),
      ];
      $form['contact_data_custom'] = $this->contactData()->buildForm($form['contact_data_custom'], $form_state);
    }

    return $form;
  }

  /**
   * Validate the participation form.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   */
  public function validateForm(array $form, array &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents'] ?? []);

    [$primitive, $key] = explode(":", $values['contact_data'], 2);
    if ($key === '__custom') {
      $this->contactData = PartyCommunicationContactData::create($primitive, NULL);
      $this->contactData->validateForm($form['contact_data_custom'], $form_state);
    }
  }

  /**
   * Submit the participation form.
   *
   * @param array $form
   *   The form.
   * @param array $form_state
   *   The form state.
   *
   * @return void
   */
  public function submitForm(array $form, array &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents'] ?? []);
    $this->party = $values['party'];

    [$primitive, $key] = explode(":", $values['contact_data'], 2);
    if ($key === '__custom') {
      $this->contactData = PartyCommunicationContactData::create($primitive, NULL);
      $this->contactData->submitForm($form['contact_data_custom'], $form_state);
    }
    else {
      $this->contactData = PartyCommunicationContact::getData(party_load($this->party), $primitive, [$key]);
    }
  }

  public static function submitFormUpdateParty($form, &$form_state) {
    $array_parents = $form_state['triggering_element']['#array_parents'];
    array_splice($array_parents, -1, 1, ['party']);
    $element = drupal_array_get_nested_value($form, $array_parents);

    $participation = drupal_array_get_nested_value($form_state, array_merge(['participations'], array_slice($element['#parents'], 0, -1), ['#participation']));
    $participation->setPartyId(drupal_array_get_nested_value($form_state['values'], $element['#parents']));
    $participation->setContactData(NULL);

    $form_state['rebuild'] = TRUE;
  }

  public static function submitFormUpdateContactData($form, &$form_state) {
    $array_parents = $form_state['triggering_element']['#array_parents'];
    array_splice($array_parents, -1, 1, ['contact_data']);
    $element = drupal_array_get_nested_value($form, $array_parents);
    $participation = drupal_array_get_nested_value($form_state, array_merge(['participations'], array_slice($element['#parents'], 0, -1), ['#participation']));
    $selection = drupal_array_get_nested_value($form_state['values'], $element['#parents']);

    [$primitive, $key] = explode(":", $selection, 2);
    if ($key === "__custom") {
      $participation->setContactData(PartyCommunicationContactData::create(
        $primitive,
        NULL
      ));
    }
    else {
      $participation->setContactData(PartyCommunicationContact::getData(party_load($participation->partyId()), $primitive, [$key]));
    }

    $form_state['rebuild'] = TRUE;
  }

  public static function submitFormReloadAjaxCallback($form, $form_state) {
    return drupal_array_get_nested_value($form, array_slice($form_state['triggering_element']['#array_parents'], 0, -1));
  }

  /**
   * Get the properties of a participation structure.
   *
   * @return array[]
   */
  public static function structurePropertyInfo() : array {
    return [
      'id' => [
        'type' => 'text',
        'label' => t('Participation ID'),
        'getter callback' => [PartyCommunicationParticipation::class, 'structurePropertyGet'],
      ],
      'role' => [
        'type' => 'text',
        'label' => t('Participation Role'),
        'getter callback' => [PartyCommunicationParticipation::class, 'structurePropertyGet'],
      ],
      'status' => [
        'type' => 'text',
        'label' => t('Participation Status'),
        'getter callback' => [PartyCommunicationParticipation::class, 'structurePropertyGet'],
      ],
      'party' => [
        'type' => 'party',
        'label' => t('Party'),
        'getter callback' => [PartyCommunicationParticipation::class, 'structurePropertyGet'],
      ],
      'name' => [
        'type' => 'text',
        'label' => t('Name'),
        'getter callback' => [PartyCommunicationParticipation::class, 'structurePropertyGet'],
      ],
      'contact_data' => [
        'type' => 'party_communication_contact',
        'label' => t('Contact Information'),
        'property info' => [
          'primitive' => [
            'type' => 'text',
            'label' => t('Primitive'),
            'options list' => 'party_communication_contact_primitive_options',
            'getter callback' => [PartyCommunicationContact::class, 'structurePropertyGet'],
          ],
          'raw' => [
            'type' => 'text',
            'label' => t('Raw Data'),
            'getter callback' => [PartyCommunicationContact::class, 'structurePropertyGet'],
          ],
          'string' => [
            'type' => 'text',
            'label' => t('String'),
            'getter callback' => [PartyCommunicationContact::class, 'structurePropertyGet'],
          ],
          'formatted' => [
            'type' => 'text',
            'label' => t('Formatted'),
            'getter callback' => [PartyCommunicationContact::class, 'structurePropertyGet'],
          ],
          'key' => [
            'type' => 'text',
            'label' => t('Key'),
            'getter callback' => [PartyCommunicationContact::class, 'structurePropertyGet'],
          ],
          'source' => [
            'type' => 'party_communication_contact_source',
            'label' => t('Source'),
            'getter callback' => [PartyCommunicationContact::class, 'structurePropertyGet'],
            'property info' => [
              'label' => [
                'type' => 'text',
                'label' => t('Key'),
                'getter callback' => [PartyCommunicationContactSource::class, 'structurePropertyGet'],
              ],
            ]
          ],
        ],
        'property info alter' => [PartyCommunicationContact::class, 'structurePropertyInfoAlter'],
        'getter callback' => [PartyCommunicationParticipation::class, 'structurePropertyGet'],
      ],
      'data' => [
        'type' => 'party_communication_participation_data',
        'property info' => [],
        'property info alter' => [static::class, 'dataStructurePropertyInfoAlter'],
        'getter callback' => [static::class, 'structurePropertyGet'],
      ],
    ];
  }

  public static function dataStructurePropertyInfoAlter($wrapper, $info) {
    try {
      $value = $wrapper->value();
    }
    catch (EntityMetadataWrapperException $e) {
      $value = [];
    }

    if (is_array($value)) {
      foreach ($value as $key => $value) {
        if (isset($info['properties'][$key])) {
          continue;
        }

        $info['properties'][$key] = [
          'type' => is_int($value) || (is_string($value) && is_numeric($value) && stripos($value, '.') === FALSE) ?
            'integer' :
            (is_float($value) || (is_string($value) && is_numeric($value) && stripos($value, '.') !== FALSE) ?
              'decimal' :
              (is_string($value) ? 'text' :
                (is_object($value) || (is_array($value) && is_string(key($value))) ? 'struct' : 'list')
              )
            ),
          'label' => ucfirst($key),
          'getter callback' => 'entity_property_verbatim_get',
        ];

        if ($info['properties'][$key]['type'] === 'struct') {
          $info['properties'][$key]['property info'] ??= [];
          $info['properties'][$key]['property info alter'] = [static::class, 'dataStructurePropertyInfoAlter'];
        }
      }
    }

    return $info;
  }

  /**
   * Property getter for when this is a structure.
   *
   * @param \PartyCommunicationParticipation $data
   *   The participation data.
   * @param array $options
   *   The getter options.
   * @param string $name
   *   The name.
   * @param string $type
   *   The type.
   * @param array $info
   *   The info of the property being got.
   */
  public static function structurePropertyGet($data, array $options, $name, $type, $info) {
    if (!($data instanceof PartyCommunicationParticipation)) {
      return NULL;
    }

    switch ($name) {
      case 'id':
        return $data->id();
      case 'role':
        return $data->role();
      case 'status':
        return $data->status();
      case 'party':
        return $data->partyId();
      case 'name':
        return $data->contactName();
      case 'contact_data':
        return $data->contactData();
      case 'data':
        return $data->data();
    }

    return NULL;
  }

}
