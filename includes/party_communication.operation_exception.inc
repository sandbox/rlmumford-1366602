<?php

class PartyCommunicationOperationException extends Exception {

  /**
   * @var \PartyCommunication
   */
  protected $communication;

  /**
   * @var string
   */
  protected $operation;

  /**
   * PartyCommunicationOperationException constructor.
   *
   * @param \PartyCommunication $communication
   * @param $operation
   * @param string $message
   * @param int $code
   * @param \Throwable|NULL $previous
   */
  public function __construct(PartyCommunication $communication, $operation, $message = "", $code = 0, \Throwable $previous = NULL) {
    $this->communication = $communication;
    $this->operation = $operation;

    parent::__construct($message, $code, $previous);
  }

  /**
   * @return \PartyCommunication
   */
  public function getCommunication() {
    return $this->communication;
  }

  /**
   * @return string
   */
  public function getOperation() {
    return $this->operation;
  }
}
