<?php

/**
 * @file
 *   Party Communication Hooks.
 */

/**
 * Implements hook_party_communication_status_info().
 *
 * Define main statuses.
 */
function party_communication_party_communication_status_info() {
  $modes = drupal_map_assoc(array_keys(party_communication_mode_info()));
  $statuses = array();
  $statuses['draft'] = array(
    'label' => t('Draft'),
    'manual' => TRUE,
    'modes' => $modes,
  );
  $statuses['ready'] = array(
    'label' => t('Ready'),
    'manual' => TRUE,
    'modes' => $modes,
  );
  $statuses['sent'] = array(
    'label' => t('Sent'),
    'participation_locked' => TRUE,
    'modes' => $modes,
  );
  $statuses['failed'] = array(
    'label' => t('Failed to Send'),
    'participation_locked' => TRUE,
    'modes' => $modes,
  );
  $statuses['received'] = array(
    'label' => t('Received'),
    'participation_locked' => TRUE,
    'modes' => $modes,
  );
  $statuses['scheduled'] = array(
    'label' => t('Scheduled'),
    'modes' => $modes,
  );
  $statuses['cancelled'] = array(
    'label' => t('Cancelled'),
    'participation_locked' => TRUE,
    'modes' => $modes,
  );
  return $statuses;
}

/**
 * Implements hook_party_communication_groups()
 */
function party_communication_party_communication_groups() {
  return array(
    'inbound' => array(
      'label' => t('In Bound Communications'),
    ),
    'template' => array(
      'label' => t('Communication Template'),
    ),
    'custom' => array(
      'label' => t('Custom Communication'),
    ),
  );
}
