<?php

/**
 * @file
 * Contains communication class.
 */

class PartyCommunication extends Entity {

  /**
   * @var Entity
   *   The type entity.
   */
  protected $typeEntity = NULL;

  /**
   * The participation associated with this communication.
   *
   * @var \PartyCommunicationParticipation[]
   */
  protected $participations = NULL;

  /**
   * Whether the entity needs to have its participations saved.
   *
   * @var bool|null
   */
  protected $_needsParticipationsSave = NULL;

  /**
   * Post save callbacks.
   *
   * @var array[]
   *
   */
  protected $__postSaveCallbacks = array();

  protected function defaultUri() {
    return array('path' => 'admin/community/communications/communication/'.$this->id);
  }

  /**
   * Get the entity storage controller.
   *
   * @return \PartyCommunicationController
   */
  protected function getStorageController() : PartyCommunicationController {
    return entity_get_controller($this->entityType);
  }

  /**
   * Get the party communication type.
   *
   * @return \PartyCommunicationType
   */
  public function getType() {
    if ($this->typeEntity) {
      return $this->typeEntity;
    }

    $this->typeEntity = entity_load_single('party_communication_type', $this->type);
    return $this->typeEntity;
  }

  /**
   * Do an operation on this communication.
   *
   * @param $op
   * @param $settings
   *
   * @return mixed
   */
  public function doOperation($op, $settings = []) {
    return $this->getType()->getController()->doOperation($op, $this, $settings);
  }

  /**
   * Validate operation.
   *
   * @param $op
   * @param $reasons
   *
   * @return mixed
   */
  public function validateOperation($op, array $options = [], &$reasons = []) {
    return $this->getType()->getController()->validateOperation($op, $this, $options, $reasons);
  }

  /**
   * Build the operation form.
   *
   * @param $form
   *   The existing form element.
   * @param $form_state
   *   The form state.
   * @param $op
   *   The operation.
   * @param array $settings
   *   The default settings for the operation.
   *
   * @return array
   *   The prepared form array.
   */
  public function operationForm($form, &$form_state, $op, array $settings = []) {
    if (is_string($op)) {
      $op = ['op' => $op] + $this->getType()->getController()->supportedOperations()[$op];
    }

    $settings = $settings + $this->getType()->getController()->settingsOperation($op['op'], $this);
    if (is_callable($op['form'])) {
      $form = call_user_func_array($op['form'], [$form, &$form_state, $this, $settings]);
    }

    $hook_context = [
      'communication' => $this,
      'operation' => $op,
      'settings' => $settings,
    ];
    drupal_alter(['party_communication_operation_form', 'party_communication_operation_' . $op['op'] . '_form'], $form, $form_state, $hook_context);

    return $form;
  }

  public function operationFormValidate($form, &$form_state, $op) {
    $ops = $this->getType()->getController()->supportedOperations();
    if (!empty($ops[$op]['formValidate'])) {
      $form_state['op'] = $op;
      call_user_func_array($ops[$op]['formValidate'], array($form, &$form_state, $this));
    }

    foreach (['party_communication_operation_form_validate', 'party_communication_operation_' . $op . '_form_validate'] as $hook) {
      foreach (module_implements($hook) as $module) {
        $func = "{$module}_{$hook}";
        if (is_callable($func)) {
          $func($form, $form_state, [
            'communication' => $this,
            'operation' => $ops[$op],
          ]);
        }
      }
    }
  }

  public function operationFormSubmit($form, &$form_state, $op) {
    if (!isset($form_state['configuration'])) {
      $form_state['configuration'] = array();
    }

    $ops = $this->getType()->getController()->supportedOperations();
    if (!empty($ops[$op]['formSubmit']) && is_callable($ops[$op]['formSubmit'])) {
      $form_state['op'] = $op;
      call_user_func_array($ops[$op]['formSubmit'], array(
        $form,
        &$form_state,
        $this
      ));
    }

    foreach (['party_communication_operation_form_submit', 'party_communication_operation_' . $op . '_form_submit'] as $hook) {
      foreach (module_implements($hook) as $module) {
        $func = "{$module}_{$hook}";
        if (is_callable($func)) {
          $func($form, $form_state, [
            'communication' => $this,
            'operation' => $ops[$op],
          ]);
        }
      }
    }

    return $form_state['configuration'];
  }

  public function operationFormFinish($form, &$form_state, $op) {
    $ops = $this->getType()->getController()->supportedOperations();
    if (!empty($ops[$op]['formFinish']) && is_callable($ops[$op]['formFinish'])) {
      $form_state['op'] = $op;
      call_user_func_array($ops[$op]['formFinish'], array($form, &$form_state));
    }
  }

  public function send($settings = array()) {
    return $this->doOperation('send', $settings);
  }

  public function validateSend($settings = array(), &$reasons = array()) {
    return $this->validateOperation('send', $reasons);
  }

  /**
   * Act just before any presave hooks are fired.
   */
  public function preSave() {
    global $user;

    if (empty($this->mode) && !empty($this->type)) {
      $this->mode = $this->getType()->mode;
    }

    if (!isset($this->inoutbound)) {
      $this->inoutbound = PARTY_COMMUNICATION_OUTBOUND;
    }

    if (!isset($this->is_new_revision)) {
      $this->is_new_revision = TRUE;
    }

    if ($this->is_new_revision) {
      if (empty($this->updaterSet) && module_exists('party_user') && $updater = party_user_get_party($user)) {
        $this->updater = $updater->pid;
      }
      $this->revision_timestamp = REQUEST_TIME;
    }

    if (empty($this->creator) && !empty($this->is_new)) {
      if (!empty($this->communication_sender[LANGUAGE_NONE][0]['target_id'])) {
        $this->creator = $this->communication_sender[LANGUAGE_NONE][0]['target_id'];
      }
      else if (module_exists('party_user') && $creator = party_user_get_party($user)) {
        $this->creator = $creator->pid;
      }
    }

    if (!empty($this->is_new) && empty($this->created)) {
      $this->created = REQUEST_TIME;
    }
  }

  /**
   * Act at the end of a save process.
   */
  public function postSave($update) {
    if (!$update && $this->participations) {
      $this->getStorageController()->saveParticipations($this, $this->participations, TRUE);
      $this->_needsParticipationsSave = NULL;
    }
    else if ($update && (!empty($this->is_new_revision) || $this->needsParticipationsSave())) {
      if (!$this->participations) {
        $this->participations = $this->original->getParticipations();
      }
      $this->getStorageController()->saveParticipations($this, $this->participations, TRUE);
      $this->_needsParticipationsSave = NULL;
    }
  }

  /**
   * Force the participations to be saved next time the entity is saved.
   *
   * @param bool $value
   *
   * @return $this
   */
  public function setNeedsParticipationsSave($value = TRUE) {
    $this->_needsParticipationsSave = $value;
    return $this;
  }

  /**
   * Check whether the entity needs to have its participations saved.
   *
   * @return bool
   */
  public function needsParticipationsSave() : bool {
    return $this->_needsParticipationsSave ?? FALSE;
  }

  /**
   * Add a participation.
   *
   * @param \PartyCommunicationParticipation $participation
   *
   * @return $this
   */
  public function addParticipation(PartyCommunicationParticipation $participation) : PartyCommunication{
    $this->getParticipations();

    $this->participations[$participation->id()] = $participation;

    return $this;
  }

  /**
   * Get the participation of this communication.
   *
   * @param string|null $role
   *   The role to restrict the return by.
   *
   * @return \PartyCommunicationParticipation[]
   */
  public function getParticipations(string $role = NULL) : array {
    if (is_null($this->participations)) {
      $this->participations = $this->id ? $this->getStorageController()->loadParticipations($this) : [];
    }

    return $role ?
      array_filter($this->participations ?? [], fn($participation) => $participation->role() === $role) :
      ($this->participations ?? []);
  }

  /**
   * Get a specific participation.
   *
   * @param string $id
   *   The partication id or NULL if one does not exist.
   *
   * @return \PartyCommunicationParticipation|null
   */
  public function getParticipation(string $id) : ?PartyCommunicationParticipation {
    foreach ($this->getParticipations() as $participation) {
      if ($participation->id() === $id) {
        return $participation;
      }
    }

    return NULL;
  }

  /**
   * Set a participation.
   *
   * @param \PartyCommunicationParticipation $participation
   *
   * @return $this
   */
  public function setParticipation(PartyCommunicationParticipation $participation) : PartyCommunication {
    $this->getParticipations();

    foreach ($this->participations as $k => $p) {
      if ($p->id() === $participation->id()) {
        $this->participations[$k] = $participation;
        return $this;
      }
    }

    $this->participations[$participation->id()] = $participation;
    return $this;
  }

  /**
   * Get the participation associated with a given role.
   *
   * This returns the first participation of a given role.
   *
   * @param string $role
   *   The role.
   *
   * @return \PartyCommunicationParticipation|null
   */
  public function getRoleParticipation(string $role) : ?PartyCommunicationParticipation {
    foreach ($this->getParticipations() as $participation) {
      if ($participation->role() === $role) {
        return $participation;
      }
    }

    return NULL;
  }

  /**
   * Register a callback to fire after the document save has completed.
   *
   * @param callable $callback
   *   The callback to run after save.
   * @param mixed ...$args
   *   The arguments to pass to the function
   */
  public function registerPostSaveCallback(callable $callback) {
    $this->__postSaveCallbacks[] = [
      'callback' => $callback,
      'args' => array_slice(func_get_args(), 1),
    ];
    return $this;
  }

  /**
   * Execute any post save callbacks.
   */
  public function executePostSaveCallbacks() {
    foreach ($this->__postSaveCallbacks as $psCallback) {
      if ($psCallback['callback'] instanceof \Closure) {
        $psCallback['callback']->call($this, ...$psCallback['args']);
      }
      else {
        call_user_func_array($psCallback['callback'], $psCallback['args']);
      }
    }
    $this->__postSaveCallbacks = [];
  }

}

class PartyCommunicationController extends EntityAPIController {

  /**
   * Send the communication.
   */
  public function send($entity, $settings = array()) {
    try {
      if (!variable_get('party_communication_block_all', FALSE)) {
        $controller = $entity->getType()->getController();
        $controller->doOperation('send', $entity, $settings);
      }
    }
    catch (Exception $e) {
      watchdog_exception($this->entityType, $e);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Validate the sending of the communication.
   */
  public function validateSend($entity, $settings = array(), &$reasons = array()) {
    $allowed = FALSE;
    try {
      $controller = $entity->getType()->getController();
      $allowed = $controller->validateOperation('send', $entity, [], $reasons);
    }
    catch (Exception $e) {
      watchdog_excepton($this->entityType, $e);
      return FALSE;
    }

    return $allowed;
  }

  /**
   * Create a communication.
   */
  public function create(array $values = array()) {
    if (empty($values['status'])) {
      $values['status'] = 'ready';
    }

    // Make sure the mode is set.
    if (empty($values['mode']) && !empty($values['type'])) {
      $values['mode'] = entity_load_single('party_communication_type', $values['type'])->mode;
    }

    $communication = parent::create($values);
    $this->invoke('create', $communication);
    return $communication;
  }

  /**
   * {@inheritdoc}
   */
  public function invoke($hook, $entity) {
    if (in_array($hook, array('presave'))) {
      $entity->preSave();
      $entity->getType()->getController()->onCommunicationPreSave($entity);
    }

    parent::invoke($hook, $entity);

    if (in_array($hook, array('update', 'insert'))) {
      $entity->getType()->getController()->onCommunicationPostSave($entity, $hook === 'update');
      $entity->postSave($hook === 'update');
    }
  }

  /**
   * Load the participations associated with this entity.
   *
   * @param $entity
   *
   * @return \PartyCommunicationParticipation[]
   */
  public function loadParticipations($entity) {
    $query = db_select('party_communication_participation', 'pcp');
    $query->condition('pcp.vid', $entity->vid);
    $query->fields('pcp');

    return array_map(
      function ($row) use ($entity) {
        $data = !empty($row['data']) ? json_decode($row['data'], TRUE) : [];
        return PartyCommunicationParticipation::create(
          $entity,
          $row['participation_role'],
          $row['participation_id'],
          PartyCommunicationContact::createData(
            $row['contact_data_primitive'],
            $data['contact_data'] ?? $row['contact_data'],
            $row['contact_data_key']
          ),
          $row['contact_name'],
          $row['party'],
          $row['participation_status'],
          $data ?? [],
        );
      },
      $query->execute()->fetchAllAssoc('participation_id', PDO::FETCH_ASSOC)
    );
  }

  /**
   * Save participations.
   *
   * @param \PartyCommunication $entity
   * @param \PartyCommunicationParticipation[] $participations
   * @param bool $new_revision
   *
   * @return void
   */
  public function saveParticipations($entity, array $participations, bool $new_revision = FALSE) {
    $old_ids = array_filter(array_map(
      fn($participation) => $participation->hasOldId() ? $participation->oldId() : NULL,
      $participations
    ));
    if (!empty($old_ids)) {
      db_delete('party_communication_participation')
        ->condition('participation_id', $old_ids)
        ->condition('id', $entity->id)
        ->condition('vid', $entity->vid)
        ->execute();
    }

    if ($new_revision) {
      $q = db_insert('party_communication_participation');
      $q->fields(['id', 'vid', 'participation_id', 'participation_role', 'contact_data_primitive', 'contact_data', 'contact_data_key', 'contact_name', 'party', 'participation_status', 'data']);
      foreach ($participations as $participation) {
        $q->values([
          'id' => $entity->id,
          'vid' => $entity->vid,
          'participation_id' => $participation->id(),
          'participation_role' => $participation->role(),
          'contact_data_primitive' => $participation->contactData() ? $participation->contactData()->getPrimitive() : NULL,
          'contact_data' => $participation->contactData() ? $participation->contactData()->stringifyData() : NULL,
          'contact_data_key' => $participation->contactData() ? $participation->contactData()->getKey() : NULL,
          'contact_name' => $participation->contactName(),
          'party' => $participation->partyId(),
          'participation_status' => $participation->status(),
          'data' => json_encode($participation->data() ?: []),
        ]);
      }
      $q->execute();
    }
    else {
      foreach ($participations as $participation) {
        db_merge('party_communication_participation')
          ->key([
            'vid' => $entity->vid,
            'participation_id' => $participation->id(),
          ])
          ->fields([
            'id' => $entity->id,
            'participation_role' => $participation->role(),
            'contact_data_primitive' => $participation->contactData() ? $participation->contactData()->getPrimitive() : NULL,
            'contact_data' => $participation->contactData() ? $participation->contactData()->stringifyData() : NULL,
            'contact_data_key' => $participation->contactData() ? $participation->contactData()->getKey() : NULL,
            'contact_name' => $participation->contactName(),
            'party' => $participation->partyId(),
            'participation_status' => $participation->status(),
            'data' => json_encode($participation->data() ?: []),
          ])
          ->execute();
      }
    }

    $this->resetCache([$entity->id]);
    module_invoke_all('party_communication_participations_update', $entity, $participations, $new_revision);
  }

  /**
   * Save events associated with a communication.
   *
   * @param \PartyCommunication $entity
   * @param \PartyCommunicationEvent|\PartyCommunicationEvent[] $events
   *
   * @return void
   * @throws \Exception
   */
  public function saveEvents(PartyCommunication $entity, $events) {
    if ($events instanceof PartyCommunicationEvent) {
      $events = [$events];
    }

    $query = db_insert('party_communication_event')
      ->fields(['uuid', 'id', 'vid', 'participation_id', 'datetime', 'event', 'label', 'data']);
    foreach ($events as $event) {
      $query->values([
        'uuid' => $event->uuid() ?: uuid_generate(),
        'id' => $entity->id,
        'vid' => $entity->vid,
        'participation_id' => $event->participationId(),
        'datetime' => substr($event->datetime()->format(DateTimeInterface::RFC3339_EXTENDED), 0, -6),
        'event' => $event->eventType(),
        'label' => $event->label(),
        'data' => json_encode($event->data() ?: []),
      ]);
    }
    $query->execute();

    module_invoke_all('party_communication_events_save', $entity, $events);
  }

  /**
   * Load the events for a given communication.
   *
   * @param \PartyCommunication $entity
   * @param $options
   *
   * @return \PartyCommunicationEvent[]
   */
  public function loadEvents(PartyCommunication $entity, $options = []) {
    $query = db_select('party_communication_event', 'e')->fields('e');
    $query->condition('e.id', $entity->id);
    $query->orderBy('e.datetime');

    $events = [];
    foreach ($query->execute() as $event) {
      $events[] = PartyCommunicationEvent::create(
        $entity,
        $event->event,
        $event->label ?: "",
        json_decode($event->data, TRUE) ?: [],
        $event->participation_id ?: NULL,
        DateTime::createFromFormat('Y-m-d H:i:s.v', $event->datetime, new DateTimeZone('UTC')),
        $event->uuid
      );
    }

    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $return = parent::save($entity, $transaction);

    if ($entity instanceof \PartyCommunication) {
      $entity->executePostSaveCallbacks();
    }

    return $return;
  }

}
