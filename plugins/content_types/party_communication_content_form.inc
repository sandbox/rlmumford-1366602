<?php
/**
 * Plugin for specially rendered content.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Communication Content Form'),
  'description' => t('Edit the long text fields used in the communication.'),
  'required context' => new ctools_context_required(t('Communication'), 'party_communication'),
  'category' => t('Communications'),
);

function party_communication_party_communication_content_form_content_type_render($subtype, $conf, $panel_args, &$context) {
  $communication = $context->data;

  module_load_include('inc', 'party_communication', 'party_communication.ui');

  $block = new stdClass();
  $block->module = 'party_communication';
  $block->delta = 'conten_form';
  $block->content = drupal_get_form('party_communication_long_text_content_form', $communication);

  return $block;
}

function party_communication_party_communication_content_form_content_type_admin_title($subtype, $conf, $context) {
  return t('Communication Content Form');
}

function party_communication_party_communication_content_form_content_type_edit_form($form, &$form_state) {
  return $form;
}
