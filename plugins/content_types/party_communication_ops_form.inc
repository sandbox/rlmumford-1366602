<?php
/**
 * Plugin for specially rendered content.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Communication Operations Form'),
  'description' => t('Operations buttons for the communication.'),
  'required context' => new ctools_context_required(t('Communication'), 'party_communication'),
  'category' => t('Communications'),
);

function party_communication_party_communication_ops_form_content_type_render($subtype, $conf, $panel_args, &$context) {
  $communication = $context->data;

  $block = new stdClass();
  $block->module = 'party_communication';
  $block->delta = 'operations_form';
  $block->content = drupal_get_form('party_communication_operations_form', $communication);

  return $block;
}

function party_communication_party_communication_ops_form_content_type_admin_title($subtype, $conf, $context) {
  return t('Communication Operations');
}

function party_communication_party_communication_ops_form_content_type_edit_form($form, &$form_state) {
  return $form;
}
