<?php
$plugin = array(
  'single' => TRUE,
  'title' => t('Communication Participation'),
  'description' => t('Display participation information.'),
  'required context' => new ctools_context_required(t('Communication'), 'party_communication'),
  'category' => t('Communications'),
);

function party_communication_party_communication_participations_content_type_render($subtype, $conf, $panel_args, &$context) {
  /** @var \PartyCommunication $communication */
  $communication = $context->data;
  $controller = $communication->getType()->getController();

  $build = array();
  foreach ($controller->getParticipationRoles() as $role => $role_info) {
    // Skip roles that are not included.
    if (empty($conf['roles'][$role])) {
      continue;
    }

    $participations = $communication->getParticipations($role);

    $build[$role] = [
      '#theme' => 'field',
      '#field_name' => 'communication_participations_' . $role,
      '#field_type' => 'party_communication_participation',
      '#title' => $role_info['label'],
      '#label_display' => 'inline',
      '#items' => [],
      '#access' => empty($conf['hide_empty_roles']) || !empty($participations),
    ];

    foreach ($participations as $participation) {
      $build[$role]['#items'][] = [
        'id' => $participation->id(),
        'participation' => $participation,
      ];

      $build[$role][] = $participation->render('inline');
    }
  }

  $block = new stdClass();
  $block->module = 'party_communication';
  $block->delta = 'particpations';
  $block->content = $build;

  return $block;
}

function party_communication_party_communication_participations_content_type_admin_title($subtype, $conf, $context) {
  return t('Participant Information');
}

function party_communication_party_communication_participations_content_type_edit_form($form, &$form_state) {
  $role_options = [];
  foreach (party_communication_mode_info() as $mode => $info) {
    $controller = party_communication_mode_controller($mode);
    foreach ($controller->getParticipationRoles() as $role => $info) {
      if (!isset($role_options[$role])) {
        $role_options[$role] = [
          'label' => $info['label'],
          'description' => $info['description'],
          'aka' => [],
        ];
      }
      else if ($info['label'] !== $role_options[$role]['label'] && !in_array($role_options[$role]['label'], $role_options[$role]['aka'])) {
        $role_options[$role]['aka'][] = $info['label'];
      }
    }
  }

  $form['roles'] = [
    '#type' => 'checkboxes',
    '#title' => t('Roles to Display'),
    '#description' => t('Not all roles are available on all communication types.'),
    '#options' => array_map(fn($option) => $option['label'] . (!empty($option['aka']) ? ' (Also known as ' . implode(', ', $option['aka']) . ')' : ''), $role_options),
    '#default_value' => $form_state['conf']['roles'] ?? [],
  ];

  $form['hide_empty_roles'] = [
    '#type' => 'checkbox',
    '#title' => t('Hide empty roles'),
    '#description' => t('Hide roles that have no-one in them.'),
    '#default_value' => !empty($form_state['conf']['hide_empty_roles']),
  ];

  return $form;
}

function party_communication_party_communication_participations_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['roles'] = array_filter($form_state['values']['roles'] ?? []);
  $form_state['conf']['hide_empty_roles'] = !empty($form_state['values']['hide_empty_roles']);
}
