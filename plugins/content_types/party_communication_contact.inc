<?php
/**
 * Plugin for displaying contact information.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Contact Information'),
  'description' => t('Show a summary of the contact information we hold for a particular client.'),
  'required context' => new ctools_context_required(t('Party'), 'party'),
  'category' => t('Party'),
);

function party_communication_party_communication_contact_content_type_render($subtype, $conf, $panel_args, &$context) {
  $party = $context->data;

  $build = array();

  // @todo: Loop over supported primitives and show info.
  $primitives = !empty($conf['primitives']) ? $conf['primitives'] : array_keys(party_communication_contact_primitive_options());
  $prim_info = PartyCommunicationContact::getPrimitiveInfo();
  $conf['show_all'] = TRUE;
  foreach ($primitives as $primitive) {
    if (empty($conf['show_all'])) {
      $data = PartyCommunicationContact::getData($party, $primitive);
      if (empty($data)) {
        continue;
      }

      $build[$primitive] = array(
        '#type' => 'item',
        '#title' => $prim_info[$primitive]['label'],
      );
      $rendered = $data->renderData();
      if (is_array($rendered)) {
        $build[$primitive]['data'] = $rendered;
      }
      else {
        $build[$primitive]['#markup'] = $rendered;
      }
    }
    else {
      $data = PartyCommunicationContact::collectData($party, $primitive);
      if (empty($data)) {
        continue;
      }

      $table = array(
        '#theme' => 'table',
        '#header' => array(),
        '#rows' => array(),
      );
      foreach ($data as $data_item) {
        $row = array(
          array('data' => $data_item->getTitle()),
          array('data' => $data_item->renderData()),
        );
        $table['#rows'][] = $row;
      }

      $build[$primitive] = array(
        '#type' => 'item',
        '#title' => $prim_info[$primitive]['plural label'],
        '#markup' => render($table),
      );
    }
  }

  $block = new stdClass();
  $block->module = 'party_communication';
  $block->delta = 'contact_data';
  $block->content = $build;

  return $block;
}

function party_communication_party_communication_contact_content_type_admin_title($subtype, $conf, $context) {
  return t('Rendered Communication Contact Data');
}

function party_communication_party_communication_contact_content_type_edit_form($form, &$form_state) {

  $form['primitives'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show'),
    '#description' => t('Tick the type of contact information you wish to show. Leave blank to show all.'),
    '#options' => party_communication_contact_primitive_options(),
    '#default_value' => !empty($form_state['conf']['primitives']) ? $form_state['conf']['primitives'] : array(),
  );

  $form['show_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show All'),
    '#description' => t('Leave this checkbox blank to show only the main piece of contact data for each primitive.'),
    '#default_value' => !empty($form_state['conf']['show_all']),
  );

  return $form;
}

function party_communication_party_communication_contact_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['primitives'] = array_filter($form_state['values']['primitives']);
  $form_state['conf']['show_all'] = !empty($form_state['values']['show_all']);
}
