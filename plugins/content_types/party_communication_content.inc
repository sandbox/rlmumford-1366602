<?php
/**
 * Plugin for specially rendered content.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Communication Rendered Content'),
  'description' => t('Render prepared content.'),
  'required context' => new ctools_context_required(t('Communication'), 'party_communication'),
  'category' => t('Communications'),
);

function party_communication_party_communication_content_content_type_render($subtype, $conf, $panel_args, &$context) {
  $communication = $context->data;
  $controller = $communication->getType()->getController();
  $build = array();
  foreach ($controller->communicationContentFields('send') as $field_name => $label) {
    $build[$field_name]['#markup'] = $controller->renderContentField($communication, $field_name, !empty($conf['preview']));
  }

  $block = new stdClass();
  $block->module = 'party_communication';
  $block->delta = 'rendereredContent';
  $block->content = $build;

  return $block;
}

function party_communication_party_communication_content_content_type_admin_title($subtype, $conf, $context) {
  return t('Rendered Communication Content');
}

function party_communication_party_communication_content_content_type_edit_form($form, &$form_state) {
  $form['preview'] = array(
    '#type' => 'checkbox',
    '#title' => t('Is this a preview or a final version?'),
    '#default_value' => !empty($form_state['conf']['preview']),
  );

  return $form;
}

function party_communication_party_communication_content_content_type_edit_form_submit($form, $form_state) {
  $form_state['conf']['preview'] = !empty($form_state['values']['preview']);
}
