<?php
/**
 * Plugin for specially rendered content.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Communication Thread'),
  'description' => t('Render a thread view of the communication.'),
  'required context' => new ctools_context_required(t('Communication'), 'party_communication'),
  'category' => t('Communications'),
);

function party_communication_party_communication_thread_content_type_render($subtype, $conf, $panel_args, &$context) {
  module_load_include('inc', 'party_communication', 'party_communication.ui');

  $block = new stdClass();
  $block->module = 'party_communication';
  $block->delta = 'thread';
  $block->content = party_communication_thread_build($context->data);

  return $block;
}

function party_communication_party_communication_thread_content_type_admin_title($subtype, $conf, $context) {
  return t('Rendered Communication Thread');
}

function party_communication_party_communication_thread_content_type_edit_form($form, &$form_state) {

  return $form;
}

function party_communication_party_communication_thread_content_type_edit_form_submit($form, $form_state) {
}
