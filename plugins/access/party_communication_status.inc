<?php

/**
 * @file
 * Plugin to provide access control based upon node type.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Communication: status"),
  'description' => t('Control access by communication status.'),
  'callback' => 'party_communication_party_communication_status_party_communication_access_check',
  'default' => array('status' => array()),
  'settings form' => 'party_communication_party_communication_status_party_communication_access_settings',
  'settings form submit' => 'party_communication_party_communication_status_party_communication_access_settings_submit',
  'summary' => 'party_communication_party_communication_status_party_communication_access_summary',
  'required context' => new ctools_context_required(t('Communication'), 'party_communication'),
);

/**
 * Settings form for the 'by party_communication_status' access plugin
 */
function party_communication_party_communication_status_party_communication_access_settings($form, &$form_state, $conf) {
  $options = party_communication_status_options();
  $form['settings']['status'] = array(
    '#title' => t('Status'),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#description' => t('Only the checked statuses will be valid.'),
    '#default_value' => $conf['status'],
  );
  return $form;
}

/**
 * Compress the party_communication_statuss allowed to the minimum.
 */
function party_communication_party_communication_status_party_communication_access_settings_submit($form, &$form_state) {
  $form_state['values']['settings']['status'] = array_filter($form_state['values']['settings']['status']);
}

/**
 * Check for access.
 */
function party_communication_party_communication_status_party_communication_access_check($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data) || empty($context->data->type)) {
    return FALSE;
  }

  if (array_filter($conf['status']) && empty($conf['status'][$context->data->status])) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Provide a summary description based upon the checked party_communication_statuss.
 */
function party_communication_party_communication_status_party_communication_access_summary($conf, $context) {
  if (!isset($conf['status'])) {
    $conf['status'] = array();
  }
  $statuses = party_communication_status_options();

  return format_plural(count($statuses), '@identifier is type "@types"', '@identifier type is one of "@types"', array('@types' => implode(', ', $statuses), '@identifier' => $context->identifier));
}

