<?php
/**
 * @file
 * Class for the Panelizer party_communication entity plugin.
 */

/**
 * Panelizer Entity party_communication plugin class.
 *
 * Handles party_communication specific functionality for Panelizer.
 */
class PanelizerEntityPartyCommunication extends PanelizerEntityDefault {
  /**
   * True if the entity supports revisions.
   */
  public $supports_revisions = TRUE;
  public $entity_admin_root = 'admin/structure/communication_types/manage/%';
  public $entity_admin_bundle = 4;
  // No bundle support so we hardcode the default bundle.
  public $views_table = 'party_communication';
  public $uses_page_manager = FALSE;

  /**
   * Determine if the entity allows revisions.
   */
  public function entity_allows_revisions($entity) {
    $retval[0] = $this->supports_revisions;
    $retval[1] = user_access('administer communications');

    return $retval;
  }

  /**
   * Implements PanelizerEntityDefault::entity_access().
   */
  public function entity_access($op, $entity) {
    return entity_access($op, 'party_communication', $entity);
  }

  /**
   * Implements PanelizerEntityDefault::entity_save().
   */
  public function entity_save($entity) {
    return entity_save('party_communication', $entity);
  }

  /**
   * Overrides PanelizerEntityDefault::preprocess_panelizer_view_mode().
   */
  public function preprocess_panelizer_view_mode(&$vars, $entity, $element, $panelizer, $info) {
    $panelizer->link_to_entity = FALSE;

    parent::preprocess_panelizer_view_mode($vars, $entity, $element, $panelizer, $info);
  }

  /**
   * Overrides PanelizerEntityDefault::hook_entity_insert().
   *
   * Do nothing!
   */
  public function hook_entity_insert($entity) {}

  /**
   * Overrides PanelizerEntityDefault::hook_entity_insert().
   *
   * Do nothing!
   */
  public function hook_entity_update($entity) {}

  /**
   * Overrides the entity getter to make sure panelizer is populated.
   */
  public function get_entity_view_entity($build) {
    $entity = parent::get_entity_view_entity($build);

    if (!isset($entity->panelizer)) {
      $entities = array($entity->id => $entity);
      $this->hook_entity_load($entities);
    }

    return $entity;
  }

}
